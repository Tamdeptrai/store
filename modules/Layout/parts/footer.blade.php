<footer id="footer" class="footer-wrapper">
    <!-- FOOTER 1 -->
    <div class="footer-widgets footer footer-1">
        <div class="container">
            <div class="row large-columns-4 mb-0">
            @if($list_widget_footers = setting_item_with_lang("list_widget_footer"))
                <?php $list_widget_footers = json_decode($list_widget_footers); $stt = 1;?>
                @foreach($list_widget_footers as $key=>$item)
                    <div  class="col pb-0 widget widget_nav_menu">
                        <span class="widget-title color-white">{{$item->title}}</span>
                        <div class="is-divider small"></div>
                        <div class="menu-menu-top-container">
                            {!! clean($item->content) !!}
                        </div>
                    </div>
                @endforeach
            @endif  
        </div>
        </div>
        <!-- end row -->
    </div>
    <!-- footer 1 -->
    <!-- FOOTER 2 -->
    <div class="footer-widgets footer footer-2 ">
        <div class="container">
            <div class="row large-columns-3 mb-0">
                <div id="text-2" class="col pb-0 widget widget_text">
                    <span class="widget-title color-white">Phương thức thanh toán</span>
                    <div class="is-divider small"></div>
                    <div class="textwidget">
                        <p>
                            <img loading="lazy" class="alignnone size-medium wp-image-764" src="/images/payment_7.png" alt="" width="67" height="26" /> 
                            <img loading="lazy" class="alignnone size-medium wp-image-765" src="/images/payment_6.png" alt="" width="67" height="26" /> 
                            <img loading="lazy" class="alignnone size-medium wp-image-766" src="/images/payment_5.png" alt="" width="67" height="26"  /> 
                            <img loading="lazy" class="alignnone size-medium wp-image-767" src="/images/payment_4.png" alt="" width="67" height="26" /> 
                            <img loading="lazy" class="alignnone size-medium wp-image-768" src="/images/payment_3.png" alt="" width="67" height="26" /> 
                            <img loading="lazy" class="alignnone size-medium wp-image-769" src="/images/payment_2.png" alt="" width="67" height="26"  /> 
                            <img loading="lazy" class="alignnone size-medium wp-image-770" src="/images/payment_1.png" alt="" width="67" height="26" />
                        </p>
                    </div>
                </div>
                <div id="custom_html-2" class="widget_text col pb-0 widget widget_custom_html">
                    <span class="widget-title color-white">Kết nối với chúng tôi</span>
                    <div class="is-divider small"></div>
                    <div class="textwidget custom-html-widget">
                        <ul class="list-menu list-inline">
                            <li class="twitter"> <a href="#" target="_blank" rel="noopener noreferrer"> <i class="fa fa-twitter" aria-hidden="true"></i> </a> </li>
                            <li class="facebook"> <a href="#" target="_blank" rel="noopener noreferrer"> <i class="fa fa-facebook" aria-hidden="true"></i> </a> </li>
                            <li class="pinterest"> <a href="#" target="_blank" rel="noopener noreferrer"> <i class="fa fa-pinterest-p" aria-hidden="true"></i> </a> </li>
                            <li class="google"> <a href="#" target="_blank" rel="noopener noreferrer"> <i class="fa fa-google-plus" aria-hidden="true"></i> </a> </li>
                            <li class="instagram"> <a href="#" target="_blank" rel="noopener noreferrer"> <i class="fa fa-instagram" aria-hidden="true"></i> </a> </li>
                            <li class="youtube"> <a href="#" target="_blank" rel="noopener noreferrer"> <i class="fa fa-youtube" aria-hidden="true"></i> </a> </li>
                        </ul>
                    </div>
                </div>
                <div id="text-3" class="col pb-0 widget widget_text">
                    <span class="widget-title color-white">Đăng ký nhận tin</span>
                    <div class="is-divider small"></div>
                    <div class="textwidget">
                        <p class="color-white">Nhận thông tin sản phẩm mới nhất, tin khuyến mãi và nhiều hơn nữa.</p>
                        <div role="form" class="wpcf7 newsletter-form" lang="vi" dir="ltr">
                            <div class="screen-reader-response" role="alert" aria-live="polite"></div>
                            <form action="{{ route('newsletter.subscribe') }}" method="post" class="wpcf7-form init bravo-subscribe-form" novalidate="novalidate">
                                @csrf
                                
                                <div class="flex-row medium-flex-wrap">
                                    <div class="flex-col flex-grow">
                                        <span class="wpcf7-form-control-wrap your-email">
                                            <input type="email" name="email" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false" placeholder="Email của bạn" /></span>
                                    </div>
                                    <div class="flex-col">
                                        <input type="submit" value="Đăng ký" class="wpcf7-form-control wpcf7-submit button" />
                                    </div>
                                </div>
                                <div class="wpcf7-response-output form-mess" role="alert" aria-hidden="true"></div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        <!-- end row -->
        </div>
    </div>
    <!-- end footer 2 -->
    <div class="absolute-footer light medium-text-center text-center">
        <div class="container clearfix">
            <div class="footer-primary pull-left">
                <div class="copyright-footer">
                </div>
            </div>
            <!-- .left -->
        </div>
        <!-- .container -->
    </div>
    <!-- .absolute-footer -->
    <a href="#top" class="back-to-top button icon invert plain fixed bottom z-1 is-outline hide-for-medium circle" id="top-link"><i class="icon-angle-up" ></i></a>
</footer>
<link rel="stylesheet" href="{{asset('libs/flags/css/flag-icon.min.css')}}" >
@include('Layout::parts/login-register-modal')

{!! \App\Helpers\Assets::css(true) !!}

<script src="{{asset('libs/lazy-load/intersection-observer.js')}}"></script>
<script async src="{{asset('libs/lazy-load/lazyload.min.js')}}"></script>

<script>
    window.lazyLoadOptions = {
        elements_selector: ".lazy",
    };

    window.addEventListener('LazyLoad::Initialized', function (event) {
        window.lazyLoadInstance = event.detail.instance;
    }, false);
</script>
<script src="{{ asset('libs/lodash.min.js') }}"></script>
<script src="{{ asset('libs/jquery-3.3.1.min.js') }}"></script>
<script src="{{ asset('libs/jquery_ui/jquery-ui.min.js') }}"></script>
<script src="{{ asset('libs/vue/vue.js') }}"></script>
<script src="{{ asset('libs/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('libs/bootbox/bootbox.min.js') }}"></script>
<script src="{{asset('libs/daterange/moment.min.js')}}"></script>
<script src="{{asset('libs/daterange/daterangepicker.min.js')}}"></script>
@if(Auth::id())
    <script src="{{ asset('module/media/js/browser.js?_ver='.config('app.version')) }}"></script>
@endif
<script src="{{ asset('libs/carousel-2/owl.carousel.min.js') }}"></script>
<script src="{{ asset('libs/select2/js/select2.min.js') }}" ></script>
<script src="{{ asset('libs/slimScroll/jquery.slimscroll.min.js') }}" ></script>
<script src="{{ asset('libs/slick/slick.min.js') }}" ></script>
<script src="{{ asset('js/functions.js?_ver='.config('app.version')) }}"></script>


<script type='text/javascript' src="{{ asset('websieukhung/plugins/contact-form-7/includes/js/scripts7752.js') }}" id='contact-form-7-js'></script>
<script type='text/javascript' src="{{ asset('websieukhung/plugins/devvn-quick-buy/js/jquery.validate.min4c71.js') }}" id='jquery.validate-js'></script>
<script type='text/javascript' src='{{ asset('websieukhung/js/underscore.min4511.js') }}' id='underscore-js'></script>
<script type='text/javascript' id='wp-util-js-extra'>
    /* <![CDATA[ */
    var _wpUtilSettings = {"ajax":{}};
    /* ]]> */
</script>
<script type='text/javascript' src='{{ asset('websieukhung/js/wp-util.min5697.js') }}' id='wp-util-js'></script>
<script type='text/javascript' src="{{ asset('websieukhung/plugins/woocommerce/assets/js/jquery-blockui/jquery.blockUI.min44fd.js') }}" id='jquery-blockui-js'></script>
<script type='text/javascript' id='wc-add-to-cart-variation-js-extra'>
    /* <![CDATA[ */
    var wc_add_to_cart_variation_params = {};
    /* ]]> */
</script>
<!--            <script type='text/javascript' src='wp-content/plugins/woocommerce/assets/js/frontend/add-to-cart-variation.min274c.js?ver=4.3.2' id='wc-add-to-cart-variation-js'></script>-->
<script type='text/javascript' id='devvn-quickbuy-script-js-extra'>
    /* <![CDATA[ */
    var devvn_quickbuy_array = {};
    /* ]]> */
</script>
<script type='text/javascript' src="{{ asset('websieukhung/plugins/devvn-quick-buy/js/devvn-quick-buy4c71.js') }}" id='devvn-quickbuy-script-js'></script>
<script type='text/javascript' src="{{ asset('websieukhung/plugins/ot-flatsome-vertical-menu/assets/vendor/superfish/hoverIntent9632.js') }}" id='ot-hoverIntent-js'></script>
<script type='text/javascript' src="{{ asset('websieukhung/plugins/ot-flatsome-vertical-menu/assets/vendor/superfish/superfish.min9632.js') }}" id='ot-superfish-js'></script>
<script type='text/javascript' src="{{ asset('websieukhung/plugins/ot-flatsome-vertical-menu/assets/js/ot-vertical-menu.min9632.js') }}" id='ot-vertical-menu-js'></script>
<script type='text/javascript' id='wc-add-to-cart-js-extra'>
    /* <![CDATA[ */
    var wc_add_to_cart_params = {};
    /* ]]> */
</script>
<script type='text/javascript' src="{{ asset('websieukhung/plugins/woocommerce/assets/js/frontend/add-to-cart.min274c.js') }}" id='wc-add-to-cart-js'></script>
<script type='text/javascript' src="{{ asset('websieukhung/plugins/woocommerce/assets/js/js-cookie/js.cookie.min6b25.js') }}" id='js-cookie-js'></script>
<script type='text/javascript' id='woocommerce-js-extra'>
    /* <![CDATA[ */
    var woocommerce_params = {};
    /* ]]> */
</script>
<script type='text/javascript' src="{{ asset('websieukhung/plugins/woocommerce/assets/js/frontend/woocommerce.min274c.js') }}" id='woocommerce-js'></script>
<script type='text/javascript' id='wc-cart-fragments-js-extra'>
    /* <![CDATA[ */
    var wc_cart_fragments_params = {};
    /* ]]> */
</script>
{{--<script type='text/javascript' src="{{ asset('websieukhung/plugins/woocommerce/assets/js/frontend/cart-fragments.min274c.js') }}" id='wc-cart-fragments-js'></script>--}}
{{--<script type='text/javascript' src="{{ asset('websieukhung/themes/flatsome/inc/extensions/flatsome-live-search/flatsome-live-searchfb6f.js') }}" id='flatsome-live-search-js'></script>--}}
<script type='text/javascript' src="{{ asset('websieukhung/js/hoverIntent.minc245.js') }}" id='hoverIntent-js'></script>
<script type='text/javascript' id='flatsome-js-js-extra'>
    /* <![CDATA[ */
    var flatsomeVars = {"rtl":"","sticky_height":"70","lightbox":{"close_markup":"<button title=\"%title%\" type=\"button\" class=\"mfp-close\"><svg xmlns=\"http:\/\/www.w3.org\/2000\/svg\" width=\"28\" height=\"28\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-x\"><line x1=\"18\" y1=\"6\" x2=\"6\" y2=\"18\"><\/line><line x1=\"6\" y1=\"6\" x2=\"18\" y2=\"18\"><\/line><\/svg><\/button>","close_btn_inside":false},"user":{"can_edit_pages":false},"i18n":{"mainMenu":"Main Menu"},"options":{"cookie_notice_version":"1"}};
    /* ]]> */
</script>
<script type='text/javascript' src="{{ asset('websieukhung/themes/flatsome/assets/js/flatsomefb6f.js') }}" id='flatsome-js-js'></script>
<script type='text/javascript' src="{{ asset('websieukhung/themes/flatsome/assets/js/woocommercefb6f.js') }}" id='flatsome-theme-woocommerce-js-js'></script>
<script type='text/javascript' src="{{ asset('websieukhung/js/wp-embed.min5697.js') }}" id='wp-embed-js'></script>
<script type='text/javascript' src="{{ asset('websieukhung/themes/flatsome/inc/shortcodes/ux_countdown/countdown-script-min5697.js') }}" id='flatsome-countdown-script-js'></script>
<script type='text/javascript' src="{{ asset('websieukhung/themes/flatsome/inc/shortcodes/ux_countdown/ux-countdownc169.js') }}"></script>
<script src="{{ asset('libs/flick/flickity.pkgd.min.js') }}"></script>

<script type="text/javascript">
    jQuery(document).ready(function() {
        var swiper_duan = new Swiper('#duanHome', {
                    slidesPerView: 3,
                    centeredSlides: true,
                    autoplay: {
                        delay: 2200,
                        disableOnInteraction: false,
                    },
                    spaceBetween: 0,
                    navigation: {
                        nextEl: '.duan-next',
                        prevEl: '.duan-prev',
                    },
                    speed: 400,
                    loop: true,
                    breakpoints: {
                        768: {
                            slidesPerView: 2,
                            spaceBetween: 30
                        }
                    }
                });
                $('#duanHome').on('mouseover', function () {
                    swiper_duan.autoplay.stop();
                });
                $('#duanHome').on('mouseout', function () {
                    swiper_duan.autoplay.start();
                });
            jQuery('.slider-banner').flickity({
                // options
                "cellAlign": "center",
                "imagesLoaded": true,
                "lazyLoad": 1,
                "freeScroll": false,
                "wrapAround": true,
                "autoPlay": 6000,
                "pauseAutoPlayOnHover" : true,
                "prevNextButtons": false,
                "contain" : true,
                "adaptiveHeight" : true,
                "dragThreshold" : 10,
                "percentPosition": true,
                "pageDots": true,
                "rightToLeft": false,
                "draggable": true,
                "selectedAttraction": 0.1,
                "parallax" : 0,
                "friction": 0.6 
            });    
            jQuery('.slider-deal').flickity({
                // options
                "imagesLoaded": true, "groupCells": "100%", "dragThreshold" : 5, "cellAlign": "left","wrapAround": true,"prevNextButtons": true,"percentPosition": true,"pageDots": false, "rightToLeft": false, "autoPlay" : 2000
            });
            jQuery('.slider-brand').flickity({
                // options
                "cellAlign": "center",
                "imagesLoaded": true,
                "lazyLoad": 1,
                "freeScroll": false,
                "wrapAround": true,
                "autoPlay": 600000,
                "pauseAutoPlayOnHover" : true,
                "prevNextButtons": false,
                "contain" : true,
                "adaptiveHeight" : true,
                "dragThreshold" : 10,
                "percentPosition": true,
                "pageDots": false,
                "rightToLeft": false,
                "draggable": true,
                "selectedAttraction": 0.1,
                "parallax" : 0,
                "friction": 0.6  
            });
    });
    jQuery('#shop-sidebar aside .widget-title').click(function(){
        jQuery(this).parent().toggleClass('active');
    });
</script>
{{--@if(setting_item('inbox_enable'))--}}
{{--    <script src="{{ asset('module/core/js/chat-engine.js?_ver='.config('app.version')) }}"></script>--}}
{{--@endif--}}
<script src="{{ asset('js/home.js?_ver='.config('app.version')) }}"></script>

@if(!empty($is_user_page))
    <script src="{{ asset('module/user/js/user.js?_ver='.config('app.version')) }}"></script>
@endif

{!! \App\Helpers\Assets::js(true) !!}

@yield('footer')

@php \App\Helpers\ReCaptchaEngine::scripts() @endphp
