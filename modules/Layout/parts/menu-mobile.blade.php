@php
            //$category = \Modules\Product\Models\ProductCategory::getCachedTree();
            $menu = (new \Modules\Core\Models\Menu())::findWithCache(2);
            if(!empty($menu))
            {
                $category = json_decode($menu->items, true);
                
            }else{
                $category = [];
            }
@endphp
<!-- Mobile Sidebar -->
            <div id="main-menu" class="mobile-sidebar no-scrollbar mfp-hide">
               <div class="sidebar-menu no-scrollbar ">
                  <ul class="nav nav-sidebar  nav-vertical nav-uppercase">
                     <div id="mega-menu-wrap"
                        class="ot-vm-hover">
                        <div id="mega-menu-title">
                           <i class="icon-menu"></i> Danh mục sản phẩm
                        </div>
                        <ul id="mega_menu" class="sf-menu sf-vertical">
                           @foreach($category as $key=> $cate)

                              <li class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-{{ $key }} {{ (isset($cate['children']) && $cate['children']) ? 'menu-item-has-children' : '' }}">
                                 <a href="{{ $cate['url'] }}" class="menu-image-title-after menu-image-not-hovered">
                                 @if(isset($cate['bg']))
                                    {!! get_image_tag($cate['bg'], 'full', ['class'=>'menu-image menu-image-title-after']) !!}
                                 @endif
                                    <span class="menu-image-title-after menu-image-title">{{ $cate['name'] }}</span>
                                 </a>
                                 @if(isset($cate['children']) && $cate['children'])
                                    <ul class="sub-menu">
                                       @foreach($cate['children'] as $key2=> $sub_menu)
                                                    <li id="menu-item-{{ $key2 }}" class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-{{ $key2 }}">
                                                        <a href="{{ $sub_menu['url'] }}">{{ $sub_menu['name'] }}</a></li>
                                                @endforeach
                                    </ul>
                                 @endif
                              </li>
                           @endforeach                     
                        </ul>
                     </div>
                  </ul>
               </div>
               <!-- inner -->
            </div>
            <!-- #mobile-menu -->