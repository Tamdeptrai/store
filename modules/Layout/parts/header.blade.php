{{--<div class="bravo-header">--}}
{{--    @if(!empty($page_style) and view()->exists('Layout::headers.style_'.$page_style->header))--}}
{{--        @include('Layout::headers.style_'.$page_style->header)--}}
{{--    @else--}}
{{--        @include('Layout::headers.default')--}}
{{--    @endif--}}
{{--</div>--}}
<header id="header" class="header has-sticky sticky-jump">
    <div class="header-wrapper">
        <div id="top-bar" class="header-top hide-for-sticky nav-dark hide-for-medium">
            <div class="flex-row container">
                <div class="flex-col hide-for-medium flex-left">
                    <ul class="nav nav-left medium-nav-center nav-small  nav-">
                    </ul>
                </div>
                <!-- flex-col left -->
                <div class="flex-col hide-for-medium flex-center">
                    <ul class="nav nav-center nav-small  nav-">
                    </ul>
                </div>
                <!-- center -->
                <div class="flex-col hide-for-medium flex-right">
                    <ul class="nav top-bar-nav nav-right nav-small  nav-">
                        <li id="menu-item-663" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-663"><a href="tel:{{ setting_item('hotline_phone') }}" class="nav-top-link">Chăm sóc khách hàng :  <span> {{ setting_item('hotline_phone') }} </span></a></li>
{{--                        <li id="menu-item-666" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-666"><a href="#" class="nav-top-link">Hệ thống cửa hàng</a></li>--}}
{{--                        <li id="menu-item-667" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-667"><a href="#" class="nav-top-link">Thanh toán</a></li>--}}
                    </ul>
                </div>
                <!-- .flex-col right -->
            </div>
            <!-- .flex-row -->
        </div>
        <!-- #header-top -->
        <div id="masthead" class="header-main hide-for-sticky nav-dark">
            <div class="header-inner flex-row container logo-left medium-logo-center" role="navigation">
                <!-- Logo -->
                <div id="logo" class="flex-col logo">
                    <!-- Header logo -->
                    <a href="{{url(app_get_locale(false,'/'))}}" title="#" rel="home">
                        @if($logo_id = setting_item("logo_id"))
                            <?php $logo = get_file_url($logo_id,'full') ?>
                            <img width="203" height="80" src="{{$logo}}" class="header_logo header-logo" alt="#"/>
                            <img  width="203" height="80" src="{{$logo}}" class="header-logo-dark" alt="#"/></a>
                        @endif
                </div>
                <!-- Mobile Left Elements -->
                <div class="flex-col show-for-medium flex-left">
                    <ul class="mobile-nav nav nav-left ">
                        <li class="nav-icon has-icon">
                            <a href="#" data-open="#main-menu" data-pos="left" data-bg="main-menu-overlay" data-color="" class="is-small" aria-label="Menu" aria-controls="main-menu" aria-expanded="false">
                                <i class="icon-menu" ></i>
                            </a>
                        </li>
                    </ul>
                </div>
                <!-- Left Elements -->
                <div class="flex-col hide-for-medium flex-left
                              flex-grow">
                    <ul class="header-nav header-nav-main nav nav-left  nav-size-large nav-spacing-large nav-uppercase" >
                        <li class="header-search-form search-form html relative has-icon">
                            <div class="header-search-form-wrapper">
                                <div class="searchform-wrapper ux-search-box relative is-normal">
                                    <form role="search" method="get" class="searchform" action="{{route('product.index')}}">
                                        <div class="flex-row relative">
                                            <div class="flex-col flex-grow">
                                                <label class="screen-reader-text" for="woocommerce-product-search-field-0">Tìm kiếm:</label>
                                                <input type="search" id="woocommerce-product-search-field-0" class="search-field mb-0" placeholder="Tìm sản phẩm" value="{{strip_tags(request()->query('s'))}}" name="s" />
                                                <input type="hidden" name="post_type" value="product" />
                                            </div>
                                            <!-- .flex-col -->
                                            <div class="flex-col search-group-btn">
                                                <button type="submit" value="Tìm kiếm" class="ux-search-submit submit-button secondary button icon mb-0">
                                                    <i class="icon-search" ></i>
                                                </button>
                                            </div>
                                            <!-- .flex-col -->
                                        </div>
                                        <!-- .flex-row -->
                                        <div class="live-search-results text-left z-top"></div>
                                    </form>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
                <!-- Right Elements -->
                <div class="flex-col hide-for-medium flex-right">
                    <ul class="bravo-extra-menu header-nav header-nav-main nav nav-right  nav-size-large nav-spacing-large nav-uppercase">
                        <li class="html custom html_topbar_left">
                            <div class="hotline">
                                <div class="hotline-icon"> <i class="fa fa-phone"></i> </div>
                                <div class="hotline-content"> <a href="tel:{{ setting_item('hotline_phone') }}">{{ setting_item('hotline_phone') }}</a> <span>Tổng đài miễn phí</span> </div>
                            </div>
                        </li>
                        <li class="account-item has-icon @if(Auth::user()) logged-in @else no-logged-in @endif">
                            @if(!Auth::user())
                                <a href="#login"
                                   data-toggle="modal" data-target="#login"
                                   class="nav-top-link nav-top-not-logged-in is-small login">
                                    <i class="icon-user" ></i>
                                    <span>Đăng nhập</span>
                                </a><!-- .account-login-link -->
                            @else
                                {{-- <a class="u-left">
                                    @if($avatar_url = Auth::user()->getAvatarUrl())
                                        <div class="avatar"><img src="{{$avatar_url}}" alt="{{Auth::user()->getDisplayName()}}"></div>
                                    @else
                                        <span class="avatar-text">{{ Auth::user()->name }}</span>
                                    @endif
                                </a> --}}
                                <div class="u-right ">
                                    <div class="dropdown">
                                        <a href="#" data-toggle="dropdown" class="login">{{ Auth::user()->name }}
                                            <i class="fa fa-angle-down"></i>
                                        </a>
                                        <ul class="user-list-action dropdown-menu text-left">

                                            <li class="menu-hr"><a href="{{route('user.orders.index')}}"><i class="fa fa-clock-o"></i> {{__("Order History")}}</a></li>
                                            <li class="menu-hr"><a href="{{ url(app_get_locale() . '/user/profile') }}"><i class="fa fa-cogs"></i> {{__("My Profile")}}</a></li>
                                            <li class="menu-hr"><a href="{{url(app_get_locale().'/user/profile/change-password')}}"><i class="fa fa-lock"></i> {{__("Change password")}}</a></li>
                                            @if(Auth::user()->hasPermissionTo('dashboard_access'))
                                                <li class="menu-hr"><a href="{{url('/admin')}}"><i class="fa fa-tachometer"></i> {{__("Admin Dashboard")}}</a></li>
                                            @endif
                                            <li class="menu-hr">
                                                <a  href="#" onclick="event.preventDefault(); document.getElementById('logout-form-topbar').submit();"><i class="fa fa-sign-out"></i> {{__('Logout')}}</a>
                                            </li>
                                        </ul>
                                        <form id="logout-form-topbar" action="{{ route('auth.logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </div>
                                </div>
                            @endif
                        </li>
                        <li class="cart-item has-icon has-dropdown user-mini-cart">
                            <a href="#" title="Giỏ hàng" class="header-cart-link is-small">
                                <i class="icon-shopping-bag"
                                   data-icon-label="{{ Cart::count() }}">
                                </i>
                            </a>
                            @include('Booking::frontend.cart.v2.mini-cart')
                            <!-- .nav-dropdown -->
                        </li>
                    </ul>
                </div>
                <!-- Mobile Right Elements -->
                <div class="flex-col show-for-medium flex-right">
                    <ul class="mobile-nav nav nav-right ">
                        <li class="cart-item has-icon">
                            @if(!Auth::user())
                            <a href="/login" class="header-cart-link off-canvas-toggle nav-top-link is-small" title="Đăng nhập">
                                <i class="icon-user"
                                   data-icon-label="0">
                                </i>
                            </a>
                            @else
                                <a href="#" class="header-cart-link off-canvas-toggle nav-top-link is-small" data-open="#user-popup" data-class="off-canvas-cart" title="Giỏ hàng" data-pos="right">
                                    <i class="icon-user"
                                       data-icon-label="0">
                                    </i>
                                </a>
                                <div id="user-popup" class="mfp-hide widget_shopping_cart">
                                    <div class="cart-popup-inner inner-padding">
                                        <div class="cart-popup-title text-center">
                                            <h4 class="uppercase">{{ Auth::user()->name }}</h4>
                                            <div class="is-divider"></div>
                                        </div>
                                        <div class="widget_shopping_cart_content">
                                            <ul class="user-list-action text-left">

                                                <li class="menu-hr"><a href="{{route('user.orders.index')}}"><i class="fa fa-clock-o"></i> {{__("Order History")}}</a></li>
                                                <li class="menu-hr"><a href="{{ url(app_get_locale() . '/user/profile') }}"><i class="fa fa-clock-o"></i> {{__("My Profile")}}</a></li>
                                                <li class="menu-hr"><a href="{{url(app_get_locale().'/user/profile/change-password')}}"><i class="fa fa-lock"></i> {{__("Change password")}}</a></li>
                                                @if(Auth::user()->hasPermissionTo('dashboard_access'))
                                                    <li class="menu-hr"><a href="{{url('/admin')}}"><i class="fa fa-tachometer"></i> {{__("Admin Dashboard")}}</a></li>
                                                @endif
                                                <li class="menu-hr">
                                                    <a  href="#" onclick="event.preventDefault(); document.getElementById('logout-form-topbar').submit();"><i class="fa fa-sign-out"></i> {{__('Logout')}}</a>
                                                </li>
                                            </ul>
                                            <form id="logout-form-topbar" action="{{ route('auth.logout') }}" method="POST" style="display: none;">
                                                {{ csrf_field() }}
                                            </form>
                                        </div>
                                        <div class="cart-sidebar-content relative"></div>
                                    </div>
                                </div>
                            @endif
                        </li>
                        <li class="cart-item has-icon">
                            <a href="{{route('booking.cart')}}" class="header-cart-link off-canvas-toggle nav-top-link is-small" title="Giỏ hàng">
                                <i class="icon-shopping-bag"
                                   data-icon-label="{{ Cart::count() }}">
                                </i>
                            </a>
                            {{-- <!-- Cart Sidebar Popup -->
                            <div id="cart-popup" class="mfp-hide widget_shopping_cart">
                                <div class="cart-popup-inner inner-padding">
                                    <div class="cart-popup-title text-center">
                                        <h4 class="uppercase">Giỏ hàng</h4>
                                        <div class="is-divider"></div>
                                    </div>
                                    <div class="widget_shopping_cart_content">

                                        <p class="woocommerce-mini-cart__empty-message">Chưa có sản phẩm trong giỏ hàng.</p>
                                    </div>
                                    <div class="cart-sidebar-content relative"></div>
                                </div>
                            </div> --}}
                        </li>

                    </ul>
                </div>
            </div>
            <!-- .header-inner -->
        </div>
        <!-- .header-main -->
        @php
            //$category = \Modules\Product\Models\ProductCategory::getCachedTree();
            $menu = (new \Modules\Core\Models\Menu())::findWithCache(2);
            if(!empty($menu))
            {
                $category = json_decode($menu->items, true);

            }else{
                $category = [];
            }
        @endphp
        <div id="wide-nav" class="header-bottom wide-nav flex-has-center">
            <div class="flex-row container">
                <div class="flex-col hide-for-medium flex-left">
                    <ul class="nav header-nav header-bottom-nav nav-left  nav-box nav-size-medium nav-uppercase">
                        <div id="mega-menu-wrap"
                             class="ot-vm-hover">
                            <div id="mega-menu-title">
                                <i class="icon-menu"></i> Danh mục sản phẩm
                            </div>
                            <ul id="mega_menu" class="sf-menu sf-vertical">
                                @foreach($category as $key=> $cate)
                                    <li id="menu-item-{{ $key }}" class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-{{ $key }} {{ (isset($cate['children']) && $cate['children']) ? 'menu-item-has-children' : '' }}">
                                        <a href="{{ $cate['url'] }}" class="menu-image-title-after menu-image-not-hovered">
                                            @if(isset($cate['bg']))
                                                {!! get_image_tag($cate['bg'], 'full', ['class'=>'menu-image menu-image-title-after']) !!}
                                            @endif
                                            <span class="menu-image-title-after menu-image-title">{{ $cate['name'] }}</span>
                                        </a>
                                        @if(isset($cate['children']) && $cate['children'])
                                            <ul class="sub-menu">
                                                @foreach($cate['children'] as $key2=> $sub_menu)
                                                    <li id="menu-item-{{ $key2 }}" class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-{{ $key2 }}">
                                                        <a href="{{ $sub_menu['url'] }}">{{ $sub_menu['name'] }}</a></li>
                                                @endforeach
                                            </ul>
                                        @endif
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </ul>
                </div>
                <!-- flex-col -->
                <div class="flex-col hide-for-medium flex-center">
                    <?php generate_menu('primary')?>
                </div>
                <!-- flex-col -->
                <div class="flex-col hide-for-medium flex-right flex-grow">
                    <ul class="nav header-nav header-bottom-nav nav-right  nav-box nav-size-medium nav-uppercase">
                    </ul>
                </div>
                <!-- flex-col -->
                <div class="flex-col show-for-medium flex-grow">
                    <ul class="nav header-bottom-nav nav-center mobile-nav  nav-box nav-size-medium nav-uppercase">
                        <li class="header-search-form search-form html relative has-icon">
                            <div class="header-search-form-wrapper">
                                <div class="searchform-wrapper ux-search-box relative is-normal">
                                    <form role="search" method="get" class="searchform" action="{{route('product.index')}}">
                                        <div class="flex-row relative">
                                            <div class="flex-col flex-grow">
                                                <label class="screen-reader-text" for="woocommerce-product-search-field-1">Tìm kiếm:</label>
                                                <input type="search" id="woocommerce-product-search-field-1" class="search-field mb-0" placeholder="Tìm sản phẩm" value="" name="s" />
                                                <input type="hidden" name="post_type" value="product" />
                                            </div>
                                            <!-- .flex-col -->
                                            <div class="flex-col">
                                                <button type="submit" value="Tìm kiếm" class="ux-search-submit submit-button secondary button icon mb-0">
                                                    <i class="icon-search" ></i>
                                                </button>
                                            </div>
                                            <!-- .flex-col -->
                                        </div>
                                        <!-- .flex-row -->
                                        <div class="live-search-results text-left z-top"></div>
                                    </form>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- .flex-row -->
        </div>
        <!-- .header-bottom -->
        <div class="header-bg-container fill">
            <div class="header-bg-image fill"></div>
            <div class="header-bg-color fill"></div>
        </div>
        <!-- .header-bg-container -->
    </div>
</header>
