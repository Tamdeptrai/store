<div id="mf-quick-view-modal" class="mf-quick-view-modal martfury-modal woocommerce" tabindex="-1" style="display: none;">
    <div class="mf-modal-overlay"></div>
    <div class="modal-content">
        <a href="#" class="close-modal">
            <i class="icon-cross"></i>
        </a>
        <div class="product-modal-content" style="display: none;">

        </div>
    </div>
    <div class="mf-loading"></div>
</div>
