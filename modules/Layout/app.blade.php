<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" class="{{$html_class ?? ''}} js">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    @php
        $favicon = setting_item('site_favicon');
    @endphp
    @if($favicon)
        @php
            $file = (new \Modules\Media\Models\MediaFile())->findById($favicon);
        @endphp
        @if(!empty($file))
            <link rel="icon" type="{{$file['file_type']}}" href="{{asset('uploads/'.$file['file_path'])}}" />
        @else:
            <link rel="icon" type="image/png" href="{{url('images/favicon.png')}}" />
        @endif
    @endif

    @include('Layout::parts.seo-meta')
    <link href="{{ asset('websieukhung/libs/font-awesome/4.7.0/css/font-awesome5697.css') }}" rel="stylesheet">
    {{-- <link href="{{ asset('websieukhung/css/dist/dashicons.min5697.css') }}" rel="stylesheet"> --}}

    <link rel='stylesheet' id='menu-icons-extra-css'  href='{{ asset('websieukhung/plugins/ot-flatsome-vertical-menu/libs/menu-icons/css/extra.min9fd1.css?v='.config('app.version')) }}' type='text/css' media='all' />
    <link rel='stylesheet' id='wp-block-library-css'  href='{{ asset('websieukhung/css/dist/block-library/style.min5697.css?v='.config('app.version')) }}' type='text/css' media='all' />
    <link rel='stylesheet' id='wc-block-vendors-style-css'  href='{{ asset('websieukhung/plugins/woocommerce/packages/woocommerce-blocks/build/vendors-stylede39.css?v='.config('app.version')) }}' type='text/css' media='all' />
    <link rel='stylesheet' id='wc-block-style-css'  href='{{ asset('websieukhung/plugins/woocommerce/packages/woocommerce-blocks/build/stylede39.css?v='.config('app.version')) }} ' type='text/css' media='all' />
    <link rel='stylesheet' id='contact-form-7-css'  href='{{ asset('websieukhung/plugins/contact-form-7/includes/css/styles7752.css?v='.config('app.version')) }}' type='text/css' media='all' />
    <link rel='stylesheet' id='devvn-quickbuy-style-css'  href='{{ asset('websieukhung/plugins/devvn-quick-buy/css/devvn-quick-buy4c71.css?v='.config('app.version')) }}' type='text/css' media='all' />
    <link rel='stylesheet' id='menu-image-css'  href='{{ asset('websieukhung/plugins/menu-image/includes/css/menu-image7d47.css?v='.config('app.version')) }}' type='text/css' media='all' />
    <link rel='stylesheet' id='ot-vertical-menu-css-css'  href='{{ asset('websieukhung/plugins/ot-flatsome-vertical-menu/assets/css/style9632.css?v='.config('app.version')) }}' type='text/css' media='all' />
    <style id='woocommerce-inline-inline-css' type='text/css'>
        .woocommerce form .form-row .required { visibility: visible; }
    </style>
{{--    <link rel="stylesheet" href="https://unpkg.com/flickity@2/dist/flickity.min.css">--}}
    <link rel='stylesheet' id='flatsome-icons-css'  href='{{ asset('websieukhung/themes/flatsome/assets/css/fl-iconsae34.css?v='.config('app.version')) }}' type='text/css' media='all' />
    <link rel='stylesheet' id='flatsome-main-css'  href='{{ asset('websieukhung/themes/flatsome/assets/css/flatsomefb6f.css?v='.config('app.version')) }} ' type='text/css' media='all' />
    <link rel='stylesheet' id='flatsome-shop-css'  href='{{ asset('websieukhung/themes/flatsome/assets/css/flatsome-shopfb6f.css?v='.config('app.version')) }}' type='text/css' media='all' />
    <link rel='stylesheet' id='flatsome-style-css'  href='{{ asset('websieukhung/themes/flatsome-child/style6aec.css?v='.config('app.version')) }}' type='text/css' media='all' />
    <link rel='stylesheet' id='flatsome-googlefonts-css'  href='http://fonts.googleapis.com/css?family=Roboto%3Aregular%2C700%2Cregular%2C500%2Cregular&amp;display=swap&amp;ver=3.9' type='text/css' media='all' />

    <link rel='stylesheet' id='ot-vertical-menu-css-css'  href='{{ asset('css/style.css?v='.config('app.version')) }}' type='text/css' media='all' />

    <link href="{{ asset('libs/jquery_ui/jquery-ui.min.css') }}" rel="stylesheet">
    <link href="{{ asset('libs/bootstrap/css/bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('libs/carousel-2/owl.carousel.css') }}" rel="stylesheet">
{{--    <link href="{{ asset('libs/font-awesome/css/font-awesome.css') }}" rel="stylesheet">--}}
{{--    <link href="{{ asset('libs/icons/css/set.css') }}" rel="stylesheet">--}}
{{--    <link href="{{ asset('libs/select2/css/select2.min.css') }}" rel="stylesheet">--}}
    <link href="{{ asset('libs/slick/slick.css') }}" rel="stylesheet">
    <link href="{{ asset('dist/frontend/css/_login_register.css?v='.config('app.version')) }}" rel="stylesheet">
    <link href="{{ asset('dist/frontend/css/_review.css?v='.config('app.version')) }}" rel="stylesheet">
{{--     <link href="{{ asset('css/app.css') }}" rel="stylesheet">--}}
{{--   <link href="{{ asset('dist/frontend/css/app.css') }}" rel="stylesheet">--}}
    <!-- Fonts -->
{{--    <script type='text/javascript' src='{{ asset('websieukhung/js/jquery/jquery4a5f.js') }}' id='jquery-core-js'></script>--}}

    {!! \App\Helpers\Assets::css() !!}
    {!! \App\Helpers\Assets::js() !!}
    <script>
        var bookingCore = {
            url:'{{url( app_get_locale() )}}',
            booking_decimals:{{(int)setting_item('currency_no_decimal',2)}},
            thousand_separator:'{{setting_item('currency_thousand')}}',
            decimal_separator:'{{setting_item('currency_decimal')}}',
            currency_position:'{{setting_item('currency_format')}}',
            currency_symbol:'{{currency_symbol()}}',
            date_format:'{{get_moment_date_format()}}',
            map_provider:'{{setting_item('map_provider')}}',
            map_gmap_key:'{{setting_item('map_gmap_key')}}',
            routes:{
                login:'{{route('auth.login')}}',
                register:'{{route('auth.register')}}',
                remove_cart_item:'{{route('booking.remove_cart_item')}}',
                view_cart:'{{route('booking.cart')}}'
            },
            currentUser:{{(int)Auth::id()}}
        };
        var Bravo = {
            url:'{{url( app_get_locale() )}}',
            url_root:'{{ url('') }}',
            booking_decimals:{{(int)get_current_currency('currency_no_decimal',2)}},
            thousand_separator:'{{get_current_currency('currency_thousand')}}',
            decimal_separator:'{{get_current_currency('currency_decimal')}}',
            currency_position:'{{get_current_currency('currency_format')}}',
            currency_symbol:'{{currency_symbol()}}',
            currency_rate:'{{get_current_currency('rate',1)}}',
            date_format:'{{get_moment_date_format()}}',
            map_provider:'{{setting_item('map_provider')}}',
            map_gmap_key:'{{setting_item('map_gmap_key')}}',
            routes:{
                login:'{{route('auth.login')}}',
                register:'{{route('auth.register')}}',
                add_to_cart:'{{route('booking.addToCart')}}'
            },
            currentUser:{{(int)Auth::id()}},
            variations: [],
            currentVariation: [],
            compare_count: ''
        };
        var i18n = {
            warning:"{{__("Warning")}}",
            success:"{{__("Success")}}",
            in_stock: "{{__('In Stock')}}",
            out_stock: "{{__('Out Of Stock')}}",
            num_stock: "{{__('__num__ In Stock')}}",
            delete_cart_item_confirm:"{{__("Do you want to delete this cart item?")}}",
            add_compare: "{{__('Compare')}}",
            browse_compare: "{{__('Browse compare')}}"
        };
    </script>
    <!-- Styles -->
    @yield('head')
    {{--Custom Style--}}
    <link href="{{ route('core.style.customCss') }}" rel="stylesheet">
    <link href="{{ asset('libs/carousel-2/owl.carousel.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('libs/swiper/swiper.css') }}">
    <script src="{{ asset('libs/swiper/swiper.min.js') }}"></script>
</head>
@php $page_style = (!empty($p_style)) ? json_decode($p_style) : json_decode(setting_item('homepage_style')); @endphp
<body class="{{$body_class ?? ''}}  yith-wcan-pro header-shadow lightbox nav-dropdown-has-arrow mobile-submenu-slide mobile-submenu-slide-levels-2 mobile-submenu-toggle">
    {!! clean(setting_item('body_scripts')) !!}
    <div id="wrapper">
        @include('Layout::parts.header')

{{--        @if(isset($show_breadcrumb) && $show_breadcrumb == 0)--}}
{{--            @include('Layout::parts.bc')--}}
{{--        @endif--}}
        @yield('content')
{{--        <main id="main" class="">--}}
{{--            <div id="content" role="main" class="content-area">--}}

{{--            </div>--}}
{{--        </main>--}}
        @include('Layout::parts.footer')
    </div>
    @include('Layout::parts.menu-mobile')
    {!! clean(setting_item('footer_scripts')) !!}
</body>
</html>
