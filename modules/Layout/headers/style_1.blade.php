<div class="header-style-1 header-style-default">
    <div class="bravo-container container">
        @include('Layout::headers.parts.main-header')
    </div>
    <div class="bravo-main-menu-wrap">
        <div class="bravo-container container">
            @include('Layout::headers.parts.main-menu')
        </div>
    </div>
</div>
