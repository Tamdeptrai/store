<div class="copy-right footer-bottom">
    <div class="row footer-row">
        <div class="col-footer-copyright col-lg-5 col-md-12 col-sm-12 col-xs-12">
            <div class="footer-copyright">{!! setting_item_with_lang("footer_text_left") ?? ''  !!}</div>

        </div>
        <div class="col-footer-payments col-lg-7 col-md-12 col-sm-12 col-xs-12">
            <div class="footer-payments">
                {!! setting_item_with_lang("footer_text_right") ?? ''  !!}
            </div>
        </div>
    </div>
</div>
