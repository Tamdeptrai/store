@extends('layouts.app')
@section('head')
    <link href="{{ asset('module/news/css/news.css?_ver='.config('app.version')) }}" rel="stylesheet">
   
@endsection
@section('content')
    <main id="main" class="archive">
        <div id="content" role="main" class="blog-wrapper blog-single page-wrapper">

                @php
                    $title_page = setting_item("news_page_list_title");
                    if(!empty($custom_title_page)){
                        $title_page = $custom_title_page;
                    }
                @endphp
                <div class="container">
                    <div class="row row-small ">
                        <div class="post-sidebar large-3 col1">
                            @include('News::frontend.layouts.details.v2.news-sidebar')
                        </div>
                        <div class="large-9 bg-wh-a col1 medium-col-first article single-post">
                            <header class="archive-page-header">
                                  <div class="row row-small">
                                     <div class="large-12 col">
                                        <h1 class="page-title is-large uppercase">
                                        </h1>
                                     </div>
                                  </div>
                               </header>
                               <article class="post type-post status-publish format-standard has-post-thumbnail hentry category-blog-tin-tuc category-khuyen-mai">
                                  <div class="article-inner ">
                                     <header class="entry-header">
                                        <div class="entry-header-text entry-header-text-top text-left">
                                           {{-- <h6 class="entry-category is-xsmall">
                                              <a href="#" rel="category tag">Blog Tin Tức</a>, <a href="#" rel="category tag">Khuyến Mãi</a>
                                           </h6> --}}
                                           <h1 class="entry-title">{{$translation->title}}</h1>
                                           <div class="entry-divider is-divider small"></div>
                                        </div>
                                        <!-- .entry-header -->
                                     </header>
                                     <!-- post-header -->
                                     <div class="entry-content single-page entry-footer">
                                        {!! clean($translation->content) !!}<div class="blog-share text-center">
                                       <div class="is-divider medium "></div>
                                         <div class="footer-socials">
                                            <div class="social-links">
                                                <a class="share-facebook martfury-facebook"
                                                   title="{{__("Facebook")}}"
                                                   href="https://www.facebook.com/sharer/sharer.php?u={{$row->getDetailUrl()}}&amp;title={{$translation->title}}"
                                                   target="_blank">
                                                    <i class="fa fa-facebook"></i>
                                                </a>
                                                <a class="share-twitter martfury-twitter"
                                                   href="https://twitter.com/share?url={{$row->getDetailUrl()}}&amp;title={{$translation->title}}"
                                                   title="{{__("Twitter")}}"
                                                   target="_blank">
                                                    <i class="fa fa-twitter" aria-hidden="true"></i>
                                                </a>
                                                <a class="share-google-plus martfury-google-plus"
                                                   title="{{__("Google Plus")}}"
                                                   href="https://plus.google.com/share?url={{$row->getDetailUrl()}}&amp;text={{$translation->title}}"
                                                   target="_blank">
                                                    <i class="fa fa-google-plus"></i>
                                                </a>
                                                <a class="share-linkedin martfury-linkedin"
                                                   href="http://www.linkedin.com/shareArticle?url={{$row->getDetailUrl()}}&amp;title={{$translation->title}}"
                                                   title="{{__("Linkedin")}}"
                                                   target="_blank">
                                                    <i class="fa fa-linkedin" aria-hidden="true"></i>
                                                </a>
                                                <a class="share-vkontakte martfury-vkontakte"
                                                   href="http://vk.com/share.php?url={{$row->getDetailUrl()}}&amp;title={{$translation->title}}"
                                                   title="{{__('Vkontakte')}}"
                                                   target="_blank">
                                                    <i class="fa fa-vk"></i>
                                                </a>
                                                <a class="share-pinterest martfury-pinterest"
                                                   href="http://pinterest.com/pin/create/button?url={{$row->getDetailUrl()}}&amp;description={{$translation->title}}"
                                                   title="{{__('Pinterest')}}"
                                                   target="_blank">
                                                    <i class="fa fa-pinterest-p" aria-hidden="true"></i>
                                                </a>
                                            </div>
                                        </div>
                                      </div>                                    
                                     </div>
                                     <!-- .entry-content2 -->
                                     @if(!empty($row->getAuthor))
                                       <div class="entry-author author-box">
                                          <div class="flex-row align-top">
                                             <div class="flex-col mr circle">
                                                <div class="blog-author-image">
                                                   <img alt='' src='http://0.gravatar.com/avatar/3243ac252c2ef1966407d277ecb2f052?s=90&amp;d=mm&amp;r=g' srcset='http://0.gravatar.com/avatar/3243ac252c2ef1966407d277ecb2f052?s=180&#038;d=mm&#038;r=g 2x' class='avatar avatar-90 photo' height='90' width='90' loading='lazy'/>                
                                                </div>
                                             </div>
                                             <!-- .flex-col -->                                             
                                               <div class="flex-col flex-grow">
                                                  <h5 class="author-name uppercase pt-half">
                                                     {{$row->getAuthor->getDisplayName() ?? ''}}              
                                                  </h5>
                                                  <p class="author-desc small"></p>
                                               </div>                                           
                                             <!-- .flex-col -->
                                          </div>
                                       </div>
                                     @endif
                                     <nav role="navigation" id="nav-below" class="navigation-post">
                                        <div class="flex-row next-prev-nav bt bb">
                                           <div class="flex-col flex-grow nav-prev text-left">
                                              <div class="nav-previous">
                                                @if($prev_page)
                                                  <a href="{{ $prev_page->getDetailUrl() }}" rel="prev"><span class="hide-for-small"><i class="icon-angle-left" ></i></span>{{ $prev_page->title }}</a>
                                                @endif
                                              </div>
                                           </div>
                                           <div class="flex-col flex-grow nav-next text-right">
                                              <div class="nav-next">
                                                @if($next_page)
                                                  <a href="{{ $next_page->getDetailUrl() }}" rel="next">{{ $next_page->title }}<span class="hide-for-small"><i class="icon-angle-right" ></i></span></a>
                                                @endif
                                              </div>
                                           </div>
                                        </div>
                                     </nav>
                                  </div>
                                  <!-- .article-inner -->
                               </article>
                        </div>
                    </div>            
                </div>
        </div>
    </main>
@endsection


