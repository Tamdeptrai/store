@extends('layouts.app')
@section('head')
    <link href="{{ asset('module/news/css/news.css?_ver='.config('app.version')) }}" rel="stylesheet">

@endsection
@section('content')
    <main id="main" class="archive">
        <div id="content" role="main" class="blog-wrapper blog-single page-wrapper">

            @php
                $title_page = setting_item_with_lang("news_page_list_title");
                if(!empty($custom_title_page)){
                    $title_page = $custom_title_page;
                }
            @endphp
            <div class="container">
                <div class="row row-small ">
                    <div class="post-sidebar large-3 col1">
                        @include('News::frontend.layouts.details.v2.news-sidebar')
                    </div>
                    <div class="large-9 bg-wh-a col1 medium-col-first article single-post">
                        <header class="archive-page-header">
                            <div class="row row-small">
                                <div class="large-12 col">
                                    <h1 class="page-title is-large uppercase">
                                    </h1>
                                </div>
                            </div>
                        </header>
                        @if($rows->count() > 0)
                        <div class="row large-columns-1 medium-columns- small-columns-1">
                            @foreach($rows as $row)
                                @php $translation = $row->translateOrOrigin(app()->getLocale()); @endphp
                                    <div class="col post-item">
                                        <div class="col-inner">
                                            <a href="{{$row->getDetailUrl()}}" class="plain">
                                                <div class="box box-vertical box-text-bottom box-blog-post has-hover">
                                                    <div class="box-image" style="width:30%;">
                                                        <div class="image-cover" style="padding-top:56%;">
                                                            @if($image_tag = get_image_tag($row->image_id,'full',['class'=>'attachment-medium size-medium wp-post-image']))
                                                                {!! $image_tag !!}
                                                            @else
                                                                <img src="{{asset('images/image-error.jpg')}}" class="attachment-medium size-medium wp-post-image" alt="Image empty">
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="box-text text-left">
                                                        <div class="box-text-inner blog-post-inner">
                                                            <h5 class="post-title is-large ">{{$translation->title}}</h5>
                                                            <div class="post-meta is-small op-8">{{ display_date($row->updated_at)}}</div>
                                                            <div class="is-divider"></div>
                                                            <p class="from_the_blog_excerpt ">
                                                                {!! clean(get_exceprt($row->content,210,"...")) !!}
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                            @endforeach
                        </div>
                        @else
                            <div class="alert alert-danger">
                                {{__("No data.")}}
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection


