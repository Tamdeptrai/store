<div id="secondary" class="widget-area " role="complementary">
    {{-- <aside id="nav_menu-11" class="widget widget_nav_menu">
        <span class="widget-title "><span>Danh mục tin tức</span></span>
        <div class="is-divider small"></div>
        <div class="menu-menu-chinh-container">
            @include('News::frontend.layouts.sidebars.v2.category')
        </div>
    </aside> --}}                            
                              @if(!empty($related))
                                  <aside id="flatsome_recent_posts-3" class="widget flatsome_recent_posts">
                                     <span class="widget-title "><span>BÀI VIẾT MỚI NHẤT</span></span>
                                     <div class="is-divider small"></div>
                                     <ul>
                                        @foreach($related as $item)
                                            <li class="recent-blog-posts-li">
                                               <div class="flex-row recent-blog-posts align-top pt-half pb-half">
                                                  <div class="flex-col mr-half">
                                                     <div class="badge post-date  badge-outline">
                                                        
                                                        {!! get_image_tag($item->image_id,'full',['class'=>'badge-inner bg-fill']) !!}
                                                     </div>
                                                  </div>
                                                  <div class="flex-col flex-grow">
                                                     <a href="{{$item->getDetailUrl()}}" title="{{$item->title}}">{{$item->title}}</a>
                                                     <span class="post_comments op-7 block is-xsmall"><a href="#"></a></span>
                                                  </div>
                                               </div>
                                            </li>
                                        @endforeach
                                     </ul>
                                  </aside>
                              @endif
 </div>