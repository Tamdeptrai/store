<section class="section san-pham-danh-muc  tintuc container">
    <div class="section-content relative">
        <div class="row row-small"  >
            <div  class="col sanpham small-12 large-12"  >
                <div class="col-inner"  >
                    <div class="row row-collapse align-middle tieude"  >
                        <div class="col medium-4 small-12 large-4"  >
                            <div class="col-inner"  >
                                <h2><a href="#" >@if($title) {{$title}} @endif</a></h2>
                            </div>
                        </div>
                    </div>
                    <div class="row row-collapse" >
                        @if($rows)
                                <div class=" medium-8 small-12 large-8"  >
                                        <div class="col-inner"  >
                                            <div class="row large-columns-1 medium-columns-1 small-columns-1 row-collapse">
                                                @foreach($rows as $key => $row)
                                                    @if($key == 0)
                                                        <div class="col post-item" >
                                                            <div class="col-inner">
                                                                    <div class="box box-vertical box-text-bottom box-blog-post has-hover">
                                                                        <div class="box-image" style="width:37%;">
                                                                            <div class="image-cover" style="padding-top:60%;">
                                                                                <a href="{{$row->getDetailUrl()}}" class="plain">
                                                                                @if($row->image_id)
                                                                                    @if(!empty($disable_lazyload))
                                                                                        <img src="{{get_file_url($row->image_id,'medium')}}" class=" attachment-medium size-medium wp-post-image" alt="{{$translation->name ?? ''}}">
                                                                                    @else
                                                                                        {!! get_image_tag($row->image_id,'medium',['class'=>' attachment-medium size-medium wp-post-image','alt'=>$row->title]) !!}
                                                                                    @endif
                                                                                @endif
                                                                                </a>
                                                                            </div>
                                                                        </div>
                                                                        <div class="box-text text-left big-box" >
                                                                            <div class="box-text-inner blog-post-inner">
                                                                                <h5 class="post-title is-large ">
                                                                                    <a href="{{$row->getDetailUrl()}}" class="plain">{{ $row->title }}</a></h5>
                                                                                <div class="post-meta is-small op-8">{{ display_date($row->updated_at)}}</div>
                                                                                <div class="is-divider"></div>
                                                                                <p class="from_the_blog_excerpt ">{!! clean(get_exceprt($row->content,70,"...")) !!}</p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                            </div>
                                                        </div>
                                                    @endif
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>

                            <div class="col1 list-no-img medium-4 small-12 large-4"  >
                                <div class="col-inner"  >
                                    <div class="row large-columns-1 medium-columns-1 small-columns-1 row-collapse">
                                        @foreach($rows as $key2 => $row)
                                            @if($key2 > 0)
                                                <div class="col post-item" >
                                                    <div class="col-inner">
                                                        <div class="plain">
                                                            <div class="box box-vertical box-text-bottom box-blog-post has-hover">
                                                                <div class="box-image">
                                                                    <div class="image-cover" style="padding-top:56.25%;"></div>
                                                                </div>
                                                                <div class="box-text text-left">
                                                                    <div class="box-text-inner blog-post-inner">
                                                                        <h5 class="post-title is-large ">
                                                                            <a href="{{$row->getDetailUrl()}}">{{ $row->title }}</a>
                                                                        </h5>
                                                                        <div class="is-divider"></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endif
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
{{--<div class="bravo-list-news">--}}
{{--    <div class="container">--}}
{{--        @if($title)--}}
{{--            <div class="title">--}}
{{--                {{$title}}--}}
{{--                @if(!empty($desc))--}}
{{--                    <div class="sub-title">--}}
{{--                        {{$desc}}--}}
{{--                    </div>--}}
{{--                @endif--}}
{{--            </div>--}}
{{--        @endif--}}
{{--        <div class="list-item">--}}
{{--            <div class="row">--}}
{{--                @foreach($rows as $row)--}}
{{--                    <div class="col-lg-4 col-md-6">--}}
{{--                        @include('News::frontend.blocks.list-news.loop')--}}
{{--                    </div>--}}
{{--                @endforeach--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--</div>--}}
