<?php
namespace Modules\Core\Walkers;
class MenuWalker
{
    protected static $currentMenuItem;
    protected        $menu;

    public function __construct($menu)
    {
        $this->menu = $menu;
    }

    public function generate()
    {
        $items = json_decode($this->menu->items, true);
        if (!empty($items)) {
            echo '<ul class="nav header-nav header-bottom-nav nav-center  nav-box nav-size-medium nav-uppercase main-menu-ul menu-generated">';
            $this->generateTree($items);
            echo '</ul>';
        }
    }

    public function generateTree($items = [], $class_li = '')
    {

        foreach ($items as $item) {

            $class = $item['class'] ?? '';
            $url = $item['url'] ?? '';

            $item['target'] = $item['target'] ?? '';
            if (!isset($item['item_model']))
                continue;
            if (class_exists($item['item_model'])) {
                $itemClass = $item['item_model'];
                if(is_callable([$itemClass,'findWithCache'])){
                    $itemObj = call_user_func([$itemClass,'findWithCache'],$item['id']);
                }else {
                    $itemObj = $itemClass::find($item['id']);
                }
                if (empty($itemObj)) {
                    continue;
                }
                $url = $itemObj->getDetailUrl();
            }

            if ($this->checkCurrentMenu($item, $url))
                $class .= ' active';


            if (!empty($item['children'])) {
                $class.=' has-dropdown menu-item-has-children';
            }

            if(!empty($item['layout']) and $item['layout'] == 'multi_row'){
                $class.=' is-mega-menu';
            }
            if(empty($class_li))
            {
                printf('<li class="menu-item menu-item-type-post_type menu-item-object-page %s">', e($class));
            }else{
                printf('<li class="%s %s">', e($class), e($class_li));
            }

            if (!empty($item['children'])) {
                printf('<a  target="%s" href="%s" class="nav-top-link">%s <i class="icon-angle-down"></i></a>', e($item['target']), e($url), e($item['name']));
            }else{
                printf('<a  target="%s" href="%s" class="nav-top-link">%s</a>', e($item['target']), e($url), e($item['name']));
            }

            if (!empty($item['children'])) {
                echo '<ul class="children-menu dropdown-submenu sub-menu nav-dropdown nav-dropdown-default">';
                if(!empty($item['layout']) and $item['layout'] == 'multi_row'){
                    $this->generateMultiRowTree($item['children']);
                }else{
                    $this->generateTree($item['children']);
                }
                echo "</ul>";
            }
            echo '</li>';
        }
    }
    public function generateMultiRowTree($items = [])
    {
        echo '<li class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-has-children nav-dropdown-col">';
            echo '<div class="mega-menu-content">';
                echo '<div class="row flex-nowrap">';
                        foreach ($items as $item) {

                            $class = $item['class'] ?? '';
                            $url = $item['url'] ?? '';
                            $item['target'] = $item['target'] ?? '';
                            if (!isset($item['item_model']))
                                continue;
                            if (class_exists($item['item_model'])) {
                                $itemClass = $item['item_model'];
                                $itemObj = $itemClass::find($item['id']);
                                if (empty($itemObj)) {
                                    continue;
                                }
                                $url = $itemObj->getDetailUrl();
                            }

                            if ($this->checkCurrentMenu($item, $url))
                                $class .= ' active';


                            if (!empty($item['children'])) {
                                $class.=' menu-item-has-children';
                            }

                            printf('<div class="%s mega-menu-col col">', e($class));
                            echo '<div class="menu-item-mega">';
                                printf('<a  target="%s" href="%s" class="">%s</a>', e($item['target']), e($url), e($item['name']));
                                if (!empty($item['children'])) {
                                    echo '<div class="mega-menu-submenu">';
                                        echo '<ul class="sub-menu nav-column nav-dropdown-default">';
                                            $this->generateTree($item['children'], 'menu-item menu-item-type-taxonomy menu-item-object-product_cat 1' );
                                        echo "</ul>";
                                    echo "</div>";
                                }
                                echo '</div>';
                            echo '</div>';
                        }

                echo '</div>';
            echo '</div>';
        echo '</li>';
    }


    protected function checkCurrentMenu($item, $url = '')
    {

        if (!static::$currentMenuItem)
            return false;
        if (empty($item['item_model']))
            return false;
        if (is_string(static::$currentMenuItem) and ($url == static::$currentMenuItem or $url == url(static::$currentMenuItem))) {
            return true;
        }
        if (is_object(static::$currentMenuItem) and get_class(static::$currentMenuItem) == $item['item_model'] && static::$currentMenuItem->id == $item['id']) {
            return true;
        }
        return false;
    }

    public static function setCurrentMenuItem($item)
    {
        static::$currentMenuItem = $item;
    }

    public static function getActiveMenu()
    {
        return static::$currentMenuItem;
    }
}
