<section class="section doitac container">
   <div class="bg section-bg fill bg-fill  bg-loaded">
   </div>
   <div class="section-content relative">
      <div class="row row-collapse align-middle align-center bg-wh">
         <div class="col-md-3 medium-2 small-12 large-2">
            <div class="col-inner">
               <p>{{$title ?? ''}}</p>
            </div>
         </div>
         <div class="col-md-9 medium-10 small-12 large-10">
            <div class="col-inner">
               <div class="slider-wrapper relative">
                  <div class="slider-brand slider slider-nav-circle slider-nav-large slider-nav-light slider-style-normal is-draggable flickity-enabled">
                     
                           @if(!empty($item))
                               @foreach($item as $row)
                                   <div class="ux-logo has-hover align-middle ux_logo inline-block" style="" aria-selected="false">
                                      <div class="ux-logo-link block image-" title="" href="" style="padding: 15px;">
                                        {!! get_image_tag($row['image'],'thumb',['class'=>'ux-logo-image block','alt'=>$row['title'],'style'=>'height:25px;']) !!}
                                        
                                    </div>
                                   </div>
                               @endforeach
                           @endif
                  </div>
                  <div class="loading-spin dark large centered" style="display: none;"></div>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>


