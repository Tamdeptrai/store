<section class="section san-pham-danh-muc container">
    <div class="section-content relative">
        <div class="row row-small">
            @if(!empty($item))
                @foreach($item as $key=>$list)
                    @if($colItem == "big_and_small")

                    @else
                        <div  class="col medium-4 small-12 large-4"  >
                            <div class="col-inner"  >
                                <div class="img has-hover x md-x lg-x y md-y lg-y" id="image_2054201499">
                                    <div class="img-inner dark" >
                                        {!! get_image_tag($list['image'], 'full') !!}
                                    </div>
                                    <style>
                                        #image_2054201499 {
                                            width: 100%;
                                        }
                                    </style>
                                </div>
                            </div>
                        </div>
                    @endif
                @endforeach
            @endif
        </div>
    </div>
    <style>
        #section_188698148 {
            padding-top: 0px;
            padding-bottom: 0px;
        }
    </style>
</section>
{{--<div class="bravo_Promotion">--}}
{{--    <div class="martfury-container">--}}
{{--        <div class="row">--}}
{{--            @if(!empty($item))--}}
{{--                @foreach($item as $key=>$list)--}}
{{--                    @if($colItem == "big_and_small")--}}
{{--                        <div class="col-md-{{ $key == 0 ? "8" : "4" }}">--}}
{{--                            @if($key == 0)--}}
{{--                                @include("Template::frontend.blocks.Promotion.parts.bigandsmall")--}}
{{--                            @else--}}
{{--                                @include("Template::frontend.blocks.Promotion.parts.loop")--}}
{{--                            @endif--}}
{{--                        </div>--}}
{{--                    @else--}}
{{--                        <div class="col-sm-12 col-lg-{{$colItem ?? '4'}} col-md-6 col-xs-12">--}}
{{--                            @include("Template::frontend.blocks.Promotion.parts.loop")--}}
{{--                        </div>--}}
{{--                    @endif--}}
{{--                @endforeach--}}
{{--            @endif--}}
{{--        </div>--}}
{{--    </div>--}}
{{--</div>--}}
