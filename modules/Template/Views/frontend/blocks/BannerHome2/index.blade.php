<div class="container1 " style="padding-top: 40px;padding-bottom: 40px;">
    <div class="duan-home-wrapper">
        <div class="c20"></div>
        <div class="grid">

            <h2 class="title-cat-home" style="text-align: center;">
                <a style="color: #FFF; "
                   href="/du-an-tieu-bieu/"> Dự án tiêu
                    biểu </a></h2>

            <div class="c10"></div>
            <div class="cat-home-intro">
                <div>Thành công nối tiếp thành công, TBV ngày càng nhận được các công trình thi công tầm cỡ về bếp công nghiệp,  bếp nhà hàng, bếp khách sạn, thiết bị giặt là công nghiệp trên khắp cả nước.
                </div>
            </div>

            <div class="c20"></div>
            <div class="duan-home-group" style="">
                <div class="c10"></div>
                <div class="duan-next btn-next"><img src="/images/btn-next.png"/></div>
                <div class="duan-prev btn-prev"><img src="/images/btn-prev.png"/></div>
                <div class="swiper-container" id="duanHome">
                    <div class="c10"></div>
                    <div class="c30"></div>
                    <div class="swiper-wrapper">
                        @if(!empty($sliders))
                            @foreach($sliders as $item)
                                <div class="swiper-slide">
                                    <div class="duan-group" style=" ">
                                        <div class="img-duan">
                                            <a
                                                href="{!! clean($item['link']) !!}">
                                                {!! get_image_tag($item['image'],'medium',['lazy'=>false]) !!}
                                            </a>
                                        </div>
                                        <div class="c10"></div>

                                        <div class="duan-home-active">
                                            <h3 class="duan-home-name">
                                                <a
                                                    href="{!! clean($item['link']) !!}">Thi
                                                    {{ $item['title'] }}</a></h3>
                                            <div class="c10"></div>
                                            <div style="text-align:center;">
                                                {{ $item['content'] }}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @endif                        
                    </div>
                </div>
            </div>
            <div class="c20"></div>            
        </div>
        <div class="c10"></div>
    </div>
</div>
