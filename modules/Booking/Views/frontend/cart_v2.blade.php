@extends('layouts.app')
@section('head')
{{--    <link href="{{ asset('module/booking/css/checkout.css?_ver='.config('app.version')) }}" rel="stylesheet">--}}
    <style type="text/css">
            .cart .col1{
                padding: 0 30px 30px;
            }
            .cart .cart-collaterals{
                border-left: 1px solid #ececec;
            }
    </style>
@endsection
@section('content')
<main id="main" class="archive cart">
        <div id="content" role="main" class="content-area page-wrapper">
            <div class="container">
                <div class="row row-main">
                    <div class="large-12 col">
                        <div class="col-inner">
                            <div class="woocommerce">
                                <div class="woocommerce-notices-wrapper">
                                    @if(isset($message['class']))
                                        <ul class=" message-wrapper {{ $message['class'] == 'bravo-error' ? 'woocommerce-error' : 'woocommerce-message' }}" role="alert">
                                            <li>
                                                <div class="message-container container alert-color medium-text-center">
                                                    <span class="message-icon icon-close"></span>
                                                    {{ $message['text'] ?? '' }}
                                                </div>
                                            </li>
                                        </ul>
                                    @endif
                                </div>

                                @if(Cart::count() > 0)
                                    <div class="woocommerce row row-large row-divided">
                                        <div class="col1 large-7 pb-0 cart-auto-refresh">
                                            @include ('Booking::frontend.cart.v2.form')
                                        </div>
                                        <div class="cart-collaterals large-5 col1 pb-0">
                                            <div class="cart-sidebar col-inner ">
                                                <div class="cart_totals calculated_shipping">
                                                    <table cellspacing="0">
                                                        <thead>
                                                        <tr>
                                                            <th class="product-name" colspan="2" style="border-width:3px;">Cộng giỏ hàng</th>
                                                        </tr>
                                                        </thead>
                                                    </table>
                                                    <h2>Cộng giỏ hàng</h2>
                                                    <table cellspacing="0" class="shop_table shop_table_responsive">
                                                        <tbody>
                                                        <tr class="cart-subtotal">
                                                            <th>Tạm tính</th>
                                                            <td data-title="Tạm tính"><span class="woocommerce-Price-amount amount">{{number_format(Cart::subtotal())}}<span class="woocommerce-Price-currencySymbol">₫</span></span></td>
                                                        </tr>
                                                        <tr class="order-total">
                                                            <th>Tổng</th>
                                                            <td data-title="Tổng"><strong><span class="woocommerce-Price-amount amount">{{number_format(Cart::final_total())}}<span class="woocommerce-Price-currencySymbol">₫</span></span></strong> </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                    <div class="wc-proceed-to-checkout">
                                                        <a href="{{route('booking.checkout')}}" class="checkout-button button alt wc-forward">
                                                            Tiến hành thanh toán</a>
                                                    </div>
                                                </div>
                                                <form class="checkout_coupon mb-0" action="{{ route('booking.cart') }}" method="post">
                                                    @csrf
                                                    <div class="coupon">
                                                        <h3 class="widget-title"><i class="icon-tag"></i> Phiếu ưu đãi</h3>
                                                        <input type="text" name="coupon" class="input-text mb-2" id="coupon_code" value="" placeholder="Mã ưu đãi" required>
                                                        <input type="submit" class="is-form expand" name="apply_coupon" value="Áp dụng">
                                                    </div>
                                                </form>
                                                <div class="cart-sidebar-content relative"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="cart-footer-content after-cart-content relative"></div>
                                @else
                                    <p class="cart-empty woocommerce-info">Chưa có sản phẩm nào trong giỏ hàng.</p>
                                    <p class="return-to-shop">
                                        <a class="button primary wc-backward" href="{{url('/')}}">Quay trở lại cửa hàng</a>
                                    </p>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</main>
@endsection
