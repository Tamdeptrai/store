@extends('layouts.app')
@section('head')
    <link href="{{ asset('module/booking/css/checkout.css?_ver='.config('app.version')) }}" rel="stylesheet">
@endsection
@section('content')
    <main id="main" class="archive">
        <div id="content" role="main" class="content-area">
            <div class="bravo-booking-page padding-content" style="padding-top: 20px;">
                <div class="container">
                    <div class="row booking-success-notice">
                        <div class="col-lg-8 col-md-8">
                            <div class="d-flex align-items-center">
                                <img src="{{url('images/ico_success.svg')}}" alt="Payment Success">
                                <div class="notice-success">
                                    <p class="line1"><span>{{$booking->first_name}},</span>
                                        Cảm ơn bạn. Đơn đặt hàng của bạn đã được gửi thành công
                                    </p>
                                    <p class="line2">Chi tiết đơn hàng đã được gửi đến: <span>{{$booking->email}}</span></p>
                                </div>

                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4">
                            <ul class="booking-info-detail">
                                <li><span>Mã đơn hàng :</span> {{$booking->id}}</li>
                                <li><span>Ngày :</span> {{display_date($booking->created_at)}}</li>
                                @if(!empty($gateway))
                                    <li><span>Phương thức thanh toán :</span> {{$gateway->name}}</li>
                                @endif
                            </ul>
                        </div>
                    </div>
                    <div class="row booking-success-detail">
                        <div class="col-md-8">
                            @include ('Booking::frontend/booking/v2/order-information')
{{--                            @if(\Illuminate\Support\Facades\Auth::id())--}}
{{--                                <div class="text-center">--}}
{{--                                    <a href="{{url(app_get_locale().'/user/booking-history')}}" class="btn btn-primary">{{__('Order History')}}</a>--}}
{{--                                </div>--}}
{{--                            @endif--}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
    @php @endphp
@endsection
@section('footer')
@endsection
