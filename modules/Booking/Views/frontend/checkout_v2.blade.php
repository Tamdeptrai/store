@extends('layouts.app')
@section('head')
    {{--    <link href="{{ asset('module/booking/css/checkout.css?_ver='.config('app.version')) }}" rel="stylesheet">--}}
@endsection
@section('content')
    <main id="main" class="archive">
        <div id="content" role="main" class="content-area page-wrapper">
            <div class="container">
                <div id="bravo-checkout-page" class="row row-main">
                    <div class="large-12 col">
                        <div class="col-inner">
                            <div class="woocommerce">
                                <div class="woocommerce-notices-wrapper"></div>
{{--                                <div class="woocommerce-form-coupon-toggle">--}}
{{--                                    <div class="woocommerce-info message-wrapper">--}}
{{--                                        <div class="message-container container medium-text-center">--}}
{{--                                            Bạn có mã ưu đãi? <a href="#" class="showcoupon">Ấn vào đây để nhập mã</a>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <form class="checkout_coupon woocommerce-form-coupon has-border is-dashed" action="{{ route('booking.cart') }}" method="post"--}}
{{--                                      style="">--}}
{{--                                    @csrf--}}
{{--                                    <p>Nếu bạn có mã giảm giá, vui lòng điền vào phía bên dưới.</p>--}}
{{--                                    <div class="coupon">--}}
{{--                                        <div class="flex-row medium-flex-wrap">--}}
{{--                                            <div class="flex-col flex-grow">--}}
{{--                                                <input type="text" name="coupon" class="input-text"--}}
{{--                                                       placeholder="Mã ưu đãi" id="coupon_code" value="">--}}
{{--                                                <input type="hidden" name="page" value="checkout">--}}
{{--                                            </div>--}}
{{--                                            <div class="flex-col">--}}
{{--                                                <button type="submit" class="button expand" name="apply_coupon"--}}
{{--                                                        value="Áp dụng">Áp dụng--}}
{{--                                                </button>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                        <!-- row -->--}}
{{--                                    </div>--}}
{{--                                    <!-- coupon -->--}}
{{--                                </form>--}}
                                <div class="woocommerce-notices-wrapper"></div>
                                <div class="checkout woocommerce-checkout ">
                                    <div class="row pt-0 ">
                                        @include ('Booking::frontend.checkout.v2.checkout-form')
                                        <!-- large-7 -->
                                        @include ('Booking::frontend.checkout.v2.detail')
                                        <!-- large-5 -->
                                    </div>
                                    <!-- row -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>

@endsection
@section('footer')
    <script src="{{ asset('module/booking/js/checkout.js') }}"></script>
@endsection
