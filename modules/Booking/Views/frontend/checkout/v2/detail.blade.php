<div class="large-5">
    <div class="col-inner has-border">
        <div class="checkout-sidebar sm-touch-scroll">
            <h3 id="order_review_heading">Đơn hàng của bạn</h3>
            <div id="order_review" class="woocommerce-checkout-review-order">
                <table class="shop_table woocommerce-checkout-review-order-table">
                    <thead>
                    <tr>
                        <th class="product-name">Sản phẩm</th>
                        <th class="product-total">Tạm tính</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(Cart::count())
                        @foreach(Cart::content() as $cartItem)
                            <tr class="cart_item">
                                <td class="product-name">
                                    {{ $cartItem->name }}
                                    <strong class="product-quantity">× {{$cartItem->qty}}</strong>
                                </td>
                                <td class="product-total">
                                   <span class="woocommerce-Price-amount amount">{{ number_format($cartItem->price) }}<span class="woocommerce-Price-currencySymbol">₫</span></span>
                                </td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                    <tfoot>
                        <tr class="cart-subtotal">
                            <th>Tạm tính</th>
                            <td><span class="woocommerce-Price-amount amount">{{ number_format(Cart::subtotal()) }}<span class="woocommerce-Price-currencySymbol">₫</span></span>
                            </td>
                        </tr>
                        @if($coupons = session('coupon'))
                            @foreach($coupons as $coupon)
                                <tr class="cart-coupon">
                                    <td class="coupon_text text-left">{{ __('Coupon: :code',['code'=>$coupon['name']]) }}</td>
                                    @php $discount = ($coupon['type'] == 'percent') ? Cart::subtotal() * $coupon['discount']/100 : $coupon['discount'] @endphp
                                    <td class="coupon_discount text-right">-{{ number_format($discount) }} đ</td>
                                </tr>
                            @endforeach
                        @endif
                        <tr class="order-total">
                            <th>Tổng</th>
                            <td><strong><span class="woocommerce-Price-amount amount">{{number_format(Cart::final_total())}}<span class="woocommerce-Price-currencySymbol">₫</span></span></strong>
                            </td>
                        </tr>
                    </tfoot>
                </table>
                @include ('Booking::frontend.checkout.v2.checkout-payment')
            </div>
            <div class="woocommerce-privacy-policy-text"></div>
        </div>
    </div>
</div>
