@php
    $user = auth()->user();
    $has_user = ($user) ? true : false;
    $shipping = (!empty(session('shipping'))) ? session('shipping') : null;
@endphp
<div class="large-7">
    <div id="customer_details">
        <div class="clear">
            <div class="woocommerce-billing-fields">
                <h3>Thông tin thanh toán</h3>
                <div class="woocommerce-billing-fields__field-wrapper">
                    <p class="form-row-wide validate-required"
                       id="billing_first_name_field">
                        <label for="billing_first_name" class="">Họ Tên <abbr
                                class="required"
                                title="bắt buộc">*</abbr></label>
                        <span class="woocommerce-input-wrapper"><input
                                type="text" class="input-text "
                                name="billing_first_name"
                                id="billing_first_name" placeholder=""
                                value="{{ $has_user ? $user->name : '' }}"
                                autocomplete="given-name"></span>
                    </p>
                    <p class="form-row-wide" id="billing_company_field"><label for="billing_company" class="">Tên
                            công ty <span
                                class="optional">(tuỳ chọn)</span></label>
                        <span class="woocommerce-input-wrapper">
                  <input type="text" class="input-text "
                         name="billing_company" id="billing_company"
                         placeholder="" value=""
                         autocomplete="organization"></span>
                    </p>

                    <p class="address-field form-row-wide  validate-required"
                       id="billing_address_1_field">
                        <label for="billing_address_1" class="">Địa chỉ <abbr
                                class="required"
                                title="bắt buộc">*</abbr></label>
                        <span class="woocommerce-input-wrapper">
                  <input type="text" class="input-text "
                         name="billing_address_1"
                         id="billing_address_1" placeholder="Địa chỉ"
                         value="{{$shipping['address'] ?? $user->address ?? ''}}"
                         autocomplete="address-line1"
                         data-placeholder="Địa chỉ">
                  </span>
                    </p>
                    <div>
                        <p class="form-row-first validate-required validate-phone"
                           id="billing_phone_field">
                            <label for="billing_phone" class="">Số điện thoại<abbr
                                    class="required"
                                    title="bắt buộc">*</abbr></label>
                            <span class="woocommerce-input-wrapper">
                  <input type="tel" class="input-text "
                         name="billing_phone" id="billing_phone"
                         placeholder="" value="{{$user->phone ?? ''}}"
                         autocomplete="tel"></span>
                        </p>
                        <p class="form-row-first validate-required validate-email"
                           id="billing_email_field">
                            <label for="billing_email" class="">Địa chỉ email<abbr
                                    class="required"
                                    title="bắt buộc">*</abbr></label>
                            <span class="woocommerce-input-wrapper">
                  <input type="email" class="input-text "
                         name="billing_email" id="billing_email"
                         placeholder=""
                         value="{{$has_user ? $user->email : ''}}"
                         autocomplete="email username">
                  </span>
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="clear">
            <div class="woocommerce-shipping-fields">
            </div>
            <div class="woocommerce-additional-fields">
                <h3>Thông tin bổ sung</h3>
                <div class="woocommerce-additional-fields__field-wrapper">
                    <p class="notes" id="order_comments_field">
                        <label for="order_comments" class="">Ghi chú đơn hàng
                            <span class="optional">(tuỳ chọn)</span></label>
                        <span class="woocommerce-input-wrapper">
                  <textarea name="order_comments" class="input-text "
                            id="order_comments"
                            placeholder="Ghi chú về đơn hàng, ví dụ: thời gian hay chỉ dẫn địa điểm giao hàng chi tiết hơn."
                            rows="2" cols="5"></textarea>
                  </span>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
