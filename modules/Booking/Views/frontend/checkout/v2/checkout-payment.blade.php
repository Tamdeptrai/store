<div id="payment" class="woocommerce-checkout-payment">
    <h3 class="form-section-title">Chọn hình thức thanh toán</h3>
    <div class="gateways-table accordion wc_payment_methods payment_methods methods" id="accordionExample" style="margin-bottom: 10px;">
        @foreach($gateways as $k=>$gateway)
            <div class="card">
                <div class="card-header">
                    <div class="mb-0">
                        <label class="" data-toggle="collapse" data-target="#gateway_{{$k}}" >
                            <input type="radio" name="payment_gateway" value="{{$k}}">
                            @if($logo = $gateway->getDisplayLogo())
                                <img src="{{$logo}}" alt="{{$gateway->getDisplayName()}}">
                            @endif
                            {{$gateway->getDisplayName()}}
                        </label>
                    </div>
                </div>
                <div id="gateway_{{$k}}" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                    <div class="card-body">
                        <div class="gateway_name">
                            {!! clean($gateway->getDisplayName()) !!}
                        </div>
                        {!! clean($gateway->getDisplayHtml()) !!}
                    </div>
                </div>
            </div>
        @endforeach
    </div>
    <div class="html_before_actions"></div>

    <p class="message-container container alert-color medium-text-center mt10" v-show=" message.content" v-html="message.content" :class="{'danger':!message.type,'success':message.type}"></p>
    <div class="form-row place-order">
        <div class="woocommerce-terms-and-conditions-wrapper">
        </div>
        <button type="submit" @click="doCheckout"
                class="button alt"
                name="woocommerce_checkout_place_order"
                id="place_order" value="Đặt hàng">Đặt hàng <i class="fa fa-spin fa-spinner" v-show="onSubmit"></i>
        </button>
    </div>
</div>
