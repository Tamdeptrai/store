@php $stt = 0; @endphp
@foreach(Cart::content() as $cartItem)
    <tr class="woocommerce-cart-form__cart-item cart_item">
        @if($cartItem->model)
            <td class="product-remove remove_cart">
                <a href="#" class="remove bravo_delete_cart_item" aria-label="Xóa sản phẩm này" data-id="{{$cartItem->rowId}}">×</a>
            </td>
            <td class="product-thumbnail">
                <a href="{{$cartItem->model->getDetailUrl()}}">
                    {!! get_image_tag($cartItem->model->image_id,'thumb',['class'=>'float-left img-120','lazy'=>false]) !!}
                </a>
            </td>
            <td class="product-name" data-title="Sản phẩm">
                <a href="{{$cartItem->model->getDetailUrl()}}">{{$cartItem->name}}</a>
                <div class="show-for-small mobile-product-price">
                    <span class="mobile-product-price__qty">{{ $cartItem->qty }} x </span>
                    <span class="woocommerce-Price-amount amount"> {{ number_format($cartItem->price) }}<span class="woocommerce-Price-currencySymbol">₫</span></span>
                </div>
            </td>
        @endif
        <td class="product-price" data-title="Giá">
            <span class="woocommerce-Price-amount amount">{{ number_format($cartItem->price) }}<span class="woocommerce-Price-currencySymbol">₫</span></span>
        </td>
        <td class="product-quantity" data-title="Số lượng">
            <div class="quantity buttons_added form-minimal">
                <input type="button" value="-" class="minus button is-form">
                <label class="screen-reader-text" for="quantity_{{$cartItem->rowId}}">Số lượng</label>
                @php $stock_total = (!empty($cartItem->model->is_manage_stock)) ? $cartItem->model->quantity : null; @endphp
                <input class="form-control input-text qty text" step="1" min="0" max="" type="number" id="quantity_{{$cartItem->rowId}}" name="product[{{$stt}}][{{$cartItem->rowId}}]" data-stock="{{$stock_total}}" inputmode="numeric" min="0" value="{{ $cartItem->qty }}">
                <input type="button" value="+" class="plus button is-form">
            </div>
        </td>
        <td class="product-subtotal" data-title="Tạm tính">
            <span class="woocommerce-Price-amount amount">{{ number_format($cartItem->qty * $cartItem->price) }}<span class="woocommerce-Price-currencySymbol">₫</span></span>
        </td>
    </tr>
    @php $stt++; @endphp
@endforeach
