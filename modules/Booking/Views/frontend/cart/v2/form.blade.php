@php $shipping = (!empty(session('shipping'))) ? session('shipping') : null @endphp
<form class="woocommerce-cart-form" action="{{route('booking.cart')}}" method="post">
    @csrf
    <div class="cart-wrapper sm-touch-scroll">
        <table class="shop_table shop_table_responsive cart woocommerce-cart-form__contents" cellspacing="0">
            <thead>
                <tr>
                    <th class="product-name" colspan="3">Sản phẩm</th>
                    <th class="product-price">Giá</th>
                    <th class="product-quantity">Số lượng</th>
                    <th class="product-subtotal">Tạm tính</th>
                </tr>
            </thead>
            <tbody>
            @include('Booking::frontend.cart.v2.list-cart')
                <tr>
                    <td colspan="6" class="actions clear">
                        <div class="continue-shopping pull-left text-left">
                            <a class="button-continue-shopping button primary is-outline" href="{{url('/')}}">
                                ←&nbsp;Tiếp tục xem sản phẩm	</a>
                        </div>
                        <button type="submit" class="button primary mt-0 pull-left small1" name="update_cart1" value="Cập nhật giỏ hàng">Cập nhật giỏ hàng</button>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</form>
