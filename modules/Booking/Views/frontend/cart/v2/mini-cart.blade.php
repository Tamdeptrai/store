<ul class="nav-dropdown nav-dropdown-default">
    <li class="html widget_shopping_cart">
        @if(Cart::count())
            <div class="widget_shopping_cart_content">
                <ul class="woocommerce-mini-cart cart_list product_list_widget ">
                    @foreach(Cart::content() as $cartItem)
                        <li class="woocommerce-mini-cart-item mini_cart_item">
                            <a href="#" class="remove remove_from_cart_button ps-product__remove bravo_delete_cart_item"
                               data-id="{{$cartItem->rowId}}" aria-label="Xóa sản phẩm này">×</a>
                            <a href="{{$cartItem->model->getDetailUrl()}}">
                                {!! get_image_tag($cartItem->model->image_id,'thumb',['lazy'=>false]) !!} {{$cartItem->name}}</a>
                                <span class="quantity">{{$cartItem->qty}} × <span class="woocommerce-Price-amount amount">{{number_format($cartItem->price)}}<span class="woocommerce-Price-currencySymbol">₫</span></span></span>
                        </li>
                    @endforeach
                </ul>
                <p class="woocommerce-mini-cart__total total">
                    <strong>Tổng số phụ:</strong> <span class="woocommerce-Price-amount amount">{{number_format(Cart::subtotal())}}<span
                            class="woocommerce-Price-currencySymbol">₫</span></span>
                </p>
                <p class="woocommerce-mini-cart__buttons buttons">
                    <a href="{{route('booking.cart')}}" class="button wc-forward">Xem giỏ hàng</a>
                    <a href="{{route('booking.checkout')}}" class="button checkout wc-forward">Thanh toán</a>
                </p>
            </div>
        @else
            <p class="woocommerce-mini-cart__empty-message">Chưa có sản phẩm trong giỏ hàng.</p>
        @endif
    </li>
</ul>


