<?php
namespace Modules\Booking\Gateways;

use Illuminate\Http\Request;
use Modules\Booking\Models\Booking;
use Gloudemans\Shoppingcart\Facades\Cart;

class BankPaymentGateway extends BaseGateway
{
    public $name = 'Bank Payment';

    public function process(Request $request, $order)
    {
        // Simple change status to processing
        $order->markAsProcessing($this);
        $order->sendNewBookingEmails();

        Cart::destroy();
        session()->forget(['coupon','shipping','tmp_order_id']);

        return response()->json([
            'url' => $order->getDetailUrl()
        ]);
    }

    public function getOptionsConfigs()
    {
        return [
            [
                'type'  => 'checkbox',
                'id'    => 'enable',
                'label' => __('Enable Bank Payment?')
            ],
            [
                'type'  => 'input',
                'id'    => 'name',
                'label' => __('Custom Name'),
                'std'   => __("Bank Payment")
            ],
            [
                'type'  => 'upload',
                'id'    => 'logo_id',
                'label' => __('Custom Logo'),
            ],
            [
                'type'  => 'editor',
                'id'    => 'html',
                'label' => __('Custom HTML Description')
            ],
        ];
    }
}
