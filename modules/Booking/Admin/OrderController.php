<?php
namespace Modules\Booking\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Modules\AdminController;
use Modules\Product\Models\Order;
use Modules\Product\Models\OrderItem;

class OrderController extends AdminController
{
    public function index(Request $request){
        $this->checkPermission('report_view');

        $status = $request->input('status');
        $orders = Order::query()->orderBy('id','desc');
        if(!empty($status))
        {
            $orders = $orders->where('status',$status);
        }
        $orders = $orders->paginate(20);
        $data = [
            'rows'=> $orders,
            'statues'=>[
                Order::COMPLETED => 'Hoàn thành',
                Order::PROCESSING =>'Đang xử lý',
                Order::CONFIRMED =>'Đã xác nhận',
                Order::CANCELLED =>'Đã hủy',
                Order::PAID => 'Đã thanh toán',
                Order::UNPAID =>'Chưa thanh toán',
            ]
        ];
        return view('Booking::admin.orders.index',$data);
    }

    public function bulkEdit(Request $request)
    {

        $ids = $request->input('ids');
        $action = $request->input('action');
        if (empty($ids) or !is_array($ids)) {
            return redirect()->back()->with('error', __('No items selected!'));
        }
        if (empty($action)) {
            return redirect()->back()->with('error', __('Please select an action!'));
        }

        if($action == Order::COMPLETED || $action == Order::PROCESSING || $action == Order::CONFIRMED || $action == Order::CANCELLED || $action == Order::PAID || $action == Order::UNPAID)
        {

        }
        switch ($action){
            case "delete":
                foreach ($ids as $id) {
                    $query = Order::where("id", $id);
                    if (!$this->hasPermission('product_manage_others')) {
                        $query->where("create_user", Auth::id());
                        $this->checkPermission('product_delete');
                    }
                    $query->first()->delete();
                }
                return redirect()->back()->with('success', __('Deleted success!'));
                break;
            case Order::COMPLETED:
                foreach ($ids as $id) {
                    $query = Order::where("id", $id)->first();
//                    if (!$this->hasPermission('product_manage_others')) {
//                        $query->where("create_user", Auth::id());
//                    }
                    if($query)
                    {
                        $query->status = Order::COMPLETED;
                        $query->save();
                    }
                }
                return redirect()->back()->with('success', __('Update success!'));
                break;
            case Order::PROCESSING:
                foreach ($ids as $id) {
                    $query = Order::where("id", $id)->first();
//                    if (!$this->hasPermission('product_manage_others')) {
//                        $query->where("create_user", Auth::id());
//                    }
                    if($query)
                    {
                        $query->status = Order::PROCESSING;
                        $query->save();
                    }
                }
                return redirect()->back()->with('success', __('Update success!'));
                break;
            case Order::CONFIRMED:
                foreach ($ids as $id) {
                    $query = Order::where("id", $id)->first();
//                    if (!$this->hasPermission('product_manage_others')) {
//                        $query->where("create_user", Auth::id());
//                    }
                    if($query)
                    {
                        $query->status = Order::CONFIRMED;
                        $query->save();
                    }
                }
                return redirect()->back()->with('success', __('Update success!'));
                break;
            case Order::CANCELLED:
                foreach ($ids as $id) {
                    $query = Order::where("id", $id)->first();
//                    if (!$this->hasPermission('product_manage_others')) {
//                        $query->where("create_user", Auth::id());
//                    }
                    if($query)
                    {
                        $query->status = Order::CANCELLED;
                        $query->save();
                    }
                }
                return redirect()->back()->with('success', __('Update success!'));
                break;
            case Order::PAID:
                foreach ($ids as $id) {
                    $query = Order::where("id", $id)->first();
//                    if (!$this->hasPermission('product_manage_others')) {
//                        $query->where("create_user", Auth::id());
//                    }
                    if($query)
                    {
                        $query->status = Order::PAID;
                        $query->save();
                    }
                }
                return redirect()->back()->with('success', __('Update success!'));
                break;
            case Order::UNPAID:
                foreach ($ids as $id) {
                    $query = Order::where("id", $id)->first();
//                    if (!$this->hasPermission('product_manage_others')) {
//                        $query->where("create_user", Auth::id());
//                    }
                    if($query)
                    {
                        $query->status = Order::UNPAID;
                        $query->save();
                    }
                }
                return redirect()->back()->with('success', __('Update success!'));
                break;
        }
        return redirect()->back();
    }
}
