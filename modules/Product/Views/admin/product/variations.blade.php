<div class="variation-actions">
    <div class="row">
        <div class="col-md-4">
            <div class="d-flex align-items-center">
                <div class="mr-3 flex-shrink-0" ><strong>{{__('Bulk Action')}}: </strong></div>
                <div class="input-group ajax-bulk-action-variations" >
                    <select class="form-control">
                        <option value="add">{{__('Add variation')}}</option>
                    </select>
                    <div class="input-group-append">
                        <button class="btn btn-info" type="button" ><i class="fa fa-hand-o-right"></i> {{__('Go')}}</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<hr>
<div class="variation-list">

</div>
<hr>
<a href="#" class="btn btn-primary btn-sm btn-save-variations">{{__('Save variations')}}</a>