@extends('layouts.app')
@section('head')
    <link rel="stylesheet" type="text/css" href="{{ asset("libs/flexslider/flexslider.css") }}"/>
    <link href="{{ asset('css/detail_v2.css?v='.config('app.version')) }}" rel="stylesheet">
    <link href="{{ asset('websieukhung/css/font-akr.css?v='.config('app.version')) }}" rel="stylesheet">
    <link href="{{ asset('websieukhung/css/pe-icon-7-stroke.min.css?v='.config('app.version')) }}" rel="stylesheet">
    <link href="{{ asset('websieukhung/libs/fotorama/fotorama.css?v='.config('app.version')) }}" rel="stylesheet">

@endsection
@section('content')
    <main id="main" class="archive">
        <div id="content" role="main" class="content-area">
            <section class="single-products section-padding">
                <div class="container">
                <div class="">
                    <div class="hd-card-body-view">
                        <div class="row">
                            <div class="col-md-5 col-sm-12 col-xs-12">
                                @if($row->getGallery())
                                    <div class="fotorama" data-maxwidth="100%"  data-autoplay="true" data-allowfullscreen="true" data-nav="thumbs">
                                        @foreach($row->getGallery() as $key=>$item)
                                            <img src="{{$item['large']}}" title="" alt="">
                                        @endforeach
                                    </div>
                                @endif
                            </div>
                            <div class="col-md-4 col-sm-7 col-xs-12">
                                <div class="descr-text">
                                    <div id="product-overview">
                                        <div class="productdecor-details">
                                            <h1 itemprop="name"> {{$translation->title}} </h1>
                                            <meta itemprop="mpn" content="{{$translation->title}}" />
                                            <meta itemprop="description" content="" />
                                            <meta itemprop="sku" content="Bosch SMS46MI05E" />
                                            <div itemscope itemprop="brand" itemtype="http://schema.org/Thing">
                                                <meta itemprop="name" content="{{$translation->title}}" />
                                            </div>
                                            <div itemprop="offers" itemtype="http://schema.org/Offer" itemscope>
                                                <link itemprop="url" href="{{$row->getDetailUrl()}}" />
                                                <meta itemprop="availability" content="https://schema.org/InStock" />
                                                <meta itemprop="priceCurrency" content="VND" />
                                                <meta itemprop="itemCondition" content="https://schema.org/UsedCondition" />
                                                <meta itemprop="price" content="21500" />
                                                <meta itemprop="priceValidUntil" content="2022-01-01" />
                                                <div itemprop="seller" itemtype="http://schema.org/Organization" itemscope>
                                                    <meta itemprop="name" content="{{$translation->title}}" />
                                                </div>
                                            </div>
                                            <div itemscope itemprop="aggregateRating" itemtype="http://schema.org/AggregateRating">
                                                <meta itemprop="reviewCount" content="54943" />
                                                <meta itemprop="ratingValue" content="4.7" />
                                            </div>
                                            <div itemprop="review" itemtype="http://schema.org/Review" itemscope>
                                                <div itemprop="author" itemtype="http://schema.org/Person" itemscope>
                                                    <meta itemprop="name" content="Siêu thị bếp Vũ Sơn, Máy rửa bát Bosch SMS46MI05E, Bosch SMS46MI05E, Máy rửa bát Bosch" />
                                                </div>
                                                <div itemprop="reviewRating" itemtype="http://schema.org/Rating" itemscope>
                                                    <meta itemprop="ratingValue" content="4.7" />
                                                    <meta itemprop="bestRating" content="5" />
                                                </div>
                                            </div>
{{--                                            <div class="product-sets">--}}
{{--                                                <div class="pro-rating cendo-pro">--}}
{{--                                                    <div class="pro_one">--}}
{{--                                                        <div class="tf-stars tf-stars-svg"><span style="width: 86% !important" class="tf-stars-svg"></span></div>--}}
{{--                                                    </div>--}}
{{--                                                    <p class="rating-links">--}}
{{--                                                        <a href="#">4.5</a> (14 reviews)--}}
{{--                                                    </p>--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
                                            <div id="product-details-lists">
                                                <p>{!! $row->short_desc !!}</p>
                                            </div>
                                            <div class="productdecor-price">
                                                @if($row->product_type=='variable')
                                                    @if(!empty($priceRange = getMinMaxPriceProductVariations($row)))
                                                        <strong class="price">
                                                            @if($priceRange['min'] == $priceRange['max'])
                                                                <span>{{number_format($priceRange['max'])}} đ</span>
                                                            @else
                                                                <ins>
                                                                    <span>{{number_format($priceRange['min'])}} đ</span>
                                                                    -
                                                                    <span>{{number_format($priceRange['max'])}} đ</span>
                                                                </ins>
                                                            @endif
                                                        </strong>
                                                    @endif
                                                @else
                                                    <strong class="price">
                                                        @if(empty($row->sale_price) && empty($row->price))
                                                            <span>Liên hệ</span>
                                                        @else
                                                            @if(!empty($row->sale_price))
                                                                <del><span>{{number_format($row->price)}} đ</span></del>
                                                                <span>{{number_format($row->sale_price)}} đ</span>
                                                            @else
                                                                <span>{{number_format($row->price)}} đ</span>
                                                            @endif
                                                        @endif
                                                    </strong>
                                                @endif
                                            </div>
                                            <div class="box_support d-none">
                                                <p class="hotline">CHƯƠNG TRÌNH KHUYẾN MÃI</p>
                                                <p class="value">Giảm tới 10%</p>
                                                <div class="product-call-requests">
                                                    <label class="ty-control-group__title">
                                                        <input class="ty-input-text-full cm-number" id="PhoneRegister" size="50" type="tel" maxlength="11" autocomplete="off" minlength="8" name="" placeholder="Nhập số điện thoại " value="">
                                                    </label>
                                                    <div class="ty-btn ty-btn cm-call-requests"><a href="javascript:void(0)" onclick="getPhone();">Đăng ký ngay</a></div>
                                                    <span class="call-note">Chúng tôi sẽ gọi lại cho quý khách</span>
                                                </div>
                                            </div>
                                            <div class="product-sets">
                                                <div class="qty-block">
                                                    <fieldset id="product-actions-fieldset">
                                                        <a class="btn" href="tel:0986 083 083"><i class="pe-7s-call"></i>Liên hệ trực tiếp {{ setting_item('hotline_phone') }} <span>(Để có giá tốt nhất)</span></a>
                                                        @if($row->product_type != 'variable' && !empty($row->price))
                                                            <a href="javascript:void(0)" class="buy_now btn btn-default btn-b1 btn-cart bravo_add_to_cart" data-product='{!! json_encode(['id'=>$row->id,'type'=>$row->product_type,'buy_now'=>1])!!}'>
                                                                <i class="pe-7s-cart"></i>Mua ngay<span>(Xem hàng, không mua không sao)</span>
                                                            </a>
                                                        @endif
                                                    </fieldset>
                                                </div>
                                            </div>
                                            <div class="whotline">
                                                <li>
                                                    <a href="tel:024 33 100 100">
                                                        <span>Tổng đài tư vấn (8:00 - 19:00)</span>
                                                        <p class="hotline">{{ setting_item('hotline_phone') }}</p>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="tel:0986 083 083">
                                                        <span>Hotline (24/7)</span>
                                                        <p class="hotline">{{ setting_item('hotline_phone') }}</p>
                                                    </a>
                                                </li>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-25 col-sm-5 col-xs-12 box-right">
{{--                                <div class="why-buy hidden-xs">--}}
{{--                                    <label>Tại sao mua hàng của chúng tôi ?</label>--}}
{{--                                </div>--}}
{{--                                @include('Product::frontend.details.sidebar.shipping')--}}
                                <div class="map-bt">
                                    <label>Hệ thống siêu thị:</label>
                                    <p class="item-showroom ">
                                        <i class="fa fa-map-marker"></i>TBV - Tầng 3, L03,L11, Khu đô thị Dương Nội, La Khê, Hà Đông<br />
                                        <span>Tầng 3, L03,L11, Khu đô thị Dương Nội, La Khê, Hà Đông</span>
                                    </p>
{{--                                    <p class="item-showroom ">--}}
{{--                                        <i class="fa fa-map-marker"></i>TBV - Nhà máy: Cầu Thống Nhất, Thanh Nhàn, Thanh Xuân, Sóc Sơn, Hà Nội<br />--}}
{{--                                        <span>Nhà máy: Cầu Thống Nhất, Thanh Nhàn, Thanh Xuân, Sóc Sơn, Hà Nội</span>--}}
{{--                                    </p>--}}

{{--                                    <a href="javascript:void(0)" class="viewmoreaddress" onclick="showalladdress();">Xem thêm Showroom</a>--}}
{{--                                    <a href="javascript:void(0)" class="hideaddress" onclick="hideaddress();">Thu gọn Showroom</a>--}}
                                </div>
                            </div>
                        </div>
                        <div class="col_product_view">
                            <div class="">
                                <div class="col-md-8 pdr_0">
                                    <div class="description-title">Thông tin chi tiết</div>
                                    <div class="description-content descr-text">
                                        {!! $row->content !!}
                                    </div>
                                    @if($row->content && strlen($row->content) > 200)
                                    <div class="show-more">
                                        <a href="javascript:void(0)" class="readmore" id="js-show-more">Xem thêm  </a>
                                    </div>
                                    @endif
                                </div>
                                <div class="col-md-4 pdl_0">
                                    <div class="attribute-title">Thông số kỹ thuật</div>
                                    <div class="attribute-content descr-text">
                                        <div style="clear:both;"> </div>
                                        {!! $row->specification !!}
                                        <p> </p>
                                        <div style="clear:both;"> </div>
                                    </div>
                                    @if($row->specification && strlen($row->specification) > 200)
                                        <div class="show-more">
                                            <a href="javascript:void(0)" class="readmore" id="js2-show-more">Xem thêm thông số  </a>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </section>
            @if(!empty($row->same_category))
                <section class="single-products section-padding-bottom">
                    <div class="container">
                        <div class="">
                            <div class="hd-card-body">
                                <div class="hd-module-title">
                                    <h3 class="module-title">Sản phẩm Liên quan</h3>
                                </div>
                                <div class="row">
                                    <div id="similar-products" class="">
                                        <div  class="col sanpham small-12 large-12"  >
                                            <div class="col-inner"  >
                                                <div class="slider-deal row large-columns-5 medium-columns-3 small-columns-2 row-small slider row-slider slider-nav-simple slider-nav-light slider-nav-push"  data-flickity-options='{"imagesLoaded": true, "groupCells": "100%", "dragThreshold" : 5, "cellAlign": "left","wrapAround": true,"prevNextButtons": true,"percentPosition": true,"pageDots": false, "rightToLeft": false, "autoPlay" : false}'>
                                                    @include('Product::frontend.details.product-same')
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            @endif
            <section class="single-products section-padding-bottom">
                <div class="container">
                    <div class="">

                        <div class="hd-card-body over">
                            <div class="hd-module-title">
                                <h3 class="module-title">Đánh giá </h3>
                            </div>
                            <div id="bravo-reviews" class="warr-reviews">
                                @include('Product::frontend.details.tabs.v2.review')

                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </main>
@endsection

@section('footer')
    <script>
        var bravo_booking_data = {!! json_encode($booking_data) !!}
        var bravo_booking_i18n = {
                no_date_select:'{{__('Please select Start and End date')}}',
                no_guest_select:'{{__('Please select at lease one guest')}}',
            };
        Bravo.variations = {!! $variations_product !!};
            $(document).ready(function ($) {
                // show-hide text
                $('#js-show-more').click(function () {
                    if ($('.description-content').hasClass('expand_p')) {
                        $('.description-content').removeClass("expand_p");
                        document.getElementById('js-show-more').innerHTML = " Xem thêm";
                    } else {
                        $('.description-content').addClass("expand_p");
                        document.getElementById('js-show-more').innerHTML = " Thu gọn";
                    }
                });


                $('#js2-show-more').click(function () {
                    if ($('.attribute-content').hasClass('expand_p')) {
                        $('.attribute-content').removeClass("expand_p");
                        document.getElementById('js2-show-more').innerHTML = " Xem thêm thông số";
                    } else {
                        $('.attribute-content').addClass("expand_p");
                        document.getElementById('js2-show-more').innerHTML = " Thu gọn";
                    }
                });
        });

    </script>


    <script type="text/javascript" src="{{ asset("websieukhung/libs/fotorama/fotorama.js?v=".config('app.version')) }}"></script>

    <script type="text/javascript" src="{{ asset("libs/ion_rangeslider/js/ion.rangeSlider.min.js") }}"></script>
    <script type="text/javascript" src="{{ asset("libs/flexslider/jquery.flexslider-min.js") }}"></script>
    <script type="text/javascript" src="{{ asset("libs/sticky/jquery.sticky.js") }}"></script>
    <script type="text/javascript" src="{{ asset("module/product/js/product-detail.js") }}"></script>
@endsection
