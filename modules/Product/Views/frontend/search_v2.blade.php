    @extends('layouts.app')
    @section('head')
        <link rel="stylesheet" type="text/css" href="{{ asset("libs/ion_rangeslider/css/ion.rangeSlider.min.css") }}"/>
    @endsection
    @section('content')
        <div class="shop-page-title category-page-title page-title ">
            <div class="page-title-inner flex-row  medium-flex-wrap container">
                <div class="flex-col flex-grow medium-text-center">
                    <h1 class="shop-page-title is-xlarge">Kết quả tìm kiếm: “{{ request()->query('s') }}”
</h1>
                    <div class="is-small">
                        <nav class="woocommerce-breadcrumb breadcrumbs uppercase">
                            <a href="{{ url("/") }}">Trang chủ</a>
                            <span class="divider">/</span>
                            <a href="#">Sản phẩm</a>
                            <span class="divider">/</span>
                             Kết quả tìm kiếm cho “{{ request()->query('s') }}”
                        </nav>
                    </div>
                    <div class="category-filtering category-filter-row show-for-medium">
                        <a href="#" data-open="#shop-sidebar" data-visible-after="true" data-pos="left" class="filter-button uppercase plain">
                            <i class="icon-menu"></i>
                            <strong>Lọc</strong>
                        </a>
                        <div class="inline-block">
                        </div>
                    </div>
                </div>
                <!-- .flex-left -->
                <div class="flex-col medium-text-center">
                    <p class="woocommerce-result-count hide-for-medium">
                        Hiển thị {{ $rows->total() }} sản phẩm
                    </p>
                    <form class="woocommerce-ordering" method="get">
                        <select name="orderby" class="orderby" aria-label="Đơn hàng của cửa hàng">
                            <option value="popularity" selected="selected">Thứ tự theo mức độ phổ biến</option>
                            <option value="rating">Thứ tự theo điểm đánh giá</option>
                            <option value="date">Mới nhất</option>
                            <option value="price">Thứ tự theo giá: thấp đến cao</option>
                            <option value="price-desc">Thứ tự theo giá: cao xuống thấp</option>
                        </select>
                        <input type="hidden" name="paged" value="1">
                    </form>
                </div>
                <!-- .flex-right -->
            </div>
            <!-- flex-row -->
        </div>
        <main id="main" class="archive">
            <div id="content" role="main" class="content-area">
                <div class="bravo_search_product">
                    <div class="container">
                        <div class="search-header">
                            <div class="row  row-small category-page-row">
                                <div class="col large-3 hide-for-medium  col-md-3 col-sm-12 col-xs-12 col-bravo-filter">
                                    @include('Product::frontend.layouts.search_v2.filter-search')
                                </div>
                                <!-- #secondary -->
                                <div id="primary" class="col large-9 content-area col-md-9 col-sm-12 col-xs-12 col-bravo-filter-content">
                                    @include('Product::frontend.layouts.search_v2.content-search_content')
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
    @endsection

    @section('footer')
        <script type="text/javascript" src="{{ asset("libs/ion_rangeslider/js/ion.rangeSlider.min.js") }}"></script>
        <script type="text/javascript" src="{{ asset('module/product/js/product.js?_ver='.config('app.version')) }}"></script>
    @endsection
