<section class="section san-pham-danh-muc" >
    <div class="section-content relative">
        <div class="row row-small"  >
            <div  class="col sanpham small-12 large-12"  >
                <div class="col-inner"  >
                    <div class="row row-collapse align-middle tieude"  >
                        <div  class=" medium-4 small-12 large-4"  >
                            <div class="col-inner"  >
                                <h2><a href="#" data-wplink-url-error="true">{{ $title }}</a></h2>
                            </div>
                        </div>
                        <div  class="col hide-for-small medium-8 small-12 large-8"  >
                            <div class="col-inner">
                                @if(isset($model_categories))
                                    <ul>
                                        @if($model_categories && count($model_categories) > 0)
                                            @foreach($model_categories as $category)
                                                <li><a href="{{ $category->getDetailUrl() }}">{{ $category->name }}</a></li>
                                            @endforeach
                                        @endif
                                        <li><strong><a href="{{ $link_all ? $link_all : '#' }}" data-wplink-url-error="true">{{__('View All')}}</a></strong></li>
                                    </ul>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="slider-deal row large-columns-5 medium-columns-3 small-columns-2 row-small slider row-slider slider-nav-simple slider-nav-light slider-nav-push"  data-flickity-options='{"imagesLoaded": true, "groupCells": "100%", "dragThreshold" : 5, "cellAlign": "left","wrapAround": true,"prevNextButtons": true,"percentPosition": true,"pageDots": false, "rightToLeft": false, "autoPlay" : false}'>
                        @if(!empty($rows))
                            @foreach($rows as $row)
                                <?php
                                $reviewData = $row->getScoreReview();
                                $score_total = $reviewData['score_total'];
                                ?>
                                <div class="product-small col has-hover product type-product post-855 status-publish first instock product_cat-tivi-loa-am-thanh has-post-thumbnail sale shipping-taxable purchasable product-type-simple">
                                    <div class="col-inner">
                                        <div class="product-small box ">
                                            <div class="box-image">
                                                <div class="image-none">
                                                    <a href="{{$row->getDetailUrl()}}">
                                                        {!! get_image_tag($row->image_id,'thumb',['lazy'=>true,'alt'=>$row->title]) !!}
                                                    </a>
                                                </div>
                                                <div class="image-tools is-small top right show-on-hover">
                                                </div>
                                                <div class="image-tools is-small hide-for-small bottom left show-on-hover">
                                                </div>
                                                <div class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                                </div>
                                            </div>
                                            <!-- box-image -->
                                            <div class="box-text box-text-products">
                                                <div class="title-wrapper">
                                                    @if(count($row->tags) > 0)
                                                        @foreach($row->tags as $tag)
                                                            <span class="badge badge-danger">{{ $tag->name }}</span>
                                                        @endforeach
                                                    @endif
                                                    <p class="name product-title woocommerce-loop-product__title">
                                                        <a href="{{$row->getDetailUrl()}}">{{$row->title ?? ''}}</a></p>
                                                </div>
                                                <div class="price-wrapper">
                                                    @include('Product::frontend.details.price')
                                                </div>
                                                <?php
                                                $reviewData = (!empty($row)) ? $row->getScoreReview() : [];
                                                $score_total = $reviewData['score_total'];
                                                ?>
                                                <div class="bravo-reviews">
                                                    <div class="review-box">
                                                        <div class="mf-product-rating">
                                                            <div class="average-rating">
                                                                <div class="service-review tour-review-{{$score_total}}">
                                                                    <div class="list-star">
                                                                        <ul class="booking-item-rating-stars">
                                                                            <li><i class="fa fa-star-o"></i></li>
                                                                            <li><i class="fa fa-star-o"></i></li>
                                                                            <li><i class="fa fa-star-o"></i></li>
                                                                            <li><i class="fa fa-star-o"></i></li>
                                                                            <li><i class="fa fa-star-o"></i></li>
                                                                        </ul>
                                                                        <div class="booking-item-rating-stars-active" style="width: {{  $score_total * 2 * 10 ?? 0  }}%">
                                                                            <ul class="booking-item-rating-stars">
                                                                                <li><i class="fa fa-star"></i></li>
                                                                                <li><i class="fa fa-star"></i></li>
                                                                                <li><i class="fa fa-star"></i></li>
                                                                                <li><i class="fa fa-star"></i></li>
                                                                                <li><i class="fa fa-star"></i></li>
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- box-text -->
                                        </div>
                                        <!-- box -->
                                    </div>
                                    <!-- .col-inner -->
                                </div>
                            @endforeach
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

{{--<div class="martfury-container">--}}
{{--    <div class="mf-products-tabs">--}}
{{--        <div class="tabs-header">--}}
{{--            <h2><span class="cat-title">{{ $title }}</span></h2>--}}
{{--            <div class="tabs-header-nav">--}}
{{--                <a class="link" href="{{ route("product.index") }}">{{__('View All')}}</a></div>--}}
{{--        </div>--}}
{{--        <div class="tabs-content">--}}
{{--            <ul class="products list-unstyled">--}}
{{--                @if(!empty($rows))--}}
{{--                    @foreach($rows as $row)--}}
{{--                        @include('Product::frontend.layouts.product')--}}
{{--                    @endforeach--}}
{{--                @endif--}}
{{--            </ul>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--</div>--}}
