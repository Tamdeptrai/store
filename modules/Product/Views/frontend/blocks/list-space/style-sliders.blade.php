<section class="section san-deal container">
    <div class="bg section-bg fill bg-fill  bg-loaded" >
    </div>
    <div class="section-content relative">
        <div class="row row-small">
            <div class="col san-deal-title small-12 large-12"  >
                <div class="col-inner"  >
                    <h2 class=""><a href="#">{{$title ?? ''}}</a></h2>
                </div>
            </div>
        </div>
        <div class="row row-small ">
            <div class="col sanpham small-12 large-12"  >
                <div class="col-inner"  >
                    <div class="row large-columns-5 medium-columns-3 small-columns-2 row-small slider slider-deal row-slider slider-nav-simple slider-nav-light slider-nav-push"  data-flickity='{"imagesLoaded": true, "groupCells": "100%", "dragThreshold" : 5, "cellAlign": "left","wrapAround": true,"prevNextButtons": true,"percentPosition": true,"pageDots": false, "rightToLeft": false, "autoPlay" : 2000}'>
                        @foreach($rows as $row)

                            <div class="product-small col has-hover product type-product post-855 status-publish first instock product_cat-tivi-loa-am-thanh has-post-thumbnail sale shipping-taxable purchasable product-type-simple">
                                <div class="col-inner">
                                    <div class="product-small box ">
                                        <div class="box-image">
                                            <div class="image-none">
                                                <a href="{{$row->getDetailUrl()}}">
                                                    {!! get_image_tag($row->image_id,'thumb',['lazy'=>true,'alt'=>$row->title]) !!}
                                                </a>
                                            </div>
                                            <div class="image-tools is-small top right show-on-hover">
                                            </div>
                                            <div class="image-tools is-small hide-for-small bottom left show-on-hover">
                                            </div>
                                            <div class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                            </div>
                                        </div>
                                        <!-- box-image -->
                                        <div class="box-text box-text-products">
                                            <div class="title-wrapper">
                                                @if(count($row->tags) > 0)
                                                    @foreach($row->tags as $tag)
                                                        <span class="badge badge-danger">{{ $tag->name }}</span>
                                                    @endforeach
                                                @endif
                                                <p class="name product-title woocommerce-loop-product__title">
                                                    <a href="{{$row->getDetailUrl()}}">{{$row->title ?? ''}}</a>
                                                </p>
                                            </div>
                                            <div class="price-wrapper">
                                                @include('Product::frontend.details.price')
                                            </div>
                                            <?php
                                                $reviewData = (!empty($row)) ? $row->getScoreReview() : [];
                                                $score_total = $reviewData['score_total'];
                                            ?>
                                            <div class="bravo-reviews">
                                                <div class="review-box">
                                                    <div class="mf-product-rating">
                                                        <div class="average-rating">
                                                            <div class="service-review tour-review-{{$score_total}}">
                                                                <div class="list-star">
                                                                    <ul class="booking-item-rating-stars">
                                                                        <li><i class="fa fa-star-o"></i></li>
                                                                        <li><i class="fa fa-star-o"></i></li>
                                                                        <li><i class="fa fa-star-o"></i></li>
                                                                        <li><i class="fa fa-star-o"></i></li>
                                                                        <li><i class="fa fa-star-o"></i></li>
                                                                    </ul>
                                                                    <div class="booking-item-rating-stars-active" style="width: {{  $score_total * 2 * 10 ?? 0  }}%">
                                                                        <ul class="booking-item-rating-stars">
                                                                            <li><i class="fa fa-star"></i></li>
                                                                            <li><i class="fa fa-star"></i></li>
                                                                            <li><i class="fa fa-star"></i></li>
                                                                            <li><i class="fa fa-star"></i></li>
                                                                            <li><i class="fa fa-star"></i></li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                        <!-- box-text -->
                                    </div>
                                    <!-- box -->
                                </div>
                            <!-- .col-inner -->
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
{{--<div class="mf-product-deals-day woocommerce mf-products-carousel bravo_style-sliders">--}}
{{--    <div class="cat-header">--}}
{{--        <div class="header-content">--}}
{{--            <h2 class="cat-title">{{$title ?? ''}}</h2>--}}
{{--        </div>--}}
{{--        <div class="header-link">--}}
{{--            @if(!empty($categories))--}}
{{--                @foreach($categories as $cat)--}}
{{--                    <li><a class="extra-link" href="{{$cat->getDetailUrl()}}">{{ $cat->name }}</a></li>--}}
{{--                @endforeach--}}
{{--            @endif--}}
{{--            <a href="{{ route("product.index") }}">{{__('View All')}}</a>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--    <div class="products-content">--}}
{{--        <ul class="products">--}}
{{--            @foreach($rows as $row)--}}
{{--                @include('Product::frontend.layouts.product')--}}
{{--            @endforeach--}}
{{--        </ul>--}}
{{--    </div>--}}
{{--</div>--}}
