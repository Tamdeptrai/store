@if(!empty($row->same_category))
    @foreach($row->same_category as $item=>$row)
        <?php
        $reviewData = $row->getScoreReview();
        $score_total = $reviewData['score_total'];
        ?>
        <div class="product-small col has-hover product type-product post-855 status-publish first instock product_cat-tivi-loa-am-thanh has-post-thumbnail sale shipping-taxable purchasable product-type-simple">
            <div class="col-inner">
                <div class="product-small box ">
                    <div class="box-image">
                        <div class="image-none">
                            <a href="{{$row->getDetailUrl()}}">
                                {!! get_image_tag($row->image_id,'thumb',['lazy'=>true,'alt'=>$row->title]) !!}
                            </a>
                        </div>
                        <div class="image-tools is-small top right show-on-hover">
                        </div>
                        <div class="image-tools is-small hide-for-small bottom left show-on-hover">
                        </div>
                        <div class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                        </div>
                    </div>
                    <!-- box-image -->
                    <div class="box-text box-text-products">
                        <div class="title-wrapper">
                            <p class="name product-title woocommerce-loop-product__title">
                                <a href="{{$row->getDetailUrl()}}">{{$row->title ?? ''}}</a></p>
                        </div>
                        <div class="price-wrapper">
                            @include('Product::frontend.details.price')
                        </div>
                    </div>
                    <!-- box-text -->
                </div>
                <!-- box -->
            </div>
            <!-- .col-inner -->
        </div>
    @endforeach
@endif
