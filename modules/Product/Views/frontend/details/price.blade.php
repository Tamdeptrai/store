@if($row->product_type=='variable')
    @if(!empty($priceRange = getMinMaxPriceProductVariations($row)))
        <p class="price variable-price">
            @if($priceRange['min'] == $priceRange['max'])
                <ins><span class="amount">{{number_format($priceRange['max'])}}<span class="woocommerce-Price-currencySymbol">&#8363;</span></span></ins>
            @else
                <ins>
                    <span class="amount">{{number_format($priceRange['min'])}}<span class="woocommerce-Price-currencySymbol">&#8363;</span></span>
                    -
                    <span class="amount">{{number_format($priceRange['max'])}}<span class="woocommerce-Price-currencySymbol">&#8363;</span></span>
                </ins>
            @endif
        </p>
    @endif
@else
    @if(!empty($row->sale_price))
        <span class="price">
            <del>
                  <span class="woocommerce-Price-amount amount">
                      @if($row->price > 0)
                      {{number_format($row->price)}}
                      <span class="woocommerce-Price-currencySymbol">&#8363;</span>
                      @else
                        Liên hệ
                      @endif
                  </span>
            </del>
            @if($row->sale_price > 0)
                <ins>
                    <span class="woocommerce-Price-amount amount">{{number_format($row->sale_price)}}
                        <span class="woocommerce-Price-currencySymbol">&#8363;</span></span>
                </ins>
            @endif
        </span>
        @if(!empty($row->discount_percent))
        <div class="badge-container absolute left top z-1">
            <div class="callout badge-square">
                <div class="badge-inner secondary on-sale"><span class="onsale">-{{$row->discount_percent}}</span></div>
            </div>
        </div>
        @endif
    @else
        <span class="price">
            <ins>
                <span class="woocommerce-Price-amount amount">
                    @if($row->price > 0)
                        {{number_format($row->price)}}
                        <span class="woocommerce-Price-currencySymbol">&#8363;</span></span>
                    @else
                        Liên hệ
                    @endif
            </ins>
        </span>
    @endif
@endif

{{--@if($row->product_type=='variable')--}}
{{--    @if(!empty($priceRange = getMinMaxPriceProductVariations($row)))--}}
{{--        <p class="price variable-price">--}}
{{--            @if($priceRange['min'] == $priceRange['max'])--}}
{{--                <ins><span class="amount">{{format_money($priceRange['max'])}}</span></ins>--}}
{{--            @else--}}
{{--                <ins>--}}
{{--                    <span class="amount">{{format_money($priceRange['min'])}}</span>--}}
{{--                    ---}}
{{--                    <span class="amount">{{format_money($priceRange['max'])}}</span>--}}
{{--                </ins>--}}
{{--            @endif--}}
{{--        </p>--}}
{{--    @endif--}}
{{--@else--}}
{{--    @if(!empty($row->sale_price))--}}
{{--        <p class="price has-sale">--}}
{{--            <ins>--}}
{{--                <span class="amount">{{format_money($row->sale_price)}}</span>--}}
{{--            </ins>--}}
{{--            <del>--}}
{{--                <span class="amount">{{format_money($row->price)}}</span>--}}
{{--            </del>--}}
{{--            @if(!empty($row->discount_percent))--}}
{{--                <span class="sale sale-1">(-{{$row->discount_percent}})</span>--}}
{{--                <span class="sale sale-2">{{ __(':discount off',['discount'=>$row->discount_percent]) }}</span>--}}
{{--            @endif--}}
{{--        </p>--}}
{{--    @else--}}
{{--        <p class="price single-price">--}}
{{--            <ins><span class="amount">{{format_money($row->price)}}</span></ins>--}}
{{--        </p>--}}
{{--    @endif--}}
{{--@endif--}}
