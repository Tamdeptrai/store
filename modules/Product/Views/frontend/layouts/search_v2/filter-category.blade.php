<form action="{{ route("product.category.index",['slug'=>$cate_current->slug]) }}" class="bravo_form_filter">
<div id="shop-sidebar" class="sidebar-inner col-inner bravo_filter">
    <aside id="woocommerce_product_categories-2" class="widget woocommerce widget_product_categories">
        <span class="widget-title shop-sidebar">Danh mục sản phẩm</span>
        <div class="is-divider small"></div>
        @if(!empty($categories))
            <ul class="product-categories">
                @foreach ($categories as $category)
                    @if($category->children && count($category->children) > 0)
                            <li class="cat-item cat-item-{{ $category->id }} cat-parent">
                                <a href="{{ $category->getDetailUrl() }}">{{ $category->name }}</a>
                                <ul class='children'>
                                    @foreach($category->children as $cate_child)
                                        <li class="cat-item cat-item-132"><a href="{{ $cate_child->getDetailUrl() }}">{{ $cate_child->name }}</a></li>
                                    @endforeach
                                </ul>
                            </li>
                    @else
                        <li class="cat-item cat-item-{{ $category->id }}"><a href="{{ $category->getDetailUrl() }}">{{ $category->name }}</a></li>
                    @endif
                @endforeach
            </ul>
        @endif
    </aside>
    <aside id="yith-woo-ajax-navigation-2" class="widget yith-woocommerce-ajax-product-filter yith-woo-ajax-navigation woocommerce widget_layered_nav">
        <span class="widget-title shop-sidebar">{{__("BY PRICE")}}</span>
        <div class="is-divider small"></div>
        <div class="bravo-filter-price">
                    <?php
                    $price_min = $pri_from = floor ( ($product_min_max_price[0]) );
                    $price_max = $pri_to = ceil ( ($product_min_max_price[1]) );
                    if (!empty($min_price = Request::query('min_price'))) {
                        $pri_from = $min_price;
                    }
                    if (!empty($max_price = Request::query('max_price'))) {
                        $pri_to = $max_price;
                    }
                    $currency = App\Currency::getCurrency(setting_item('currency_main'));?>
                    <div class="price_slider"  data-from="{{$pri_from}}" data-to="{{$pri_to}}"></div>
                    <div class="bravo-filter-price-amount" data-step="1000">
                        <input type="text" id="min_price" name="min_price" class="d-none" value="{{$price_min}}"
                               data-min="{{$price_min}}">
                        <input type="text" id="max_price" name="max_price" class="d-none" value="{{$price_max}}"
                               data-max="{{$price_max}}">
                        <button type="submit" class="button d-sm-block d-md-none">{{__('Filter')}}</button>
                        <div class="price_label">
                            {{__('Price')}}: <span class="from">{{ number_format($price_min) }}</span>{{$currency['symbol']}} — <span
                                class="to">{{ number_format($price_max) }}</span>{{$currency['symbol']}}
                        </div>
                        <div class="clear"></div>
                    </div>
                    <input type="submit" class="bravo-price-submit" title="{{__('APPLY')}}" value="{{__('APPLY')}}">
                </div>
    </aside>
    @if(!empty($brands))
        @php
            $selected = (array) Request::query('brand');
        @endphp
        <aside id="yith-woo-ajax-navigation-3" class="g-filter-item widget yith-woocommerce-ajax-product-filter yith-woo-ajax-navigation woocommerce widget_layered_nav">
            <span class="widget-title shop-sidebar">{{__("By Brands")}}</span>
            <div class="is-divider small"></div>
            <ul class='yith-wcan-list yith-wcan bravo-custom-scroll list-unstyled'>
                @foreach($brands as $item=>$brand)
                    @php $translate = $brand->translateOrOrigin(app()->getLocale()) @endphp
                    <li>
                        <div class="bravo-checkbox">
                            <label>
                                <input @if(in_array($brand->id,$selected)) checked @endif type="checkbox" name="brand[]" value="{{$brand->id}}"> {{ $translate->name }}
                                <span class="checkmark"></span>
                            </label>
                        </div>
                    </li>
                @endforeach
            </ul>
        </aside>
    @endif
    @php
        $selected = (array) Request::query('terms');
    @endphp
    @foreach ($attributes as $key => $item)
        @php
            $translate = $item->translateOrOrigin(app()->getLocale());
        @endphp
        <aside id="yith-woo-ajax-navigation-{{ $key + 3 }}" class="g-filter-item widget yith-woocommerce-ajax-product-filter yith-woo-ajax-navigation woocommerce widget_layered_nav">
                <span class="widget-title shop-sidebar">{{$translate->name}}</span>
                <div class="is-divider small"></div>
                <ul class='yith-wcan-list yith-wcan bravo-custom-scroll list-unstyled'>
                    @foreach($item->terms as $key => $term)
                        @php $translate = $term->translateOrOrigin(app()->getLocale()); @endphp
                            <li @if($key > 2 and empty($selected)) @endif>
                                <div class="bravo-checkbox">
                                    <label>
                                        <input @if(in_array($term->id,$selected)) checked @endif type="checkbox" name="terms[]" value="{{$term->id}}"> {!! clean($translate->name) !!}
                                        <span class="checkmark"></span>
                                    </label>
                                </div>
                            </li>
                    @endforeach        
                </ul>
        </aside>        
    @endforeach
</div>
</form>