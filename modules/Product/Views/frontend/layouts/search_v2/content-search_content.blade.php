<div class="shop-container">
            <div class="woocommerce-notices-wrapper"></div>
            <div class="products row row-small large-columns-4 medium-columns-3 small-columns-2 equalize-box">
{{--                <div class="product-small col has-hover product type-product post-813 status-publish first instock product_cat-tivi-loa-am-thanh has-post-thumbnail sale shipping-taxable purchasable product-type-simple">--}}
{{--                    <div class="col-inner">--}}
{{--                        <div class="product-small box ">--}}
{{--                            <div class="box-image">--}}
{{--                                <div class="image-none">--}}
{{--                                    <a href="../../product/android-tivi-sony-4k-43-inch-kd-43x7500e/index.html">--}}
{{--                                        <img width="300" height="300" src="../../wp-content/uploads/2020/08/00034329_DESKTOP_65087-300x300.jpg" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail" alt="" loading="lazy" srcset="http://dienmay.websieukhung.com/wp-content/uploads/2020/08/00034329_DESKTOP_65087-300x300.jpg 300w, http://dienmay.websieukhung.com/wp-content/uploads/2020/08/00034329_DESKTOP_65087-100x100.jpg 100w, http://dienmay.websieukhung.com/wp-content/uploads/2020/08/00034329_DESKTOP_65087-280x280.jpg 280w, http://dienmay.websieukhung.com/wp-content/uploads/2020/08/00034329_DESKTOP_65087-24x24.jpg 24w, http://dienmay.websieukhung.com/wp-content/uploads/2020/08/00034329_DESKTOP_65087-36x36.jpg 36w, http://dienmay.websieukhung.com/wp-content/uploads/2020/08/00034329_DESKTOP_65087-48x48.jpg 48w, http://dienmay.websieukhung.com/wp-content/uploads/2020/08/00034329_DESKTOP_65087.jpg 400w" sizes="(max-width: 300px) 100vw, 300px" />				</a>--}}
{{--                                </div>--}}
{{--                                <div class="image-tools is-small top right show-on-hover">--}}
{{--                                </div>--}}
{{--                                <div class="image-tools is-small hide-for-small bottom left show-on-hover">--}}
{{--                                </div>--}}
{{--                                <div class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                            <!-- box-image -->--}}
{{--                            <div class="box-text box-text-products">--}}
{{--                                <div class="title-wrapper">--}}
{{--                                    <p class="name product-title woocommerce-loop-product__title"><a href="../../product/android-tivi-sony-4k-43-inch-kd-43x7500e/index.html">Android Tivi Sony 4K 43 inch KD-43X7500E</a></p>--}}
{{--                                </div>--}}
{{--                                <div class="price-wrapper">--}}
{{--                                    <span class="price"><del><span class="woocommerce-Price-amount amount">22.000.000&nbsp;<span class="woocommerce-Price-currencySymbol">&#8363;</span></span></del> <ins><span class="woocommerce-Price-amount amount">14.000.000&nbsp;<span class="woocommerce-Price-currencySymbol">&#8363;</span></span></ins></span>--}}
{{--                                    <div class="badge-container absolute left top z-1">--}}
{{--                                        <div class="callout badge badge-square">--}}
{{--                                            <div class="badge-inner secondary on-sale"><span class="onsale">-36%</span></div>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                            <!-- box-text -->--}}
{{--                        </div>--}}
{{--                        <!-- box -->--}}
{{--                    </div>--}}
{{--                    <!-- .col-inner -->--}}
{{--                </div>--}}

                @if($rows->total() > 0)
                    @foreach($rows as $row)
                        @include('Product::frontend.layouts.product_v2')
                    @endforeach
                @else
                    <div class="alert alert-warning product-warring" role="alert" style="width: 100%">
                        Không tìm thấy sản phẩm nào khớp với lựa chọn của bạn.
                    </div>
                @endif
            </div>
            <!-- row -->
</div>

