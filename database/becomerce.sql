/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 100408
 Source Host           : localhost:3306
 Source Schema         : becomerce

 Target Server Type    : MySQL
 Target Server Version : 100408
 File Encoding         : 65001

 Date: 09/06/2021 14:00:03
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for bravo_attrs
-- ----------------------------
DROP TABLE IF EXISTS `bravo_attrs`;
CREATE TABLE `bravo_attrs`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `display_type` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `slug` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `service` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `create_user` bigint(20) NULL DEFAULT NULL,
  `update_user` bigint(20) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of bravo_attrs
-- ----------------------------
INSERT INTO `bravo_attrs` VALUES (1, 'Color', 'color', 'color', 'product', 1, NULL, NULL, NULL, NULL);
INSERT INTO `bravo_attrs` VALUES (2, 'Size', 'text', 'size', 'product', 1, NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for bravo_attrs_translations
-- ----------------------------
DROP TABLE IF EXISTS `bravo_attrs_translations`;
CREATE TABLE `bravo_attrs_translations`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `origin_id` bigint(20) NULL DEFAULT NULL,
  `locale` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `create_user` bigint(20) NULL DEFAULT NULL,
  `update_user` bigint(20) NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `bravo_attrs_translations_origin_id_locale_unique`(`origin_id`, `locale`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for bravo_contact
-- ----------------------------
DROP TABLE IF EXISTS `bravo_contact`;
CREATE TABLE `bravo_contact`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `message` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `status` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `create_user` int(11) NULL DEFAULT NULL,
  `update_user` int(11) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for bravo_coupon
-- ----------------------------
DROP TABLE IF EXISTS `bravo_coupon`;
CREATE TABLE `bravo_coupon`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `coupon_type` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `discount` int(11) NULL DEFAULT NULL,
  `expiration` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `email` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `usage` int(11) NULL DEFAULT NULL,
  `customer_id` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `per_coupon` int(11) NULL DEFAULT NULL,
  `per_user` int(11) NULL DEFAULT NULL,
  `status` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `create_user` int(11) NULL DEFAULT NULL,
  `update_user` int(11) NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of bravo_coupon
-- ----------------------------
INSERT INTO `bravo_coupon` VALUES (1, 'QF645TY6', 'percent', 50, '2021-04-12 - 2021-04-17', '[\"admin@dev.com\",\"vendor1@dev.com\"]', NULL, '[\"14\",\"16\"]', 2, 3, 'publish', 1, NULL, NULL, NULL);
INSERT INTO `bravo_coupon` VALUES (2, '4F29N73F', 'percent', 10, '2021-04-12 - 2021-04-14', '[\"vendor1@dev.com\"]', NULL, '[\"11\",\"12\"]', 2, 3, 'publish', 1, NULL, NULL, NULL);
INSERT INTO `bravo_coupon` VALUES (3, '26EF7JTB', 'percent', 20, '2021-04-12 - 2021-04-13', '', NULL, '[\"11\"]', 2, 3, 'publish', 1, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for bravo_review
-- ----------------------------
DROP TABLE IF EXISTS `bravo_review`;
CREATE TABLE `bravo_review`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `object_id` int(11) NULL DEFAULT NULL,
  `object_model` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `rate_number` int(11) NULL DEFAULT NULL,
  `author_ip` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `status` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `publish_date` datetime(0) NULL DEFAULT NULL,
  `create_user` bigint(20) NULL DEFAULT NULL,
  `update_user` bigint(20) NULL DEFAULT NULL,
  `vendor_id` bigint(20) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  `lang` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of bravo_review
-- ----------------------------
INSERT INTO `bravo_review` VALUES (1, 8, 'product', 'This great', 'This will go great with my Hoodie that I ordered a few weeks ago.', 5, '127.0.0.1', 'approved', NULL, 1, 1, 2, NULL, NULL, NULL, NULL);
INSERT INTO `bravo_review` VALUES (2, 8, 'product', 'I love it', 'Love this shirt! The ninja near and dear to my heart. <3', 5, '127.0.0.1', 'approved', NULL, 1, 1, 2, NULL, NULL, NULL, NULL);
INSERT INTO `bravo_review` VALUES (3, 8, 'product', 'Bad!!', 'This is bad', 1, '127.0.0.1', 'approved', NULL, 1, 1, 2, NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for bravo_review_meta
-- ----------------------------
DROP TABLE IF EXISTS `bravo_review_meta`;
CREATE TABLE `bravo_review_meta`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `review_id` int(11) NULL DEFAULT NULL,
  `object_id` int(11) NULL DEFAULT NULL,
  `object_model` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `val` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `create_user` int(11) NULL DEFAULT NULL,
  `update_user` int(11) NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for bravo_seo
-- ----------------------------
DROP TABLE IF EXISTS `bravo_seo`;
CREATE TABLE `bravo_seo`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `object_id` int(11) NULL DEFAULT NULL,
  `object_model` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `seo_index` tinyint(4) NULL DEFAULT NULL,
  `seo_title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `seo_desc` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `seo_image` int(11) NULL DEFAULT NULL,
  `seo_share` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `create_user` bigint(20) NULL DEFAULT NULL,
  `update_user` bigint(20) NULL DEFAULT NULL,
  `origin_id` bigint(20) NULL DEFAULT NULL,
  `lang` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of bravo_seo
-- ----------------------------
INSERT INTO `bravo_seo` VALUES (1, 10, 'product', 1, NULL, NULL, NULL, '{\"facebook\":{\"title\":null,\"desc\":null,\"image\":null},\"twitter\":{\"title\":null,\"desc\":null,\"image\":null}}', 1, NULL, NULL, NULL, '2021-04-20 05:48:07', '2021-04-20 05:48:07');
INSERT INTO `bravo_seo` VALUES (2, 1, 'product_translation', 1, NULL, NULL, NULL, '{\"facebook\":{\"title\":null,\"desc\":null,\"image\":null},\"twitter\":{\"title\":null,\"desc\":null,\"image\":null}}', 1, NULL, NULL, NULL, '2021-04-20 05:48:07', '2021-04-20 05:48:07');
INSERT INTO `bravo_seo` VALUES (3, 11, 'product', 1, NULL, NULL, NULL, '{\"facebook\":{\"title\":null,\"desc\":null,\"image\":null},\"twitter\":{\"title\":null,\"desc\":null,\"image\":null}}', 1, 1, NULL, NULL, '2021-04-20 05:49:07', '2021-05-05 05:21:34');
INSERT INTO `bravo_seo` VALUES (4, 2, 'product_translation', 1, NULL, NULL, NULL, '{\"facebook\":{\"title\":null,\"desc\":null,\"image\":null},\"twitter\":{\"title\":null,\"desc\":null,\"image\":null}}', 1, 1, NULL, NULL, '2021-04-20 05:49:07', '2021-05-05 05:21:34');
INSERT INTO `bravo_seo` VALUES (5, 7, 'product', 1, NULL, NULL, NULL, '{\"facebook\":{\"title\":null,\"desc\":null,\"image\":null},\"twitter\":{\"title\":null,\"desc\":null,\"image\":null}}', 1, 1, NULL, NULL, '2021-05-04 05:18:46', '2021-05-04 05:19:35');
INSERT INTO `bravo_seo` VALUES (6, 3, 'product_translation', 1, NULL, NULL, NULL, '{\"facebook\":{\"title\":null,\"desc\":null,\"image\":null},\"twitter\":{\"title\":null,\"desc\":null,\"image\":null}}', 1, NULL, NULL, NULL, '2021-05-04 05:18:46', '2021-05-04 05:18:46');
INSERT INTO `bravo_seo` VALUES (7, 3, 'product_translation_en', 1, NULL, NULL, NULL, '{\"facebook\":{\"title\":null,\"desc\":null,\"image\":null},\"twitter\":{\"title\":null,\"desc\":null,\"image\":null}}', 1, 1, NULL, NULL, '2021-05-04 05:19:35', '2021-05-04 05:23:58');

-- ----------------------------
-- Table structure for bravo_terms
-- ----------------------------
DROP TABLE IF EXISTS `bravo_terms`;
CREATE TABLE `bravo_terms`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `attr_id` int(11) NULL DEFAULT NULL,
  `slug` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `create_user` bigint(20) NULL DEFAULT NULL,
  `update_user` bigint(20) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  `image_id` int(11) NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of bravo_terms
-- ----------------------------
INSERT INTO `bravo_terms` VALUES (1, 'Red', '#FF0000', 1, 'red', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `bravo_terms` VALUES (2, 'Black', '#000000', 1, 'black', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `bravo_terms` VALUES (3, 'Blue', '#0000FF', 1, 'blue', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `bravo_terms` VALUES (4, 'S', 'S', 2, 's', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `bravo_terms` VALUES (5, 'M', 'M', 2, 'm', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `bravo_terms` VALUES (6, 'L', 'L', 2, 'l', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `bravo_terms` VALUES (7, 'XL', 'XL', 2, 'xl', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `bravo_terms` VALUES (8, 'XXL', 'XXL', 2, 'Xl', NULL, NULL, NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for bravo_terms_translations
-- ----------------------------
DROP TABLE IF EXISTS `bravo_terms_translations`;
CREATE TABLE `bravo_terms_translations`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `origin_id` bigint(20) NULL DEFAULT NULL,
  `locale` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `create_user` bigint(20) NULL DEFAULT NULL,
  `update_user` bigint(20) NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `bravo_terms_translations_origin_id_locale_unique`(`origin_id`, `locale`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for core_inbox
-- ----------------------------
DROP TABLE IF EXISTS `core_inbox`;
CREATE TABLE `core_inbox`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `from_user` bigint(20) NULL DEFAULT NULL,
  `to_user` bigint(20) NULL DEFAULT NULL,
  `object_id` bigint(20) NULL DEFAULT NULL,
  `object_model` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `type` tinyint(4) NULL DEFAULT 0,
  `create_user` int(11) NULL DEFAULT NULL,
  `update_user` int(11) NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for core_inbox_messages
-- ----------------------------
DROP TABLE IF EXISTS `core_inbox_messages`;
CREATE TABLE `core_inbox_messages`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `inbox_id` bigint(20) NULL DEFAULT NULL,
  `from_user` bigint(20) NULL DEFAULT NULL,
  `to_user` bigint(20) NULL DEFAULT NULL,
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `type` tinyint(4) NULL DEFAULT 0,
  `is_read` tinyint(4) NULL DEFAULT 0,
  `create_user` int(11) NULL DEFAULT NULL,
  `update_user` int(11) NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for core_languages
-- ----------------------------
DROP TABLE IF EXISTS `core_languages`;
CREATE TABLE `core_languages`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `locale` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `flag` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `status` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `create_user` int(11) NULL DEFAULT NULL,
  `update_user` int(11) NULL DEFAULT NULL,
  `last_build_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of core_languages
-- ----------------------------
INSERT INTO `core_languages` VALUES (1, 'en', 'English', 'gb', 'publish', 1, NULL, NULL, NULL, '2021-04-12 06:15:19', '2021-04-12 06:15:19');
INSERT INTO `core_languages` VALUES (2, 'ja', 'Japanese', 'jp', 'publish', 1, NULL, NULL, NULL, '2021-04-12 06:15:19', '2021-04-12 06:15:19');

-- ----------------------------
-- Table structure for core_menu_translations
-- ----------------------------
DROP TABLE IF EXISTS `core_menu_translations`;
CREATE TABLE `core_menu_translations`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `origin_id` int(10) UNSIGNED NOT NULL,
  `locale` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `items` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `create_user` int(11) NULL DEFAULT NULL,
  `update_user` int(11) NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `core_menu_translations_locale_index`(`locale`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of core_menu_translations
-- ----------------------------
INSERT INTO `core_menu_translations` VALUES (1, 1, 'ja', '[{\"id\":1,\"name\":\"Home\",\"class\":\"\",\"target\":\"\",\"item_model\":\"Modules\\\\Page\\\\Models\\\\Page\",\"origin_name\":\"Home Page\",\"model_name\":\"Page\",\"_open\":false,\"origin_edit_url\":\"\\/admin\\/module\\/page\\/edit\\/1\",\"layout\":\"\",\"children\":[{\"id\":1,\"name\":\"Marketplace Full Width\",\"class\":\"\",\"target\":\"\",\"item_model\":\"Modules\\\\Page\\\\Models\\\\Page\",\"origin_name\":\"Home Page\",\"model_name\":\"Page\",\"_open\":false,\"origin_edit_url\":\"\"}]},{\"name\":\"Shop\",\"url\":\"\",\"item_model\":\"custom\",\"_open\":false,\"layout\":\"multi_row\",\"model_name\":\"Custom\",\"is_removed\":true,\"children\":[{\"name\":\"Catalog Pages\",\"url\":\"\",\"item_model\":\"custom\",\"_open\":false,\"model_name\":\"Custom\",\"is_removed\":true,\"children\":[{\"name\":\"Shop Sidebar\",\"url\":\"\\/product\",\"item_model\":\"custom\",\"_open\":false,\"model_name\":\"Custom\",\"is_removed\":true},{\"name\":\"Category layout\",\"url\":\"\\/category\\/clothing-apparel\",\"item_model\":\"custom\",\"_open\":false,\"model_name\":\"Custom\",\"is_removed\":true},{\"name\":\"Products Of Category\",\"url\":\"\\/category\\/consumer-electrics\",\"item_model\":\"custom\",\"_open\":false,\"model_name\":\"Custom\",\"is_removed\":true}]},{\"name\":\"Product Layouts\",\"url\":\"\",\"item_model\":\"custom\",\"_open\":false,\"model_name\":\"Custom\",\"is_removed\":true,\"children\":[{\"name\":\"Full Width\",\"url\":\"\\/product\\/mens-sports-runnning-swim-board-shorts\",\"item_model\":\"custom\",\"_open\":false,\"model_name\":\"Custom\",\"is_removed\":true}]},{\"name\":\"Product Types\",\"url\":\"\",\"item_model\":\"custom\",\"_open\":false,\"model_name\":\"Custom\",\"is_removed\":true,\"children\":[{\"name\":\"Simple\",\"url\":\"\\/product\\/herschel-leather-duffle-bag-in-brown-color\",\"item_model\":\"custom\",\"_open\":false,\"model_name\":\"Custom\",\"is_removed\":true},{\"name\":\"Color Swatches\",\"url\":\"\\/product\\/mens-sports-runnning-swim-board-shorts\",\"item_model\":\"custom\",\"_open\":false,\"model_name\":\"Custom\",\"is_removed\":true},{\"name\":\"Out of stock\",\"url\":\"\\/product\\/korea-long-sofa-fabric-in-blue-navy-color\",\"item_model\":\"custom\",\"_open\":false,\"model_name\":\"Custom\",\"is_removed\":true}]},{\"name\":\"Martfury Pages\",\"url\":\"\",\"item_model\":\"custom\",\"_open\":false,\"model_name\":\"Custom\",\"is_removed\":true,\"children\":[{\"name\":\"Shopping Cart\",\"url\":\"\\/booking\\/cart\",\"item_model\":\"custom\",\"_open\":false,\"model_name\":\"Custom\",\"is_removed\":true},{\"name\":\"Wishlist\",\"url\":\"\\/user\\/wishlist\",\"item_model\":\"custom\",\"_open\":false,\"model_name\":\"Custom\",\"is_removed\":true},{\"name\":\"My account\",\"url\":\"\\/login\",\"item_model\":\"custom\",\"_open\":false}]}]},{\"name\":\"Pages\",\"url\":\"\",\"item_model\":\"custom\",\"_open\":false,\"layout\":\"multi_row\",\"target\":\"\",\"children\":[{\"name\":\"Basic Pages\",\"url\":\"\",\"item_model\":\"\",\"_open\":false,\"children\":[{\"name\":\"404 Page\",\"url\":\"\\/404\",\"item_model\":\"custom\",\"_open\":false}]},{\"name\":\"Vendor Pages\",\"url\":\"\",\"item_model\":\"\",\"_open\":false,\"children\":[{\"name\":\"Become a Vendor\",\"url\":\"\\/page\\/become-a-vendor\",\"item_model\":\"custom\",\"_open\":false},{\"name\":\"Vendor store\",\"url\":\"\\/profile\\/1\",\"item_model\":\"custom\",\"_open\":false}]}]},{\"name\":\"Blog\",\"url\":\"\",\"item_model\":\"custom\",\"_open\":false,\"layout\":\"multi_row\",\"target\":\"\",\"children\":[{\"name\":\"Blog Layout\",\"url\":\"\",\"item_model\":\"custom\",\"_open\":false,\"children\":[{\"name\":\"Right Sidebar\",\"url\":\"\\/news\",\"item_model\":\"custom\",\"_open\":false}]},{\"name\":\"Single Blog\",\"url\":\"\",\"item_model\":\"custom\",\"_open\":false,\"children\":[{\"name\":\"Single\",\"url\":\"\\/news\\/explore-fashion-trending-for-guys-in-autumn-2017\",\"item_model\":\"custom\",\"_open\":false}]}]}]', 1, 1, NULL, NULL);
INSERT INTO `core_menu_translations` VALUES (2, 2, 'ja', '[{\"name\":\"Hot Promotions\",\"url\":\"#\",\"item_model\":\"custom\",\"_open\":false,\"icon\":\"icon-star\",\"model_name\":\"Custom\",\"is_removed\":true},{\"name\":\"Consumer Electrics\",\"url\":\"#\",\"item_model\":\"custom\",\"_open\":false,\"icon\":\"icon-laundry\",\"layout\":\"multi_row\",\"bg\":21,\"model_name\":\"Custom\",\"is_removed\":true,\"children\":[{\"name\":\"Electronics\",\"url\":\"#\",\"item_model\":\"custom\",\"_open\":false,\"layout\":\"\",\"model_name\":\"Custom\",\"is_removed\":true,\"children\":[{\"name\":\"Home Audios & Theaters\",\"url\":\"#\",\"item_model\":\"custom\",\"_open\":false,\"model_name\":\"Custom\",\"is_removed\":true},{\"name\":\"TV & Videos\",\"url\":\"#\",\"item_model\":\"custom\",\"_open\":false,\"model_name\":\"Custom\",\"is_removed\":true},{\"name\":\"Camera, Photos & Videos\",\"url\":\"#\",\"item_model\":\"custom\",\"_open\":false,\"model_name\":\"Custom\",\"is_removed\":true},{\"name\":\"Cellphones & Accessories\",\"url\":\"#\",\"item_model\":\"custom\",\"_open\":false,\"model_name\":\"Custom\",\"is_removed\":true},{\"name\":\"Headphones\",\"url\":\"#\",\"item_model\":\"custom\",\"_open\":false,\"model_name\":\"Custom\",\"is_removed\":true},{\"name\":\"Videogames\",\"url\":\"#\",\"item_model\":\"custom\",\"_open\":false,\"model_name\":\"Custom\",\"is_removed\":true},{\"name\":\"Wireless Speakers\",\"url\":\"#\",\"item_model\":\"custom\",\"_open\":false,\"model_name\":\"Custom\",\"is_removed\":true},{\"name\":\"Office Electronics\",\"url\":\"#\",\"item_model\":\"custom\",\"_open\":false,\"model_name\":\"Custom\",\"is_removed\":true}]},{\"name\":\"Accessories & Parts\",\"url\":\"#\",\"item_model\":\"custom\",\"_open\":false,\"layout\":\"\",\"model_name\":\"Custom\",\"is_removed\":true,\"children\":[{\"name\":\"Digital Cables\",\"url\":\"#\",\"item_model\":\"custom\",\"_open\":false,\"model_name\":\"Custom\",\"is_removed\":true},{\"name\":\"Audio & Video Cables\",\"url\":\"#\",\"item_model\":\"custom\",\"_open\":false,\"model_name\":\"Custom\",\"is_removed\":true},{\"name\":\"Batteries\",\"url\":\"#\",\"item_model\":\"custom\",\"_open\":false,\"model_name\":\"Custom\",\"is_removed\":true},{\"name\":\"Charger\",\"url\":\"#\",\"item_model\":\"custom\",\"_open\":false,\"model_name\":\"Custom\",\"is_removed\":true}]}]},{\"name\":\"Home, Garden & Kitchen\",\"url\":\"#\",\"item_model\":\"custom\",\"_open\":false,\"icon\":\"icon-lampshade\",\"model_name\":\"Custom\",\"is_removed\":true},{\"name\":\"Health & Beauty\",\"url\":\"#\",\"item_model\":\"custom\",\"_open\":false,\"icon\":\"icon-heart-pulse\",\"model_name\":\"Custom\",\"is_removed\":true},{\"name\":\"Jewelry & Watches\",\"url\":\"#\",\"item_model\":\"custom\",\"_open\":false,\"icon\":\"icon-diamond2\",\"model_name\":\"Custom\",\"is_removed\":true},{\"name\":\"Computers & Technologies\",\"url\":\"#\",\"item_model\":\"custom\",\"_open\":false,\"icon\":\"icon-desktop\",\"model_name\":\"Custom\",\"is_removed\":true,\"layout\":\"multi_row\",\"bg\":22,\"children\":[{\"name\":\"Computer & Technologies\",\"url\":\"#\",\"item_model\":\"custom\",\"_open\":false,\"icon\":\"\",\"model_name\":\"Custom\",\"is_removed\":true,\"children\":[{\"name\":\"Computers & Tablets\",\"url\":\"#\",\"item_model\":\"custom\",\"_open\":false,\"model_name\":\"Custom\",\"is_removed\":true},{\"name\":\"Laptop\",\"url\":\"#\",\"item_model\":\"custom\",\"_open\":false,\"model_name\":\"Custom\",\"is_removed\":true},{\"name\":\"Monitors\",\"url\":\"#\",\"item_model\":\"custom\",\"_open\":false,\"model_name\":\"Custom\",\"is_removed\":true},{\"name\":\"Networking\",\"url\":\"#\",\"item_model\":\"custom\",\"_open\":false,\"model_name\":\"Custom\",\"is_removed\":true},{\"name\":\"Drive & Storages\",\"url\":\"#\",\"item_model\":\"custom\",\"_open\":false,\"model_name\":\"Custom\",\"is_removed\":true},{\"name\":\"Computer Components\",\"url\":\"#\",\"item_model\":\"custom\",\"_open\":false,\"model_name\":\"Custom\",\"is_removed\":true},{\"name\":\"Security & Protection\",\"url\":\"#\",\"item_model\":\"custom\",\"_open\":false,\"model_name\":\"Custom\",\"is_removed\":true},{\"name\":\"Gaming Laptop\",\"url\":\"#\",\"item_model\":\"custom\",\"_open\":false,\"model_name\":\"Custom\",\"is_removed\":true},{\"name\":\"Accesories\",\"url\":\"#\",\"item_model\":\"custom\",\"_open\":false,\"model_name\":\"Custom\",\"is_removed\":true}]}]},{\"name\":\"Babies & Moms\",\"url\":\"#\",\"item_model\":\"custom\",\"_open\":false,\"icon\":\"icon-baby-bottle\",\"model_name\":\"Custom\",\"is_removed\":true},{\"name\":\"Sport & Outdoor\",\"url\":\"#\",\"item_model\":\"custom\",\"_open\":false,\"icon\":\"icon-baseball\",\"model_name\":\"Custom\",\"is_removed\":true},{\"name\":\"Phones & Accessories\",\"url\":\"#\",\"item_model\":\"custom\",\"_open\":false,\"icon\":\"icon-smartphone\",\"model_name\":\"Custom\",\"is_removed\":true},{\"name\":\"Books & Office\",\"url\":\"#\",\"item_model\":\"custom\",\"_open\":false,\"icon\":\"icon-book2\",\"model_name\":\"Custom\",\"is_removed\":true},{\"name\":\"Cars & Motocycles\",\"url\":\"#\",\"item_model\":\"custom\",\"_open\":false,\"icon\":\"icon-car-siren\",\"model_name\":\"Custom\",\"is_removed\":true},{\"name\":\"Home Improments\",\"url\":\"#\",\"item_model\":\"custom\",\"_open\":false,\"icon\":\"icon-wrench\",\"model_name\":\"Custom\",\"is_removed\":true},{\"name\":\"Vouchers & Services\",\"url\":\"#\",\"item_model\":\"custom\",\"_open\":false,\"icon\":\"icon-tag\",\"model_name\":\"Custom\",\"is_removed\":true}]', 1, 1, NULL, NULL);

-- ----------------------------
-- Table structure for core_menus
-- ----------------------------
DROP TABLE IF EXISTS `core_menus`;
CREATE TABLE `core_menus`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `items` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `create_user` int(11) NULL DEFAULT NULL,
  `update_user` int(11) NULL DEFAULT NULL,
  `origin_id` bigint(20) NULL DEFAULT NULL,
  `lang` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of core_menus
-- ----------------------------
INSERT INTO `core_menus` VALUES (1, 'Menu', '[{\"id\":1,\"name\":\"Home\",\"class\":\"\",\"target\":\"\",\"item_model\":\"Modules\\\\Page\\\\Models\\\\Page\",\"origin_name\":\"Home Page\",\"model_name\":\"Page\",\"_open\":false,\"origin_edit_url\":\"\\/admin\\/module\\/page\\/edit\\/1\",\"layout\":\"\",\"children\":[{\"id\":1,\"name\":\"Marketplace Full Width\",\"class\":\"\",\"target\":\"\",\"item_model\":\"Modules\\\\Page\\\\Models\\\\Page\",\"origin_name\":\"Home Page\",\"model_name\":\"Page\",\"_open\":false,\"origin_edit_url\":\"\"}]},{\"name\":\"Shop\",\"url\":\"\",\"item_model\":\"custom\",\"_open\":false,\"layout\":\"multi_row\",\"model_name\":\"Custom\",\"is_removed\":true,\"children\":[{\"name\":\"Catalog Pages\",\"url\":\"\",\"item_model\":\"custom\",\"_open\":false,\"model_name\":\"Custom\",\"is_removed\":true,\"children\":[{\"name\":\"Shop Sidebar\",\"url\":\"\\/product\",\"item_model\":\"custom\",\"_open\":false,\"model_name\":\"Custom\",\"is_removed\":true},{\"name\":\"Category layout\",\"url\":\"\\/category\\/clothing-apparel\",\"item_model\":\"custom\",\"_open\":false,\"model_name\":\"Custom\",\"is_removed\":true},{\"name\":\"Products Of Category\",\"url\":\"\\/category\\/consumer-electrics\",\"item_model\":\"custom\",\"_open\":false,\"model_name\":\"Custom\",\"is_removed\":true}]},{\"name\":\"Product Layouts\",\"url\":\"\",\"item_model\":\"custom\",\"_open\":false,\"model_name\":\"Custom\",\"is_removed\":true,\"children\":[{\"name\":\"Full Width\",\"url\":\"\\/product\\/mens-sports-runnning-swim-board-shorts\",\"item_model\":\"custom\",\"_open\":false,\"model_name\":\"Custom\",\"is_removed\":true}]},{\"name\":\"Product Types\",\"url\":\"\",\"item_model\":\"custom\",\"_open\":false,\"model_name\":\"Custom\",\"is_removed\":true,\"children\":[{\"name\":\"Simple\",\"url\":\"\\/product\\/herschel-leather-duffle-bag-in-brown-color\",\"item_model\":\"custom\",\"_open\":false,\"model_name\":\"Custom\",\"is_removed\":true},{\"name\":\"Color Swatches\",\"url\":\"\\/product\\/mens-sports-runnning-swim-board-shorts\",\"item_model\":\"custom\",\"_open\":false,\"model_name\":\"Custom\",\"is_removed\":true},{\"name\":\"Out of stock\",\"url\":\"\\/product\\/korea-long-sofa-fabric-in-blue-navy-color\",\"item_model\":\"custom\",\"_open\":false,\"model_name\":\"Custom\",\"is_removed\":true}]},{\"name\":\"Martfury Pages\",\"url\":\"\",\"item_model\":\"custom\",\"_open\":false,\"model_name\":\"Custom\",\"is_removed\":true,\"children\":[{\"name\":\"Shopping Cart\",\"url\":\"\\/booking\\/cart\",\"item_model\":\"custom\",\"_open\":false,\"model_name\":\"Custom\",\"is_removed\":true},{\"name\":\"Wishlist\",\"url\":\"\\/user\\/wishlist\",\"item_model\":\"custom\",\"_open\":false,\"model_name\":\"Custom\",\"is_removed\":true},{\"name\":\"My account\",\"url\":\"\\/login\",\"item_model\":\"custom\",\"_open\":false}]}]},{\"name\":\"Pages\",\"url\":\"\",\"item_model\":\"custom\",\"_open\":false,\"layout\":\"multi_row\",\"target\":\"\",\"children\":[{\"name\":\"Basic Pages\",\"url\":\"\",\"item_model\":\"\",\"_open\":false,\"children\":[{\"name\":\"404 Page\",\"url\":\"\\/404\",\"item_model\":\"custom\",\"_open\":false}]},{\"name\":\"Vendor Pages\",\"url\":\"\",\"item_model\":\"\",\"_open\":false,\"children\":[{\"name\":\"Become a Vendor\",\"url\":\"\\/page\\/become-a-vendor\",\"item_model\":\"custom\",\"_open\":false},{\"name\":\"Vendor store\",\"url\":\"\\/profile\\/1\",\"item_model\":\"custom\",\"_open\":false}]}]},{\"name\":\"Blog\",\"url\":\"\",\"item_model\":\"custom\",\"_open\":false,\"layout\":\"multi_row\",\"target\":\"\",\"children\":[{\"name\":\"Blog Layout\",\"url\":\"\",\"item_model\":\"custom\",\"_open\":false,\"children\":[{\"name\":\"Right Sidebar\",\"url\":\"\\/news\",\"item_model\":\"custom\",\"_open\":false}]},{\"name\":\"Single Blog\",\"url\":\"\",\"item_model\":\"custom\",\"_open\":false,\"children\":[{\"name\":\"Single\",\"url\":\"\\/news\\/explore-fashion-trending-for-guys-in-autumn-2017\",\"item_model\":\"custom\",\"_open\":false}]}]}]', 1, 1, NULL, NULL, NULL, NULL);
INSERT INTO `core_menus` VALUES (2, 'department menu', '[{\"name\":\"Hot Promotions\",\"url\":\"#\",\"item_model\":\"custom\",\"_open\":false,\"icon\":\"icon-star\",\"model_name\":\"Custom\",\"is_removed\":true},{\"name\":\"Consumer Electrics\",\"url\":\"#\",\"item_model\":\"custom\",\"_open\":false,\"icon\":\"icon-laundry\",\"layout\":\"multi_row\",\"bg\":21,\"model_name\":\"Custom\",\"is_removed\":true,\"children\":[{\"name\":\"Electronics\",\"url\":\"#\",\"item_model\":\"custom\",\"_open\":false,\"layout\":\"\",\"model_name\":\"Custom\",\"is_removed\":true,\"children\":[{\"name\":\"Home Audios & Theaters\",\"url\":\"#\",\"item_model\":\"custom\",\"_open\":false,\"model_name\":\"Custom\",\"is_removed\":true},{\"name\":\"TV & Videos\",\"url\":\"#\",\"item_model\":\"custom\",\"_open\":false,\"model_name\":\"Custom\",\"is_removed\":true},{\"name\":\"Camera, Photos & Videos\",\"url\":\"#\",\"item_model\":\"custom\",\"_open\":false,\"model_name\":\"Custom\",\"is_removed\":true},{\"name\":\"Cellphones & Accessories\",\"url\":\"#\",\"item_model\":\"custom\",\"_open\":false,\"model_name\":\"Custom\",\"is_removed\":true},{\"name\":\"Headphones\",\"url\":\"#\",\"item_model\":\"custom\",\"_open\":false,\"model_name\":\"Custom\",\"is_removed\":true},{\"name\":\"Videogames\",\"url\":\"#\",\"item_model\":\"custom\",\"_open\":false,\"model_name\":\"Custom\",\"is_removed\":true},{\"name\":\"Wireless Speakers\",\"url\":\"#\",\"item_model\":\"custom\",\"_open\":false,\"model_name\":\"Custom\",\"is_removed\":true},{\"name\":\"Office Electronics\",\"url\":\"#\",\"item_model\":\"custom\",\"_open\":false,\"model_name\":\"Custom\",\"is_removed\":true}]},{\"name\":\"Accessories & Parts\",\"url\":\"#\",\"item_model\":\"custom\",\"_open\":false,\"layout\":\"\",\"model_name\":\"Custom\",\"is_removed\":true,\"children\":[{\"name\":\"Digital Cables\",\"url\":\"#\",\"item_model\":\"custom\",\"_open\":false,\"model_name\":\"Custom\",\"is_removed\":true},{\"name\":\"Audio & Video Cables\",\"url\":\"#\",\"item_model\":\"custom\",\"_open\":false,\"model_name\":\"Custom\",\"is_removed\":true},{\"name\":\"Batteries\",\"url\":\"#\",\"item_model\":\"custom\",\"_open\":false,\"model_name\":\"Custom\",\"is_removed\":true},{\"name\":\"Charger\",\"url\":\"#\",\"item_model\":\"custom\",\"_open\":false,\"model_name\":\"Custom\",\"is_removed\":true}]}]},{\"name\":\"Home, Garden & Kitchen\",\"url\":\"#\",\"item_model\":\"custom\",\"_open\":false,\"icon\":\"icon-lampshade\",\"model_name\":\"Custom\",\"is_removed\":true},{\"name\":\"Health & Beauty\",\"url\":\"#\",\"item_model\":\"custom\",\"_open\":false,\"icon\":\"icon-heart-pulse\",\"model_name\":\"Custom\",\"is_removed\":true},{\"name\":\"Jewelry & Watches\",\"url\":\"#\",\"item_model\":\"custom\",\"_open\":false,\"icon\":\"icon-diamond2\",\"model_name\":\"Custom\",\"is_removed\":true},{\"name\":\"Computers & Technologies\",\"url\":\"#\",\"item_model\":\"custom\",\"_open\":false,\"icon\":\"icon-desktop\",\"model_name\":\"Custom\",\"is_removed\":true,\"layout\":\"multi_row\",\"bg\":22,\"children\":[{\"name\":\"Computer & Technologies\",\"url\":\"#\",\"item_model\":\"custom\",\"_open\":false,\"icon\":\"\",\"model_name\":\"Custom\",\"is_removed\":true,\"children\":[{\"name\":\"Computers & Tablets\",\"url\":\"#\",\"item_model\":\"custom\",\"_open\":false,\"model_name\":\"Custom\",\"is_removed\":true},{\"name\":\"Laptop\",\"url\":\"#\",\"item_model\":\"custom\",\"_open\":false,\"model_name\":\"Custom\",\"is_removed\":true},{\"name\":\"Monitors\",\"url\":\"#\",\"item_model\":\"custom\",\"_open\":false,\"model_name\":\"Custom\",\"is_removed\":true},{\"name\":\"Networking\",\"url\":\"#\",\"item_model\":\"custom\",\"_open\":false,\"model_name\":\"Custom\",\"is_removed\":true},{\"name\":\"Drive & Storages\",\"url\":\"#\",\"item_model\":\"custom\",\"_open\":false,\"model_name\":\"Custom\",\"is_removed\":true},{\"name\":\"Computer Components\",\"url\":\"#\",\"item_model\":\"custom\",\"_open\":false,\"model_name\":\"Custom\",\"is_removed\":true},{\"name\":\"Security & Protection\",\"url\":\"#\",\"item_model\":\"custom\",\"_open\":false,\"model_name\":\"Custom\",\"is_removed\":true},{\"name\":\"Gaming Laptop\",\"url\":\"#\",\"item_model\":\"custom\",\"_open\":false,\"model_name\":\"Custom\",\"is_removed\":true},{\"name\":\"Accesories\",\"url\":\"#\",\"item_model\":\"custom\",\"_open\":false,\"model_name\":\"Custom\",\"is_removed\":true}]}]},{\"name\":\"Babies & Moms\",\"url\":\"#\",\"item_model\":\"custom\",\"_open\":false,\"icon\":\"icon-baby-bottle\",\"model_name\":\"Custom\",\"is_removed\":true},{\"name\":\"Sport & Outdoor\",\"url\":\"#\",\"item_model\":\"custom\",\"_open\":false,\"icon\":\"icon-baseball\",\"model_name\":\"Custom\",\"is_removed\":true},{\"name\":\"Phones & Accessories\",\"url\":\"#\",\"item_model\":\"custom\",\"_open\":false,\"icon\":\"icon-smartphone\",\"model_name\":\"Custom\",\"is_removed\":true},{\"name\":\"Books & Office\",\"url\":\"#\",\"item_model\":\"custom\",\"_open\":false,\"icon\":\"icon-book2\",\"model_name\":\"Custom\",\"is_removed\":true},{\"name\":\"Cars & Motocycles\",\"url\":\"#\",\"item_model\":\"custom\",\"_open\":false,\"icon\":\"icon-car-siren\",\"model_name\":\"Custom\",\"is_removed\":true},{\"name\":\"Home Improments\",\"url\":\"#\",\"item_model\":\"custom\",\"_open\":false,\"icon\":\"icon-wrench\",\"model_name\":\"Custom\",\"is_removed\":true},{\"name\":\"Vouchers & Services\",\"url\":\"#\",\"item_model\":\"custom\",\"_open\":false,\"icon\":\"icon-tag\",\"model_name\":\"Custom\",\"is_removed\":true}]', 1, 1, NULL, NULL, NULL, NULL);
INSERT INTO `core_menus` VALUES (3, 'Right menu', '[{\"id\":2,\"name\":\"Sell On Martfury\",\"class\":\"\",\"target\":\"\",\"open\":false,\"item_model\":\"Modules\\\\Page\\\\Models\\\\Page\",\"origin_name\":\"Pages\",\"model_name\":\"Page\",\"_open\":true},{\"name\":\"Track Your Order\",\"url\":\"\\/user\\/orders\",\"item_model\":\"custom\",\"_open\":true}]', 1, 1, NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for core_model_has_permissions
-- ----------------------------
DROP TABLE IF EXISTS `core_model_has_permissions`;
CREATE TABLE `core_model_has_permissions`  (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `model_type` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL,
  PRIMARY KEY (`permission_id`, `model_id`, `model_type`) USING BTREE,
  INDEX `core_model_has_permissions_model_id_model_type_index`(`model_id`, `model_type`) USING BTREE,
  CONSTRAINT `core_model_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `core_permissions` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for core_model_has_roles
-- ----------------------------
DROP TABLE IF EXISTS `core_model_has_roles`;
CREATE TABLE `core_model_has_roles`  (
  `role_id` int(10) UNSIGNED NOT NULL,
  `model_type` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL,
  PRIMARY KEY (`role_id`, `model_id`, `model_type`) USING BTREE,
  INDEX `core_model_has_roles_model_id_model_type_index`(`model_id`, `model_type`) USING BTREE,
  CONSTRAINT `core_model_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `core_roles` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of core_model_has_roles
-- ----------------------------
INSERT INTO `core_model_has_roles` VALUES (2, 'App\\User', 2);
INSERT INTO `core_model_has_roles` VALUES (3, 'App\\User', 1);

-- ----------------------------
-- Table structure for core_news
-- ----------------------------
DROP TABLE IF EXISTS `core_news`;
CREATE TABLE `core_news`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `slug` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `status` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `cat_id` int(11) NULL DEFAULT NULL,
  `image_id` int(11) NULL DEFAULT NULL,
  `create_user` int(11) NULL DEFAULT NULL,
  `update_user` int(11) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  `origin_id` bigint(20) NULL DEFAULT NULL,
  `lang` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of core_news
-- ----------------------------
INSERT INTO `core_news` VALUES (1, 'Experience Great Sound With Beats’s Headphone', ' From the iconic to the unexpected, the city of San Francisco never ceases to surprise. Kick-start your effortlessly delivered Northern California holiday in the cosmopolitan hills of  The City . Join your Travel Director and fellow travellers for a Welcome Reception at your hotel.Welcome Reception  From the iconic to the unexpected, the city of San Francisco never ceases to surprise. Kick-start your effortlessly delivered Northern California holiday in the cosmopolitan hills of  The City . Join your Travel Director and fellow travellers for a Welcome Reception at your hotel.Welcome Reception <br/>From the iconic to the unexpected, the city of San Francisco never ceases to surprise. Kick-start your effortlessly delivered Northern California holiday in the cosmopolitan hills of  The City . Join your Travel Director and fellow travellers for a Welcome Reception at your hotel.Welcome Reception<br/>\r\n    From the iconic to the unexpected, the city of San Francisco never ceases to surprise. Kick-start your effortlessly delivered Northern California holiday in the cosmopolitan hills of  The City . Join your Travel Director and fellow travellers for a Welcome Reception at your hotel.Welcome Reception', 'experience-great-sound-with-beatss-headphone', 'publish', 3, 11, 1, NULL, NULL, NULL, NULL, '2021-04-12 06:15:19', NULL);
INSERT INTO `core_news` VALUES (2, 'Products Necessery For Mom', ' From the iconic to the unexpected, the city of San Francisco never ceases to surprise. Kick-start your effortlessly delivered Northern California holiday in the cosmopolitan hills of  The City . Join your Travel Director and fellow travellers for a Welcome Reception at your hotel.Welcome Reception <br/>From the iconic to the unexpected, the city of San Francisco never ceases to surprise. Kick-start your effortlessly delivered Northern California holiday in the cosmopolitan hills of  The City . Join your Travel Director and fellow travellers for a Welcome Reception at your hotel.Welcome Reception<br/>\r\n    From the iconic to the unexpected, the city of San Francisco never ceases to surprise. Kick-start your effortlessly delivered Northern California holiday in the cosmopolitan hills of  The City . Join your Travel Director and fellow travellers for a Welcome Reception at your hotel.Welcome Reception', 'products-necessery-for-mom', 'publish', 4, 12, 1, NULL, NULL, NULL, NULL, '2021-04-12 06:15:19', NULL);
INSERT INTO `core_news` VALUES (3, 'Home Interior: Modern Style 2017', ' From the iconic to the unexpected, the city of San Francisco never ceases to surprise. Kick-start your effortlessly delivered Northern California holiday in the cosmopolitan hills of  The City . Join your Travel Director and fellow travellers for a Welcome Reception at your hotel.Welcome Reception  From the iconic to the unexpected, the city of San Francisco never ceases to surprise. Kick-start your effortlessly delivered Northern California holiday in the cosmopolitan hills of  The City . Join your Travel Director and fellow travellers for a Welcome Reception at your hotel.Welcome Reception <br/>From the iconic to the unexpected, the city of San Francisco never ceases to surprise. Kick-start your effortlessly delivered Northern California holiday in the cosmopolitan hills of  The City . Join your Travel Director and fellow travellers for a Welcome Reception at your hotel.Welcome Reception<br/>\r\n    From the iconic to the unexpected, the city of San Francisco never ceases to surprise. Kick-start your effortlessly delivered Northern California holiday in the cosmopolitan hills of  The City . Join your Travel Director and fellow travellers for a Welcome Reception at your hotel.Welcome Reception', 'home-interior-modern-style-2017', 'publish', 2, 13, 1, NULL, NULL, NULL, NULL, '2021-04-12 06:15:19', NULL);
INSERT INTO `core_news` VALUES (4, 'A New Look About Startup In Product Manufacture Field', ' From the iconic to the unexpected, the city of San Francisco never ceases to surprise. Kick-start your effortlessly delivered Northern California holiday in the cosmopolitan hills of  The City . Join your Travel Director and fellow travellers for a Welcome Reception at your hotel.Welcome Reception  From the iconic to the unexpected, the city of San Francisco never ceases to surprise. Kick-start your effortlessly delivered Northern California holiday in the cosmopolitan hills of  The City . Join your Travel Director and fellow travellers for a Welcome Reception at your hotel.Welcome Reception <br/>From the iconic to the unexpected, the city of San Francisco never ceases to surprise. Kick-start your effortlessly delivered Northern California holiday in the cosmopolitan hills of  The City . Join your Travel Director and fellow travellers for a Welcome Reception at your hotel.Welcome Reception<br/>\r\n    From the iconic to the unexpected, the city of San Francisco never ceases to surprise. Kick-start your effortlessly delivered Northern California holiday in the cosmopolitan hills of  The City . Join your Travel Director and fellow travellers for a Welcome Reception at your hotel.Welcome Reception', 'a-new-look-about-startup-in-product-manufacture-field', 'publish', 1, 14, 1, NULL, NULL, NULL, NULL, '2021-04-12 06:15:19', NULL);
INSERT INTO `core_news` VALUES (5, 'B&O Play – Best Headphone For You', ' From the iconic to the unexpected, the city of San Francisco never ceases to surprise. Kick-start your effortlessly delivered Northern California holiday in the cosmopolitan hills of  The City . Join your Travel Director and fellow travellers for a Welcome Reception at your hotel.Welcome Reception  From the iconic to the unexpected, the city of San Francisco never ceases to surprise. Kick-start your effortlessly delivered Northern California holiday in the cosmopolitan hills of  The City . Join your Travel Director and fellow travellers for a Welcome Reception at your hotel.Welcome Reception <br/>From the iconic to the unexpected, the city of San Francisco never ceases to surprise. Kick-start your effortlessly delivered Northern California holiday in the cosmopolitan hills of  The City . Join your Travel Director and fellow travellers for a Welcome Reception at your hotel.Welcome Reception<br/>\r\n    From the iconic to the unexpected, the city of San Francisco never ceases to surprise. Kick-start your effortlessly delivered Northern California holiday in the cosmopolitan hills of  The City . Join your Travel Director and fellow travellers for a Welcome Reception at your hotel.Welcome Reception', 'bo-play-best-headphone-for-you', 'publish', 3, 15, 1, NULL, NULL, NULL, NULL, '2021-04-12 06:15:19', NULL);
INSERT INTO `core_news` VALUES (6, 'Unique Products For Your Kitchen From IKEA Design', ' From the iconic to the unexpected, the city of San Francisco never ceases to surprise. Kick-start your effortlessly delivered Northern California holiday in the cosmopolitan hills of  The City . Join your Travel Director and fellow travellers for a Welcome Reception at your hotel.Welcome Reception  From the iconic to the unexpected, the city of San Francisco never ceases to surprise. Kick-start your effortlessly delivered Northern California holiday in the cosmopolitan hills of  The City . Join your Travel Director and fellow travellers for a Welcome Reception at your hotel.Welcome Reception <br/>From the iconic to the unexpected, the city of San Francisco never ceases to surprise. Kick-start your effortlessly delivered Northern California holiday in the cosmopolitan hills of  The City . Join your Travel Director and fellow travellers for a Welcome Reception at your hotel.Welcome Reception<br/>\r\n    From the iconic to the unexpected, the city of San Francisco never ceases to surprise. Kick-start your effortlessly delivered Northern California holiday in the cosmopolitan hills of  The City . Join your Travel Director and fellow travellers for a Welcome Reception at your hotel.Welcome Reception', 'unique-products-for-your-kitchen-from-ikea-design', 'publish', 2, 16, 1, NULL, NULL, NULL, NULL, '2021-04-12 06:15:19', NULL);
INSERT INTO `core_news` VALUES (7, 'Explore Fashion Trending For Guys In Autumn 2017', ' From the iconic to the unexpected, the city of San Francisco never ceases to surprise. Kick-start your effortlessly delivered Northern California holiday in the cosmopolitan hills of  The City . Join your Travel Director and fellow travellers for a Welcome Reception at your hotel.Welcome Reception  From the iconic to the unexpected, the city of San Francisco never ceases to surprise. Kick-start your effortlessly delivered Northern California holiday in the cosmopolitan hills of  The City . Join your Travel Director and fellow travellers for a Welcome Reception at your hotel.Welcome Reception <br/>From the iconic to the unexpected, the city of San Francisco never ceases to surprise. Kick-start your effortlessly delivered Northern California holiday in the cosmopolitan hills of  The City . Join your Travel Director and fellow travellers for a Welcome Reception at your hotel.Welcome Reception<br/>\r\n    From the iconic to the unexpected, the city of San Francisco never ceases to surprise. Kick-start your effortlessly delivered Northern California holiday in the cosmopolitan hills of  The City . Join your Travel Director and fellow travellers for a Welcome Reception at your hotel.Welcome Reception', 'explore-fashion-trending-for-guys-in-autumn-2017', 'publish', 2, 17, 1, NULL, NULL, NULL, NULL, '2021-04-12 06:15:19', NULL);
INSERT INTO `core_news` VALUES (8, 'Compact & Powerful: Cannon Pentack Beside You Go To Anywhere', ' From the iconic to the unexpected, the city of San Francisco never ceases to surprise. Kick-start your effortlessly delivered Northern California holiday in the cosmopolitan hills of  The City . Join your Travel Director and fellow travellers for a Welcome Reception at your hotel.Welcome Reception  From the iconic to the unexpected, the city of San Francisco never ceases to surprise. Kick-start your effortlessly delivered Northern California holiday in the cosmopolitan hills of  The City . Join your Travel Director and fellow travellers for a Welcome Reception at your hotel.Welcome Reception <br/>From the iconic to the unexpected, the city of San Francisco never ceases to surprise. Kick-start your effortlessly delivered Northern California holiday in the cosmopolitan hills of  The City . Join your Travel Director and fellow travellers for a Welcome Reception at your hotel.Welcome Reception<br/>\r\n    From the iconic to the unexpected, the city of San Francisco never ceases to surprise. Kick-start your effortlessly delivered Northern California holiday in the cosmopolitan hills of  The City . Join your Travel Director and fellow travellers for a Welcome Reception at your hotel.Welcome Reception', 'compact-powerful-cannon-pentack-beside-you-go-to-anywhere', 'publish', 1, 18, 1, NULL, NULL, NULL, NULL, '2021-04-12 06:15:19', NULL);

-- ----------------------------
-- Table structure for core_news_category
-- ----------------------------
DROP TABLE IF EXISTS `core_news_category`;
CREATE TABLE `core_news_category`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `slug` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `status` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `_lft` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `_rgt` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `parent_id` int(10) UNSIGNED NULL DEFAULT NULL,
  `create_user` int(11) NULL DEFAULT NULL,
  `update_user` int(11) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `core_news_category__lft__rgt_parent_id_index`(`_lft`, `_rgt`, `parent_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of core_news_category
-- ----------------------------
INSERT INTO `core_news_category` VALUES (1, 'Entertaiment', NULL, 'entertaiment', 'publish', 1, 2, NULL, NULL, NULL, NULL, '2021-04-12 06:15:19', '2021-04-12 06:15:19');
INSERT INTO `core_news_category` VALUES (2, 'Technology', NULL, 'technology', 'publish', 3, 4, NULL, NULL, NULL, NULL, '2021-04-12 06:15:19', '2021-04-12 06:15:19');
INSERT INTO `core_news_category` VALUES (3, 'Life Style ', NULL, 'life-style', 'publish', 5, 6, NULL, NULL, NULL, NULL, '2021-04-12 06:15:19', '2021-04-12 06:15:19');
INSERT INTO `core_news_category` VALUES (4, 'Others', NULL, 'others', 'publish', 7, 8, NULL, NULL, NULL, NULL, '2021-04-12 06:15:19', '2021-04-12 06:15:19');
INSERT INTO `core_news_category` VALUES (5, 'Business', NULL, 'business', 'publish', 9, 10, NULL, NULL, NULL, NULL, '2021-04-12 06:15:19', '2021-04-12 06:15:19');
INSERT INTO `core_news_category` VALUES (6, 'Fashion', NULL, 'fashion', 'publish', 11, 12, NULL, NULL, NULL, NULL, '2021-04-12 06:15:19', '2021-04-12 06:15:19');

-- ----------------------------
-- Table structure for core_news_category_translations
-- ----------------------------
DROP TABLE IF EXISTS `core_news_category_translations`;
CREATE TABLE `core_news_category_translations`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `origin_id` int(10) UNSIGNED NOT NULL,
  `locale` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `create_user` int(11) NULL DEFAULT NULL,
  `update_user` int(11) NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `core_news_category_translations_locale_index`(`locale`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for core_news_tag
-- ----------------------------
DROP TABLE IF EXISTS `core_news_tag`;
CREATE TABLE `core_news_tag`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `news_id` int(11) NULL DEFAULT NULL,
  `tag_id` int(11) NULL DEFAULT NULL,
  `create_user` int(11) NULL DEFAULT NULL,
  `update_user` int(11) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for core_news_translations
-- ----------------------------
DROP TABLE IF EXISTS `core_news_translations`;
CREATE TABLE `core_news_translations`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `origin_id` int(10) UNSIGNED NOT NULL,
  `locale` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `create_user` int(11) NULL DEFAULT NULL,
  `update_user` int(11) NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `core_news_translations_locale_index`(`locale`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for core_notifications
-- ----------------------------
DROP TABLE IF EXISTS `core_notifications`;
CREATE TABLE `core_notifications`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `from_user` bigint(20) NULL DEFAULT NULL,
  `to_user` bigint(20) NULL DEFAULT NULL,
  `is_read` tinyint(4) NULL DEFAULT 0,
  `type` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `type_group` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `target_id` bigint(20) NULL DEFAULT NULL,
  `target_parent_id` bigint(20) NULL DEFAULT NULL,
  `params` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `create_user` int(11) NULL DEFAULT NULL,
  `update_user` int(11) NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for core_page_translations
-- ----------------------------
DROP TABLE IF EXISTS `core_page_translations`;
CREATE TABLE `core_page_translations`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `origin_id` int(10) UNSIGNED NOT NULL,
  `locale` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `short_desc` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `create_user` bigint(20) NULL DEFAULT NULL,
  `update_user` bigint(20) NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `core_page_translations_origin_id_locale_unique`(`origin_id`, `locale`) USING BTREE,
  INDEX `core_page_translations_locale_index`(`locale`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for core_pages
-- ----------------------------
DROP TABLE IF EXISTS `core_pages`;
CREATE TABLE `core_pages`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `slug` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `short_desc` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `status` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `publish_date` datetime(0) NULL DEFAULT NULL,
  `image_id` int(11) NULL DEFAULT NULL,
  `c_background` int(11) NULL DEFAULT NULL,
  `template_id` int(11) NULL DEFAULT NULL,
  `page_style` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `show_breadcrumb` tinyint(4) NULL DEFAULT NULL,
  `create_user` bigint(20) NULL DEFAULT NULL,
  `update_user` bigint(20) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `core_pages_slug_index`(`slug`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of core_pages
-- ----------------------------
INSERT INTO `core_pages` VALUES (1, 'home-page', 'Home Page', NULL, NULL, 'publish', NULL, NULL, NULL, 2, NULL, NULL, 1, NULL, NULL, '2021-04-12 06:15:19', NULL);
INSERT INTO `core_pages` VALUES (2, 'become-a-vendor', 'Become a Vendor', NULL, NULL, 'publish', NULL, NULL, NULL, 1, NULL, NULL, 1, NULL, NULL, '2021-04-12 06:15:19', NULL);
INSERT INTO `core_pages` VALUES (3, 'home-page-2', 'Home page 2', NULL, NULL, 'publish', NULL, NULL, 20, 3, '{\"header\":\"1\",\"footer\":\"1\"}', NULL, 1, NULL, NULL, '2021-04-12 06:15:19', NULL);

-- ----------------------------
-- Table structure for core_permissions
-- ----------------------------
DROP TABLE IF EXISTS `core_permissions`;
CREATE TABLE `core_permissions`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 53 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of core_permissions
-- ----------------------------
INSERT INTO `core_permissions` VALUES (1, 'report_view', 'web', '2021-04-12 06:15:18', '2021-04-12 06:15:18');
INSERT INTO `core_permissions` VALUES (2, 'contact_manage', 'web', '2021-04-12 06:15:18', '2021-04-12 06:15:18');
INSERT INTO `core_permissions` VALUES (3, 'newsletter_manage', 'web', '2021-04-12 06:15:18', '2021-04-12 06:15:18');
INSERT INTO `core_permissions` VALUES (4, 'language_manage', 'web', '2021-04-12 06:15:18', '2021-04-12 06:15:18');
INSERT INTO `core_permissions` VALUES (5, 'language_translation', 'web', '2021-04-12 06:15:18', '2021-04-12 06:15:18');
INSERT INTO `core_permissions` VALUES (6, 'booking_view', 'web', '2021-04-12 06:15:18', '2021-04-12 06:15:18');
INSERT INTO `core_permissions` VALUES (7, 'booking_update', 'web', '2021-04-12 06:15:18', '2021-04-12 06:15:18');
INSERT INTO `core_permissions` VALUES (8, 'booking_manage_others', 'web', '2021-04-12 06:15:18', '2021-04-12 06:15:18');
INSERT INTO `core_permissions` VALUES (9, 'template_view', 'web', '2021-04-12 06:15:18', '2021-04-12 06:15:18');
INSERT INTO `core_permissions` VALUES (10, 'template_create', 'web', '2021-04-12 06:15:18', '2021-04-12 06:15:18');
INSERT INTO `core_permissions` VALUES (11, 'template_update', 'web', '2021-04-12 06:15:18', '2021-04-12 06:15:18');
INSERT INTO `core_permissions` VALUES (12, 'template_delete', 'web', '2021-04-12 06:15:18', '2021-04-12 06:15:18');
INSERT INTO `core_permissions` VALUES (13, 'news_view', 'web', '2021-04-12 06:15:18', '2021-04-12 06:15:18');
INSERT INTO `core_permissions` VALUES (14, 'news_create', 'web', '2021-04-12 06:15:18', '2021-04-12 06:15:18');
INSERT INTO `core_permissions` VALUES (15, 'news_update', 'web', '2021-04-12 06:15:18', '2021-04-12 06:15:18');
INSERT INTO `core_permissions` VALUES (16, 'news_delete', 'web', '2021-04-12 06:15:18', '2021-04-12 06:15:18');
INSERT INTO `core_permissions` VALUES (17, 'news_manage_others', 'web', '2021-04-12 06:15:19', '2021-04-12 06:15:19');
INSERT INTO `core_permissions` VALUES (18, 'role_view', 'web', '2021-04-12 06:15:19', '2021-04-12 06:15:19');
INSERT INTO `core_permissions` VALUES (19, 'role_create', 'web', '2021-04-12 06:15:19', '2021-04-12 06:15:19');
INSERT INTO `core_permissions` VALUES (20, 'role_update', 'web', '2021-04-12 06:15:19', '2021-04-12 06:15:19');
INSERT INTO `core_permissions` VALUES (21, 'role_delete', 'web', '2021-04-12 06:15:19', '2021-04-12 06:15:19');
INSERT INTO `core_permissions` VALUES (22, 'permission_view', 'web', '2021-04-12 06:15:19', '2021-04-12 06:15:19');
INSERT INTO `core_permissions` VALUES (23, 'permission_create', 'web', '2021-04-12 06:15:19', '2021-04-12 06:15:19');
INSERT INTO `core_permissions` VALUES (24, 'permission_update', 'web', '2021-04-12 06:15:19', '2021-04-12 06:15:19');
INSERT INTO `core_permissions` VALUES (25, 'permission_delete', 'web', '2021-04-12 06:15:19', '2021-04-12 06:15:19');
INSERT INTO `core_permissions` VALUES (26, 'dashboard_access', 'web', '2021-04-12 06:15:19', '2021-04-12 06:15:19');
INSERT INTO `core_permissions` VALUES (27, 'dashboard_vendor_access', 'web', '2021-04-12 06:15:19', '2021-04-12 06:15:19');
INSERT INTO `core_permissions` VALUES (28, 'setting_update', 'web', '2021-04-12 06:15:19', '2021-04-12 06:15:19');
INSERT INTO `core_permissions` VALUES (29, 'menu_view', 'web', '2021-04-12 06:15:19', '2021-04-12 06:15:19');
INSERT INTO `core_permissions` VALUES (30, 'menu_create', 'web', '2021-04-12 06:15:19', '2021-04-12 06:15:19');
INSERT INTO `core_permissions` VALUES (31, 'menu_update', 'web', '2021-04-12 06:15:19', '2021-04-12 06:15:19');
INSERT INTO `core_permissions` VALUES (32, 'menu_delete', 'web', '2021-04-12 06:15:19', '2021-04-12 06:15:19');
INSERT INTO `core_permissions` VALUES (33, 'user_view', 'web', '2021-04-12 06:15:19', '2021-04-12 06:15:19');
INSERT INTO `core_permissions` VALUES (34, 'user_create', 'web', '2021-04-12 06:15:19', '2021-04-12 06:15:19');
INSERT INTO `core_permissions` VALUES (35, 'user_update', 'web', '2021-04-12 06:15:19', '2021-04-12 06:15:19');
INSERT INTO `core_permissions` VALUES (36, 'user_delete', 'web', '2021-04-12 06:15:19', '2021-04-12 06:15:19');
INSERT INTO `core_permissions` VALUES (37, 'page_view', 'web', '2021-04-12 06:15:19', '2021-04-12 06:15:19');
INSERT INTO `core_permissions` VALUES (38, 'page_create', 'web', '2021-04-12 06:15:19', '2021-04-12 06:15:19');
INSERT INTO `core_permissions` VALUES (39, 'page_update', 'web', '2021-04-12 06:15:19', '2021-04-12 06:15:19');
INSERT INTO `core_permissions` VALUES (40, 'page_delete', 'web', '2021-04-12 06:15:19', '2021-04-12 06:15:19');
INSERT INTO `core_permissions` VALUES (41, 'page_manage_others', 'web', '2021-04-12 06:15:19', '2021-04-12 06:15:19');
INSERT INTO `core_permissions` VALUES (42, 'setting_view', 'web', '2021-04-12 06:15:19', '2021-04-12 06:15:19');
INSERT INTO `core_permissions` VALUES (43, 'media_upload', 'web', '2021-04-12 06:15:19', '2021-04-12 06:15:19');
INSERT INTO `core_permissions` VALUES (44, 'media_manage', 'web', '2021-04-12 06:15:19', '2021-04-12 06:15:19');
INSERT INTO `core_permissions` VALUES (45, 'review_manage_others', 'web', '2021-04-12 06:15:19', '2021-04-12 06:15:19');
INSERT INTO `core_permissions` VALUES (46, 'system_log_view', 'web', '2021-04-12 06:15:19', '2021-04-12 06:15:19');
INSERT INTO `core_permissions` VALUES (47, 'product_view', 'web', '2021-04-12 06:15:19', '2021-04-12 06:15:19');
INSERT INTO `core_permissions` VALUES (48, 'product_create', 'web', '2021-04-12 06:15:19', '2021-04-12 06:15:19');
INSERT INTO `core_permissions` VALUES (49, 'product_update', 'web', '2021-04-12 06:15:19', '2021-04-12 06:15:19');
INSERT INTO `core_permissions` VALUES (50, 'product_delete', 'web', '2021-04-12 06:15:19', '2021-04-12 06:15:19');
INSERT INTO `core_permissions` VALUES (51, 'product_manage_others', 'web', '2021-04-12 06:15:19', '2021-04-12 06:15:19');
INSERT INTO `core_permissions` VALUES (52, 'product_manage_attributes', 'web', '2021-04-12 06:15:19', '2021-04-12 06:15:19');

-- ----------------------------
-- Table structure for core_role_has_permissions
-- ----------------------------
DROP TABLE IF EXISTS `core_role_has_permissions`;
CREATE TABLE `core_role_has_permissions`  (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`permission_id`, `role_id`) USING BTREE,
  INDEX `core_role_has_permissions_role_id_foreign`(`role_id`) USING BTREE,
  CONSTRAINT `core_role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `core_permissions` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT,
  CONSTRAINT `core_role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `core_roles` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of core_role_has_permissions
-- ----------------------------
INSERT INTO `core_role_has_permissions` VALUES (1, 3);
INSERT INTO `core_role_has_permissions` VALUES (2, 3);
INSERT INTO `core_role_has_permissions` VALUES (3, 3);
INSERT INTO `core_role_has_permissions` VALUES (4, 3);
INSERT INTO `core_role_has_permissions` VALUES (5, 3);
INSERT INTO `core_role_has_permissions` VALUES (6, 3);
INSERT INTO `core_role_has_permissions` VALUES (7, 3);
INSERT INTO `core_role_has_permissions` VALUES (8, 3);
INSERT INTO `core_role_has_permissions` VALUES (9, 3);
INSERT INTO `core_role_has_permissions` VALUES (10, 3);
INSERT INTO `core_role_has_permissions` VALUES (11, 3);
INSERT INTO `core_role_has_permissions` VALUES (12, 3);
INSERT INTO `core_role_has_permissions` VALUES (13, 3);
INSERT INTO `core_role_has_permissions` VALUES (14, 3);
INSERT INTO `core_role_has_permissions` VALUES (15, 3);
INSERT INTO `core_role_has_permissions` VALUES (16, 3);
INSERT INTO `core_role_has_permissions` VALUES (17, 3);
INSERT INTO `core_role_has_permissions` VALUES (18, 3);
INSERT INTO `core_role_has_permissions` VALUES (19, 3);
INSERT INTO `core_role_has_permissions` VALUES (20, 3);
INSERT INTO `core_role_has_permissions` VALUES (21, 3);
INSERT INTO `core_role_has_permissions` VALUES (22, 3);
INSERT INTO `core_role_has_permissions` VALUES (23, 3);
INSERT INTO `core_role_has_permissions` VALUES (24, 3);
INSERT INTO `core_role_has_permissions` VALUES (25, 3);
INSERT INTO `core_role_has_permissions` VALUES (26, 3);
INSERT INTO `core_role_has_permissions` VALUES (27, 1);
INSERT INTO `core_role_has_permissions` VALUES (27, 3);
INSERT INTO `core_role_has_permissions` VALUES (28, 3);
INSERT INTO `core_role_has_permissions` VALUES (29, 3);
INSERT INTO `core_role_has_permissions` VALUES (30, 3);
INSERT INTO `core_role_has_permissions` VALUES (31, 3);
INSERT INTO `core_role_has_permissions` VALUES (32, 3);
INSERT INTO `core_role_has_permissions` VALUES (33, 3);
INSERT INTO `core_role_has_permissions` VALUES (34, 3);
INSERT INTO `core_role_has_permissions` VALUES (35, 3);
INSERT INTO `core_role_has_permissions` VALUES (36, 3);
INSERT INTO `core_role_has_permissions` VALUES (37, 3);
INSERT INTO `core_role_has_permissions` VALUES (38, 3);
INSERT INTO `core_role_has_permissions` VALUES (39, 3);
INSERT INTO `core_role_has_permissions` VALUES (40, 3);
INSERT INTO `core_role_has_permissions` VALUES (41, 3);
INSERT INTO `core_role_has_permissions` VALUES (42, 3);
INSERT INTO `core_role_has_permissions` VALUES (43, 1);
INSERT INTO `core_role_has_permissions` VALUES (43, 3);
INSERT INTO `core_role_has_permissions` VALUES (44, 3);
INSERT INTO `core_role_has_permissions` VALUES (45, 3);
INSERT INTO `core_role_has_permissions` VALUES (46, 3);
INSERT INTO `core_role_has_permissions` VALUES (47, 1);
INSERT INTO `core_role_has_permissions` VALUES (47, 3);
INSERT INTO `core_role_has_permissions` VALUES (48, 1);
INSERT INTO `core_role_has_permissions` VALUES (48, 3);
INSERT INTO `core_role_has_permissions` VALUES (49, 1);
INSERT INTO `core_role_has_permissions` VALUES (49, 3);
INSERT INTO `core_role_has_permissions` VALUES (50, 1);
INSERT INTO `core_role_has_permissions` VALUES (50, 3);
INSERT INTO `core_role_has_permissions` VALUES (51, 3);
INSERT INTO `core_role_has_permissions` VALUES (52, 3);

-- ----------------------------
-- Table structure for core_roles
-- ----------------------------
DROP TABLE IF EXISTS `core_roles`;
CREATE TABLE `core_roles`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `origin_id` bigint(20) NULL DEFAULT NULL,
  `lang` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of core_roles
-- ----------------------------
INSERT INTO `core_roles` VALUES (1, 'vendor', 'web', NULL, NULL, '2021-04-12 06:15:19', '2021-04-12 06:15:19');
INSERT INTO `core_roles` VALUES (2, 'customer', 'web', NULL, NULL, '2021-04-12 06:15:19', '2021-04-12 06:15:19');
INSERT INTO `core_roles` VALUES (3, 'administrator', 'web', NULL, NULL, '2021-04-12 06:15:19', '2021-04-12 06:15:19');

-- ----------------------------
-- Table structure for core_settings
-- ----------------------------
DROP TABLE IF EXISTS `core_settings`;
CREATE TABLE `core_settings`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `group` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `val` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `autoload` tinyint(4) NULL DEFAULT NULL,
  `create_user` int(11) NULL DEFAULT NULL,
  `update_user` int(11) NULL DEFAULT NULL,
  `lang` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 95 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of core_settings
-- ----------------------------
INSERT INTO `core_settings` VALUES (1, 'site_locale', 'general', 'en', NULL, 1, 1, NULL, NULL, '2021-04-15 09:13:24');
INSERT INTO `core_settings` VALUES (2, 'site_enable_multi_lang', 'general', '1', NULL, 1, 1, NULL, NULL, '2021-04-15 09:13:24');
INSERT INTO `core_settings` VALUES (3, 'menu_locations', 'general', '{\"primary\":1,\"department\":2,\"menu_right\":3}', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `core_settings` VALUES (4, 'admin_email', 'general', 'contact@bookingcore.com', NULL, 1, 1, NULL, NULL, '2021-04-15 09:13:24');
INSERT INTO `core_settings` VALUES (5, 'email_from_name', 'general', 'Martfury', NULL, 1, 1, NULL, NULL, '2021-04-15 09:13:24');
INSERT INTO `core_settings` VALUES (6, 'email_from_address', 'general', 'contact@bookingcore.com', NULL, 1, 1, NULL, NULL, '2021-04-15 09:13:24');
INSERT INTO `core_settings` VALUES (7, 'logo_id', 'general', '104', NULL, 1, 1, NULL, NULL, '2021-04-19 03:11:11');
INSERT INTO `core_settings` VALUES (8, 'site_favicon', 'general', NULL, NULL, 1, 1, NULL, NULL, '2021-04-15 09:13:24');
INSERT INTO `core_settings` VALUES (9, 'topbar_left_text', 'general', '<div class=\"socials\">\r\n                                        <a href=\"#\"><i class=\"fa fa-facebook\"></i></a>\r\n                                        <a href=\"#\"><i class=\"fa fa-linkedin\"></i></a>\r\n                                        <a href=\"#\"><i class=\"fa fa-google-plus\"></i></a>\r\n                                    </div>\r\n                                    <span class=\"line\"></span>\r\n                                    <a href=\"mailto:contact@bookingcore.com\">contact@bookingcore.com</a>', NULL, 1, 1, NULL, NULL, '2021-04-15 09:13:24');
INSERT INTO `core_settings` VALUES (10, 'footer_text_left', 'general', '<p>&copy; 2021 Martfury. All Rights Reserved</p>', NULL, 1, 1, NULL, NULL, '2021-04-15 09:13:24');
INSERT INTO `core_settings` VALUES (11, 'footer_text_right', 'general', '<div class=\"text\">\r\n    <p>We Using Safe Payment For</p>\r\n</div>\r\n<ul class=\"payments\">\r\n    <li>\r\n        <img src=\"/images/p1.jpg\" alt=\"p1\">\r\n    </li>\r\n    <li>\r\n        <img src=\"/images/p2.jpg\" alt=\"p2\">\r\n    </li>\r\n    <li>\r\n        <img src=\"/images/p3.jpg\" alt=\"p3\">\r\n    </li>\r\n    <li>\r\n        <img src=\"/images/p4.jpg\" alt=\"p4\">\r\n    </li>\r\n    <li>\r\n        <img src=\"/images/p5.jpg\" alt=\"p5\">\r\n    </li>\r\n    <li>\r\n        <img src=\"/images/p6.jpg\" alt=\"p6\">\r\n    </li>\r\n</ul>', NULL, 1, 1, NULL, NULL, '2021-04-15 09:13:24');
INSERT INTO `core_settings` VALUES (12, 'list_widget_footer', 'general', '[{\"title\":\"NEED HELP?\",\"size\":\"3\",\"content\":\"<div class=\\\"contact\\\">\\r\\n        <div class=\\\"c-title\\\">\\r\\n            Call Us\\r\\n        <\\/div>\\r\\n        <div class=\\\"sub\\\">\\r\\n            + 00 222 44 5678\\r\\n        <\\/div>\\r\\n    <\\/div>\\r\\n    <div class=\\\"contact\\\">\\r\\n        <div class=\\\"c-title\\\">\\r\\n            Email for Us\\r\\n        <\\/div>\\r\\n        <div class=\\\"sub\\\">\\r\\n            hello@yoursite.com\\r\\n        <\\/div>\\r\\n    <\\/div>\\r\\n    <div class=\\\"contact\\\">\\r\\n        <div class=\\\"c-title\\\">\\r\\n            Follow Us\\r\\n        <\\/div>\\r\\n        <div class=\\\"sub\\\">\\r\\n            <a href=\\\"#\\\">\\r\\n                <i class=\\\"icofont-facebook\\\"><\\/i>\\r\\n            <\\/a>\\r\\n            <a href=\\\"#\\\">\\r\\n               <i class=\\\"icofont-twitter\\\"><\\/i>\\r\\n            <\\/a>\\r\\n            <a href=\\\"#\\\">\\r\\n                <i class=\\\"icofont-youtube-play\\\"><\\/i>\\r\\n            <\\/a>\\r\\n        <\\/div>\\r\\n    <\\/div>\"},{\"title\":\"COMPANY\",\"size\":\"3\",\"content\":\"<ul>\\r\\n    <li><a href=\\\"#\\\">About Us<\\/a><\\/li>\\r\\n    <li><a href=\\\"#\\\">Community Blog<\\/a><\\/li>\\r\\n    <li><a href=\\\"#\\\">Rewards<\\/a><\\/li>\\r\\n    <li><a href=\\\"#\\\">Work with Us<\\/a><\\/li>\\r\\n    <li><a href=\\\"#\\\">Meet the Team<\\/a><\\/li>\\r\\n<\\/ul>\"},{\"title\":\"SUPPORT\",\"size\":\"3\",\"content\":\"<ul>\\r\\n    <li><a href=\\\"#\\\">Account<\\/a><\\/li>\\r\\n    <li><a href=\\\"#\\\">Legal<\\/a><\\/li>\\r\\n    <li><a href=\\\"#\\\">Contact<\\/a><\\/li>\\r\\n    <li><a href=\\\"#\\\">Affiliate Program<\\/a><\\/li>\\r\\n    <li><a href=\\\"#\\\">Privacy Policy<\\/a><\\/li>\\r\\n<\\/ul>\"},{\"title\":\"SETTINGS\",\"size\":\"3\",\"content\":\"<ul>\\r\\n<li><a href=\\\"#\\\">Setting 1<\\/a><\\/li>\\r\\n<li><a href=\\\"#\\\">Setting 2<\\/a><\\/li>\\r\\n<\\/ul>\"}]', NULL, 1, 1, NULL, NULL, '2021-04-15 09:13:24');
INSERT INTO `core_settings` VALUES (13, 'list_widget_footer_ja', 'general', '[{\"title\":\"\\u52a9\\u3051\\u304c\\u5fc5\\u8981\\uff1f\",\"size\":\"3\",\"content\":\"<div class=\\\"contact\\\">\\r\\n        <div class=\\\"c-title\\\">\\r\\n            \\u304a\\u96fb\\u8a71\\u304f\\u3060\\u3055\\u3044\\r\\n        <\\/div>\\r\\n        <div class=\\\"sub\\\">\\r\\n            + 00 222 44 5678\\r\\n        <\\/div>\\r\\n    <\\/div>\\r\\n    <div class=\\\"contact\\\">\\r\\n        <div class=\\\"c-title\\\">\\r\\n            \\u90f5\\u4fbf\\u7269\\r\\n        <\\/div>\\r\\n        <div class=\\\"sub\\\">\\r\\n            hello@yoursite.com\\r\\n        <\\/div>\\r\\n    <\\/div>\\r\\n    <div class=\\\"contact\\\">\\r\\n        <div class=\\\"c-title\\\">\\r\\n            \\u30d5\\u30a9\\u30ed\\u30fc\\u3059\\u308b\\r\\n        <\\/div>\\r\\n        <div class=\\\"sub\\\">\\r\\n            <a href=\\\"#\\\">\\r\\n                <i class=\\\"icofont-facebook\\\"><\\/i>\\r\\n            <\\/a>\\r\\n            <a href=\\\"#\\\">\\r\\n                <i class=\\\"icofont-twitter\\\"><\\/i>\\r\\n            <\\/a>\\r\\n            <a href=\\\"#\\\">\\r\\n                <i class=\\\"icofont-youtube-play\\\"><\\/i>\\r\\n            <\\/a>\\r\\n        <\\/div>\\r\\n    <\\/div>\"},{\"title\":\"\\u4f1a\\u793e\",\"size\":\"3\",\"content\":\"<ul>\\r\\n    <li><a href=\\\"#\\\">\\u7d04, \\u7565<\\/a><\\/li>\\r\\n    <li><a href=\\\"#\\\">\\u30b3\\u30df\\u30e5\\u30cb\\u30c6\\u30a3\\u30d6\\u30ed\\u30b0<\\/a><\\/li>\\r\\n    <li><a href=\\\"#\\\">\\u5831\\u916c<\\/a><\\/li>\\r\\n    <li><a href=\\\"#\\\">\\u3068\\u9023\\u643a<\\/a><\\/li>\\r\\n    <li><a href=\\\"#\\\">\\u30c1\\u30fc\\u30e0\\u306b\\u4f1a\\u3046<\\/a><\\/li>\\r\\n<\\/ul>\"},{\"title\":\"\\u30b5\\u30dd\\u30fc\\u30c8\",\"size\":\"3\",\"content\":\"<ul>\\r\\n    <li><a href=\\\"#\\\">\\u30a2\\u30ab\\u30a6\\u30f3\\u30c8<\\/a><\\/li>\\r\\n    <li><a href=\\\"#\\\">\\u6cd5\\u7684<\\/a><\\/li>\\r\\n    <li><a href=\\\"#\\\">\\u63a5\\u89e6<\\/a><\\/li>\\r\\n    <li><a href=\\\"#\\\">\\u30a2\\u30d5\\u30a3\\u30ea\\u30a8\\u30a4\\u30c8\\u30d7\\u30ed\\u30b0\\u30e9\\u30e0<\\/a><\\/li>\\r\\n    <li><a href=\\\"#\\\">\\u500b\\u4eba\\u60c5\\u5831\\u4fdd\\u8b77\\u65b9\\u91dd<\\/a><\\/li>\\r\\n<\\/ul>\"},{\"title\":\"\\u8a2d\\u5b9a\",\"size\":\"3\",\"content\":\"<ul>\\r\\n<li><a href=\\\"#\\\">\\u8a2d\\u5b9a1<\\/a><\\/li>\\r\\n<li><a href=\\\"#\\\">\\u8a2d\\u5b9a2<\\/a><\\/li>\\r\\n<\\/ul>\"}]', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `core_settings` VALUES (14, 'page_contact_title', 'general', 'We\'d love to hear from you', NULL, 1, 1, NULL, NULL, '2021-04-15 09:13:24');
INSERT INTO `core_settings` VALUES (15, 'page_contact_sub_title', 'general', 'Send us a message and we\'ll respond as soon as possible', NULL, 1, 1, NULL, NULL, '2021-04-15 09:13:24');
INSERT INTO `core_settings` VALUES (16, 'page_contact_desc', 'general', '<h3>Martfury</h3>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>Tell. + 00 222 444 33</p>\r\n<p>Email. hello@yoursite.com</p>\r\n<p>1355 Market St, Suite 900San, Francisco, CA 94103 United States</p>', NULL, 1, 1, NULL, NULL, '2021-04-15 09:13:24');
INSERT INTO `core_settings` VALUES (17, 'page_contact_image', 'general', '9', NULL, 1, 1, NULL, NULL, '2021-04-15 09:13:24');
INSERT INTO `core_settings` VALUES (18, 'home_page_id', 'general', '1', NULL, 1, 1, NULL, NULL, '2021-04-15 09:13:24');
INSERT INTO `core_settings` VALUES (19, 'page_contact_title', 'general', 'We\'d love to hear from you', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `core_settings` VALUES (20, 'page_contact_title_ja', 'general', 'あなたからの御一報をお待ち', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `core_settings` VALUES (21, 'page_contact_sub_title', 'general', 'Send us a message and we\'ll respond as soon as possible', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `core_settings` VALUES (22, 'page_contact_sub_title_ja', 'general', '私たちにメッセージを送ってください、私たちはできるだ', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `core_settings` VALUES (23, 'page_contact_desc', 'general', '<!DOCTYPE html><html><head></head><body><h3>Martfury</h3><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>Tell. + 00 222 444 33</p><p>Email. hello@yoursite.com</p><p>1355 Market St, Suite 900San, Francisco, CA 94103 United States</p></body></html>', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `core_settings` VALUES (24, 'page_contact_image', 'general', '9', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `core_settings` VALUES (25, 'currency_main', 'payment', 'usd', NULL, 1, 1, NULL, NULL, '2021-05-05 05:36:09');
INSERT INTO `core_settings` VALUES (26, 'currency_format', 'payment', 'right_space', NULL, 1, 1, NULL, NULL, '2021-05-05 05:40:38');
INSERT INTO `core_settings` VALUES (27, 'currency_decimal', 'payment', NULL, NULL, 1, 1, NULL, NULL, '2021-05-05 05:36:09');
INSERT INTO `core_settings` VALUES (28, 'currency_thousand', 'payment', NULL, NULL, 1, 1, NULL, NULL, '2021-05-05 05:40:38');
INSERT INTO `core_settings` VALUES (29, 'currency_no_decimal', 'payment', '0', NULL, 1, 1, NULL, NULL, '2021-05-05 05:36:09');
INSERT INTO `core_settings` VALUES (30, 'map_provider', 'advance', 'gmap', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `core_settings` VALUES (31, 'map_gmap_key', 'advance', '', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `core_settings` VALUES (32, 'g_offline_payment_enable', 'payment', '1', NULL, 1, 1, NULL, NULL, '2021-05-05 05:36:09');
INSERT INTO `core_settings` VALUES (33, 'g_offline_payment_name', 'payment', 'Offline Payment', NULL, 1, 1, NULL, NULL, '2021-05-05 05:36:09');
INSERT INTO `core_settings` VALUES (34, 'date_format', 'general', 'm/d/Y', NULL, 1, 1, NULL, NULL, '2021-04-15 09:13:24');
INSERT INTO `core_settings` VALUES (35, 'site_title', 'general', 'Websieukhung', NULL, 1, 1, NULL, NULL, '2021-04-19 03:05:45');
INSERT INTO `core_settings` VALUES (36, 'site_timezone', 'general', 'UTC', NULL, 1, 1, NULL, NULL, '2021-04-15 09:13:24');
INSERT INTO `core_settings` VALUES (37, 'site_title', 'general', 'Martfury', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `core_settings` VALUES (38, 'email_header', 'general', '<h1 class=\"site-title\" style=\"text-align: center\">Martfury</h1>', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `core_settings` VALUES (39, 'email_footer', 'general', '<p class=\"\" style=\"text-align: center\">&copy; 2019 Martfury. All rights reserved</p>', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `core_settings` VALUES (40, 'enable_mail_user_registered', 'user', '', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `core_settings` VALUES (41, 'user_content_email_registered', 'user', '<h1 style=\"text-align: center\">Welcome!</h1>\r\n						<h3>Hello [first_name] [last_name]</h3>\r\n						<p>Thank you for signing up with Martfury! We hope you enjoy your time with us.</p>\r\n						<p>Regards,</p>\r\n						<p>Martfury</p>', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `core_settings` VALUES (42, 'admin_enable_mail_user_registered', 'user', '', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `core_settings` VALUES (43, 'admin_content_email_user_registered', 'user', '<h3>Hello Administrator</h3>\r\n						<p>We have new registration</p>\r\n						<p>Full name: [first_name] [last_name]</p>\r\n						<p>Email: [email]</p>\r\n						<p>Regards,</p>\r\n						<p>Martfury</p>', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `core_settings` VALUES (44, 'user_content_email_forget_password', 'user', '<h1>Hello!</h1>\r\n						<p>You are receiving this email because we received a password reset request for your account.</p>\r\n						<p style=\"text-align: center\">[button_reset_password]</p>\r\n						<p>This password reset link expire in 60 minutes.</p>\r\n						<p>If you did not request a password reset, no further action is required.\r\n						</p>\r\n						<p>Regards,</p>\r\n						<p>Martfury</p>', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `core_settings` VALUES (45, 'email_driver', 'email', 'sendmail', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `core_settings` VALUES (46, 'email_host', 'email', 'smtp.mailgun.org', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `core_settings` VALUES (47, 'email_port', 'email', '587', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `core_settings` VALUES (48, 'email_encryption', 'email', 'tls', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `core_settings` VALUES (49, 'email_username', 'email', '', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `core_settings` VALUES (50, 'email_password', 'email', '', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `core_settings` VALUES (51, 'email_mailgun_domain', 'email', '', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `core_settings` VALUES (52, 'email_mailgun_secret', 'email', '', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `core_settings` VALUES (53, 'email_mailgun_endpoint', 'email', 'api.mailgun.net', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `core_settings` VALUES (54, 'email_postmark_token', 'email', '', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `core_settings` VALUES (55, 'email_ses_key', 'email', '', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `core_settings` VALUES (56, 'email_ses_secret', 'email', '', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `core_settings` VALUES (57, 'email_ses_region', 'email', 'us-east-1', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `core_settings` VALUES (58, 'email_sparkpost_secret', 'email', '', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `core_settings` VALUES (59, 'vendor_enable', 'vendor', '1', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `core_settings` VALUES (60, 'vendor_commission_type', 'vendor', 'percent', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `core_settings` VALUES (61, 'vendor_commission_amount', 'vendor', '10', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `core_settings` VALUES (62, 'vendor_role', 'vendor', '1', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `core_settings` VALUES (63, 'footer_categories', 'general', '<div class=\"widget widget_nav_menu\">\r\n    <h4 class=\"widget-title\">Consumer Electric:</h4>\r\n    <div class=\"menu-footer\">\r\n        <ul id=\"menu-footer-link\" class=\"menu\">\r\n            <li><a href=\"#\">Air Conditioners</a></li>\r\n            <li><a href=\"#\">Audios &amp; Theaters</a></li>\r\n            <li><a href=\"#\">Car Electronics</a></li>\r\n            <li><a href=\"#\">Office Electronics</a></li>\r\n            <li><a href=\"#\">TV Televisions</a></li>\r\n            <li><a href=\"#\">Washing Machines</a></li>\r\n        </ul>\r\n    </div>\r\n</div>\r\n<div class=\"widget widget_nav_menu\">\r\n    <h4 class=\"widget-title\">Clothing & Apparel:</h4>\r\n    <div class=\"menu-footer\">\r\n        <ul id=\"menu-footer-link\" class=\"menu\">\r\n            <li><a href=\"#\">Printers</a></li>\r\n            <li><a href=\"#\">Projectors</a></li>\r\n            <li><a href=\"#\">Scanners</a></li>\r\n            <li><a href=\"#\">Store & Business</a></li>\r\n            <li><a href=\"#\">4K Ultra HD TVs</a></li>\r\n            <li><a href=\"#\">LED TVs</a></li>\r\n            <li><a href=\"#\">OLED TVs</a></li>\r\n        </ul>\r\n    </div>\r\n</div>\r\n<div class=\"widget widget_nav_menu\">\r\n    <h4 class=\"widget-title\">Home, Garden & Kitchen:</h4>\r\n    <div class=\"menu-footer\">\r\n        <ul id=\"menu-footer-link\" class=\"menu\">\r\n            <li><a href=\"#\">Cookware</a></li>\r\n            <li><a href=\"#\">Decoration</a></li>\r\n            <li><a href=\"#\">Furniture</a></li>\r\n            <li><a href=\"#\">Garden Tools</a></li>\r\n            <li><a href=\"#\">Powers And Hand Tools</a></li>\r\n            <li><a href=\"#\">Utensil & Gadget</a></li>\r\n        </ul>\r\n    </div>\r\n</div>\r\n<div class=\"widget widget_nav_menu\">\r\n    <h4 class=\"widget-title\">Health & Beauty:</h4>\r\n    <div class=\"menu-footer\">\r\n        <ul id=\"menu-footer-link\" class=\"menu\">\r\n            <li><a href=\"#\">Hair Care</a></li>\r\n            <li><a href=\"#\">Makeup</a></li>\r\n            <li><a href=\"#\">Body Shower</a></li>\r\n            <li><a href=\"#\">Skin Care</a></li>\r\n            <li><a href=\"#\">Cologine</a></li>\r\n            <li><a href=\"#\">Perfume</a></li>\r\n        </ul>\r\n    </div>\r\n</div>\r\n<div class=\"widget widget_nav_menu\">\r\n    <h4 class=\"widget-title\">Jewelry & Watches:</h4>\r\n    <div class=\"menu-footer\">\r\n        <ul id=\"menu-footer-link\" class=\"menu\">\r\n            <li><a href=\"#\">Necklace</a></li>\r\n            <li><a href=\"#\">Pendant</a></li>\r\n            <li><a href=\"#\">Diamond Ring</a></li>\r\n            <li><a href=\"#\">Sliver Earing</a></li>\r\n            <li><a href=\"#\">Leather Watcher</a></li>\r\n            <li><a href=\"#\">Rolex</a></li>\r\n            <li><a href=\"#\">Gucci</a></li>\r\n        </ul>\r\n    </div>\r\n</div>\r\n<div class=\"widget widget_nav_menu\">\r\n    <h4 class=\"widget-title\">Computer & Technologies:</h4>\r\n    <div class=\"menu-footer\">\r\n        <ul id=\"menu-footer-link\" class=\"menu\">\r\n            <li><a href=\"#\">Desktop PC</a></li>\r\n            <li><a href=\"#\">Laptop</a></li>\r\n            <li><a href=\"#\">Smartphones</a></li>\r\n            <li><a href=\"#\">Tablet</a></li>\r\n            <li><a href=\"#\">Game Controller</a></li>\r\n            <li><a href=\"#\">Audio & Video</a></li>\r\n            <li><a href=\"#\">Wireless Speaker</a></li>\r\n            <li><a href=\"#\">Drone</a></li>\r\n        </ul>\r\n    </div>\r\n</div>', NULL, 1, 1, NULL, NULL, NULL);
INSERT INTO `core_settings` VALUES (64, 'news_page_list_title', 'news', 'News', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `core_settings` VALUES (65, 'news_page_list_banner', 'news', '19', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `core_settings` VALUES (66, 'news_sidebar', 'news', '[{\"title\":null,\"content\":null,\"type\":\"search_form\"},{\"title\":\"About Us\",\"content\":\"Nam dapibus nisl vitae elit fringilla rutrum. Aenean sollicitudin, erat a elementum rutrum, neque sem pretium metus, quis mollis nisl nunc et massa\",\"type\":\"content_text\"},{\"title\":\"Recent News\",\"content\":null,\"type\":\"recent_news\"},{\"title\":\"Categories\",\"content\":null,\"type\":\"category\"},{\"title\":\"Tags\",\"content\":null,\"type\":\"tag\"}]', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `core_settings` VALUES (67, 'product_page_search_title', 'product', 'Shop', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `core_settings` VALUES (68, 'list_sliders', 'product', '[{\"image_id\":\"23\",\"title\":\"banner 1\",\"content\":null},{\"image_id\":\"24\",\"title\":\"Banner 2\",\"content\":null}]', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `core_settings` VALUES (69, 'product_policies', 'product', '[{\"title\":\"Shipping Policy\",\"content\":\"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla auctor aliquam tortor at suscipit. Etiam accumsan, est id vehicula cursus, eros ligula suscipit massa, sed auctor felis mi eu massa. Sed vulputate nisi nibh, vel maximus velit auctor nec. Integer consectetur elementum turpis, nec fermentum ipsum tempor quis. Praesent a quam congue, egestas erat sit amet, finibus justo. Quisque viverra neque vehicula eros gravida ultricies. Ut lacinia enim nec consequat tincidunt. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Vivamus ultricies ornare feugiat. Donec vitae rhoncus sapien, ac aliquet nunc.\"},{\"title\":\"Refund Policy\",\"content\":\"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla auctor aliquam tortor at suscipit. Etiam accumsan, est id vehicula cursus, eros ligula suscipit massa, sed auctor felis mi eu massa. Sed vulputate nisi nibh, vel maximus velit auctor nec. Integer consectetur elementum turpis, nec fermentum ipsum tempor quis. Praesent a quam congue, egestas erat sit amet, finibus justo. Quisque viverra neque vehicula eros gravida ultricies. Ut lacinia enim nec consequat tincidunt. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Vivamus ultricies ornare feugiat. Donec vitae rhoncus sapien, ac aliquet nunc.\"},{\"title\":\"Cancellation \\/ Return \\/ Exchange Policy\",\"content\":\"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla auctor aliquam tortor at suscipit. Etiam accumsan, est id vehicula cursus, eros ligula suscipit massa, sed auctor felis mi eu massa. Sed vulputate nisi nibh, vel maximus velit auctor nec. Integer consectetur elementum turpis, nec fermentum ipsum tempor quis. Praesent a quam congue, egestas erat sit amet, finibus justo. Quisque viverra neque vehicula eros gravida ultricies. Ut lacinia enim nec consequat tincidunt. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Vivamus ultricies ornare feugiat. Donec vitae rhoncus sapien, ac aliquet nunc.\"}]', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `core_settings` VALUES (70, 'shipping_information', 'product', '<div class=\"col-product-sidebar-item product-sidebar-shipping\">\r\n    <ul class=\"product-sidebar-shipping-info\">\r\n        <li><i class=\"icon-network\"></i>Shipping worldwide</li>\r\n        <li><i class=\"icon-3d-rotate\"></i>Free 7-day return if eligible, so easy </li>\r\n        <li><i class=\"icon-receipt\"></i>Supplier give bills for this product.</li>\r\n        <li><i class=\"icon-credit-card\"></i>Pay online or when receiving goods</li>\r\n    </ul>\r\n</div>', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `core_settings` VALUES (71, 'ads_image', 'product', '25', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `core_settings` VALUES (72, 'site_desc', 'general', NULL, NULL, 1, 1, NULL, '2021-04-15 08:51:37', '2021-04-15 09:13:24');
INSERT INTO `core_settings` VALUES (73, 'g_offline_payment_logo_id', 'payment', NULL, NULL, 1, 1, NULL, '2021-05-05 05:35:43', '2021-05-05 05:36:09');
INSERT INTO `core_settings` VALUES (74, 'g_offline_payment_html', 'payment', NULL, NULL, 1, 1, NULL, '2021-05-05 05:35:43', '2021-05-05 05:36:09');
INSERT INTO `core_settings` VALUES (75, 'g_paypal_enable', 'payment', NULL, NULL, 1, 1, NULL, '2021-05-05 05:35:43', '2021-05-05 05:36:09');
INSERT INTO `core_settings` VALUES (76, 'g_paypal_name', 'payment', 'Paypal', NULL, 1, 1, NULL, '2021-05-05 05:35:43', '2021-05-05 05:36:09');
INSERT INTO `core_settings` VALUES (77, 'g_paypal_logo_id', 'payment', NULL, NULL, 1, 1, NULL, '2021-05-05 05:35:43', '2021-05-05 05:36:09');
INSERT INTO `core_settings` VALUES (78, 'g_paypal_html', 'payment', NULL, NULL, 1, 1, NULL, '2021-05-05 05:35:43', '2021-05-05 05:36:09');
INSERT INTO `core_settings` VALUES (79, 'g_paypal_test', 'payment', NULL, NULL, 1, 1, NULL, '2021-05-05 05:35:43', '2021-05-05 05:36:09');
INSERT INTO `core_settings` VALUES (80, 'g_paypal_convert_to', 'payment', NULL, NULL, 1, 1, NULL, '2021-05-05 05:35:43', '2021-05-05 05:36:09');
INSERT INTO `core_settings` VALUES (81, 'g_paypal_exchange_rate', 'payment', NULL, NULL, 1, 1, NULL, '2021-05-05 05:35:43', '2021-05-05 05:36:09');
INSERT INTO `core_settings` VALUES (82, 'g_paypal_test_account', 'payment', NULL, NULL, 1, 1, NULL, '2021-05-05 05:35:43', '2021-05-05 05:36:09');
INSERT INTO `core_settings` VALUES (83, 'g_paypal_test_client_id', 'payment', NULL, NULL, 1, 1, NULL, '2021-05-05 05:35:43', '2021-05-05 05:36:09');
INSERT INTO `core_settings` VALUES (84, 'g_paypal_test_client_secret', 'payment', NULL, NULL, 1, 1, NULL, '2021-05-05 05:35:43', '2021-05-05 05:36:09');
INSERT INTO `core_settings` VALUES (85, 'g_paypal_account', 'payment', NULL, NULL, 1, 1, NULL, '2021-05-05 05:35:44', '2021-05-05 05:36:09');
INSERT INTO `core_settings` VALUES (86, 'g_paypal_client_id', 'payment', NULL, NULL, 1, 1, NULL, '2021-05-05 05:35:44', '2021-05-05 05:36:09');
INSERT INTO `core_settings` VALUES (87, 'g_paypal_client_secret', 'payment', NULL, NULL, 1, 1, NULL, '2021-05-05 05:35:44', '2021-05-05 05:36:09');
INSERT INTO `core_settings` VALUES (88, 'g_stripe_enable', 'payment', NULL, NULL, 1, 1, NULL, '2021-05-05 05:35:44', '2021-05-05 05:36:09');
INSERT INTO `core_settings` VALUES (89, 'g_stripe_name', 'payment', 'Stripe', NULL, 1, 1, NULL, '2021-05-05 05:35:44', '2021-05-05 05:36:09');
INSERT INTO `core_settings` VALUES (90, 'g_stripe_logo_id', 'payment', NULL, NULL, 1, 1, NULL, '2021-05-05 05:35:44', '2021-05-05 05:36:09');
INSERT INTO `core_settings` VALUES (91, 'g_stripe_html', 'payment', NULL, NULL, 1, 1, NULL, '2021-05-05 05:35:44', '2021-05-05 05:36:09');
INSERT INTO `core_settings` VALUES (92, 'g_stripe_stripe_secret_key', 'payment', NULL, NULL, 1, 1, NULL, '2021-05-05 05:35:44', '2021-05-05 05:36:09');
INSERT INTO `core_settings` VALUES (93, 'g_stripe_stripe_enable_sandbox', 'payment', NULL, NULL, 1, 1, NULL, '2021-05-05 05:35:44', '2021-05-05 05:36:09');
INSERT INTO `core_settings` VALUES (94, 'g_stripe_stripe_test_secret_key', 'payment', NULL, NULL, 1, 1, NULL, '2021-05-05 05:35:44', '2021-05-05 05:36:09');

-- ----------------------------
-- Table structure for core_subscribers
-- ----------------------------
DROP TABLE IF EXISTS `core_subscribers`;
CREATE TABLE `core_subscribers`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `first_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `last_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `create_user` int(11) NULL DEFAULT NULL,
  `update_user` int(11) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for core_tag_translations
-- ----------------------------
DROP TABLE IF EXISTS `core_tag_translations`;
CREATE TABLE `core_tag_translations`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `origin_id` int(10) UNSIGNED NOT NULL,
  `locale` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `create_user` int(11) NULL DEFAULT NULL,
  `update_user` int(11) NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `core_tag_translations_locale_index`(`locale`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for core_tags
-- ----------------------------
DROP TABLE IF EXISTS `core_tags`;
CREATE TABLE `core_tags`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `slug` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `content` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `create_user` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `update_user` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  `origin_id` bigint(20) NULL DEFAULT NULL,
  `lang` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 15 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of core_tags
-- ----------------------------
INSERT INTO `core_tags` VALUES (1, 'Business', 'business', NULL, NULL, NULL, NULL, NULL, NULL, '2021-04-12 06:15:19', '2021-04-12 06:15:19');
INSERT INTO `core_tags` VALUES (2, 'Clothings', 'clothings', NULL, NULL, NULL, NULL, NULL, NULL, '2021-04-12 06:15:19', '2021-04-12 06:15:19');
INSERT INTO `core_tags` VALUES (3, 'Design', 'design', NULL, NULL, NULL, NULL, NULL, NULL, '2021-04-12 06:15:19', '2021-04-12 06:15:19');
INSERT INTO `core_tags` VALUES (4, 'Entertaiment', 'entertaiment', NULL, NULL, NULL, NULL, NULL, NULL, '2021-04-12 06:15:19', '2021-04-12 06:15:19');
INSERT INTO `core_tags` VALUES (5, 'Fashion', 'fashion', NULL, NULL, NULL, NULL, NULL, NULL, '2021-04-12 06:15:19', '2021-04-12 06:15:19');
INSERT INTO `core_tags` VALUES (6, 'Internet', 'internet', NULL, NULL, NULL, NULL, NULL, NULL, '2021-04-12 06:15:19', '2021-04-12 06:15:19');
INSERT INTO `core_tags` VALUES (7, 'Life Style', 'life-style', NULL, NULL, NULL, NULL, NULL, NULL, '2021-04-12 06:15:19', '2021-04-12 06:15:19');
INSERT INTO `core_tags` VALUES (8, 'Marketing', 'marketing', NULL, NULL, NULL, NULL, NULL, NULL, '2021-04-12 06:15:19', '2021-04-12 06:15:19');
INSERT INTO `core_tags` VALUES (9, 'Music', 'music', NULL, NULL, NULL, NULL, NULL, NULL, '2021-04-12 06:15:19', '2021-04-12 06:15:19');
INSERT INTO `core_tags` VALUES (10, 'New Style', 'new-style', NULL, NULL, NULL, NULL, NULL, NULL, '2021-04-12 06:15:19', '2021-04-12 06:15:19');
INSERT INTO `core_tags` VALUES (11, 'Print', 'print', NULL, NULL, NULL, NULL, NULL, NULL, '2021-04-12 06:15:19', '2021-04-12 06:15:19');
INSERT INTO `core_tags` VALUES (12, 'Spring', 'spring', NULL, NULL, NULL, NULL, NULL, NULL, '2021-04-12 06:15:19', '2021-04-12 06:15:19');
INSERT INTO `core_tags` VALUES (13, 'Summer', 'summer', NULL, NULL, NULL, NULL, NULL, NULL, '2021-04-12 06:15:19', '2021-04-12 06:15:19');
INSERT INTO `core_tags` VALUES (14, 'Technology', 'technology', NULL, NULL, NULL, NULL, NULL, NULL, '2021-04-12 06:15:19', '2021-04-12 06:15:19');

-- ----------------------------
-- Table structure for core_template_translations
-- ----------------------------
DROP TABLE IF EXISTS `core_template_translations`;
CREATE TABLE `core_template_translations`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `origin_id` int(10) UNSIGNED NOT NULL,
  `locale` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `content` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `create_user` int(11) NULL DEFAULT NULL,
  `update_user` int(11) NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `core_template_translations_locale_index`(`locale`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of core_template_translations
-- ----------------------------
INSERT INTO `core_template_translations` VALUES (1, 2, 'en', 'Home Page', '[{\"type\":\"BannerHome1\",\"name\":\"Banner Home 1\",\"model\":{\"sliders\":[{\"_active\":false,\"image\":60},{\"_active\":false,\"image\":61},{\"_active\":false,\"image\":62}],\"saleOff\":[{\"_active\":true,\"title\":\"Unio<br> Leather<br> Bags\",\"number\":\"20%\",\"image\":63,\"link\":\"#\",\"sub_title\":\"100% leather <br> handmade\",\"color\":\"#f1f1f3\"},{\"_active\":true,\"title\":\"iPhone 6+<br> 32Gb\",\"number\":\"40%\",\"image\":64,\"link\":\"#\",\"sub_title\":\"Experience with<br> best smartphone<br> on the world\",\"color\":\"#f7f9f9\"}],\"banner_big_title\":\"\",\"banner_big_desc\":\"\"},\"component\":\"RegularBlock\",\"open\":true,\"is_container\":false},{\"type\":\"ListProduct\",\"name\":\"Product: List Items\",\"model\":{\"title\":\"Deals of the day\",\"desc\":\"\",\"number\":10,\"style\":\"\",\"location_id\":\"\",\"order\":\"title\",\"order_by\":\"asc\",\"is_featured\":false,\"link\":\"xxx\",\"link_product\":\"#\",\"style_list\":\"1\",\"category_id\":[\"1\"]},\"component\":\"RegularBlock\",\"open\":true,\"is_container\":false},{\"type\":\"Promotion\",\"name\":\"Promotion\",\"model\":{\"colItem\":\"4\",\"item\":[{\"_active\":true,\"title\":\"<strong>Acher Fluence</strong> Minimal<br> Speaker\",\"sub_title\":\"\",\"price\":\"298.60\",\"sale_price\":\"157.99\",\"discount\":null,\"image\":65},{\"_active\":true,\"title\":\"WOODEN<br> MINIMALISTIC<br> CHAIRS\",\"sub_title\":\"\",\"price\":null,\"sale_price\":null,\"discount\":40,\"image\":66,\"link\":\"\"},{\"_active\":true,\"title\":\"<strong>iQOS 2.4</strong><br> Holder &amp;<br> Charger\",\"sub_title\":\"\",\"price\":\"105.50\",\"sale_price\":\"\",\"discount\":null,\"image\":67}],\"styleItem\":\"\"},\"component\":\"RegularBlock\",\"open\":true,\"is_container\":false},{\"type\":\"ListProductInCategories\",\"name\":\"Product: List Items In Categories\",\"model\":{\"title\":\"Consumer Electronics\",\"category_id\":[\"29\"],\"number\":10,\"order\":\"\",\"order_by\":\"asc\",\"style_list\":\"\",\"link_all\":\"\",\"sliders\":[],\"custom_link\":[]},\"component\":\"RegularBlock\",\"open\":true,\"is_container\":false},{\"type\":\"ListProductInCategories\",\"name\":\"Product: List Items In Categories\",\"model\":{\"title\":\"Apparels & Clothings\",\"category_id\":[],\"number\":10,\"order\":\"\",\"order_by\":\"\",\"style_list\":\"\",\"link_all\":\"\",\"sliders\":[],\"custom_link\":[]},\"component\":\"RegularBlock\",\"open\":true,\"is_container\":false},{\"type\":\"ListProductInCategories\",\"name\":\"Product: List Items In Categories\",\"model\":{\"title\":\"Home, Garden & Kitchen\",\"category_id\":\"\",\"number\":10,\"order\":\"\",\"order_by\":\"\",\"style_list\":\"\",\"link_all\":\"\",\"sliders\":[],\"custom_link\":[]},\"component\":\"RegularBlock\",\"open\":true,\"is_container\":false}]', 1, 1, '2021-04-19 02:53:16', '2021-04-20 05:49:56');

-- ----------------------------
-- Table structure for core_templates
-- ----------------------------
DROP TABLE IF EXISTS `core_templates`;
CREATE TABLE `core_templates`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `content` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `type_id` int(11) NULL DEFAULT NULL,
  `create_user` int(11) NULL DEFAULT NULL,
  `update_user` int(11) NULL DEFAULT NULL,
  `origin_id` bigint(20) NULL DEFAULT NULL,
  `lang` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of core_templates
-- ----------------------------
INSERT INTO `core_templates` VALUES (1, 'Become a Vendor', '[{\"type\":\"pageBreadcrumbs\",\"name\":\"Page Breadcurmbs\",\"model\":{\"title\":\"\"},\"component\":\"RegularBlock\",\"open\":true,\"is_container\":false},{\"type\":\"callaction\",\"name\":\"Call A Action\",\"model\":{\"title\":\"Millions Of Shoppers Can’t Wait To See <br> What You Have In Store\",\"title_button\":\"Start Selling\",\"link_button\":\"#\",\"bg\":48},\"component\":\"RegularBlock\",\"open\":true,\"is_container\":false},{\"type\":\"WhySellOnMartfury\",\"name\":\"Why Sell On Martfury\",\"model\":{\"title\":\" WHY SELL ON MARTFURY\",\"content\":\"Join a marketplace where nearly 50 million buyers around <br> the world shop for unique items\",\"item\":[{\"_active\":true,\"title\":\"Low Fees\",\"sub_title\":\"It doesn’t take much to list your items <br> and once you make a sale, Martfury’s <br> transaction fee is just 2.5%.\",\"sub_link\":\"#\",\"icon_image\":49,\"sub_text\":null,\"link_title\":\"Learn More\"},{\"_active\":true,\"title\":\"Powerful Tools\",\"sub_title\":\"Our tools and services make it easy <br> to manage, promote and grow your <br> business.\",\"sub_link\":\"#\",\"icon_image\":50,\"link_title\":\"Learn More\"},{\"_active\":true,\"title\":\"Support 24/7\",\"sub_title\":\"Our tools and services make it easy <br> to manage, promote and grow your <br> business.\",\"sub_link\":\"#\",\"icon_image\":51,\"link_title\":\"Learn More\"}]},\"component\":\"RegularBlock\",\"open\":true,\"is_container\":false},{\"type\":\"HowItWorks\",\"name\":\"How It Works\",\"model\":{\"title\":\"HOW IT WORKS\",\"sub_title\":\"Easy to start selling online on Martfury just 4 simple steps\",\"list\":[{\"_active\":false,\"title\":\"Register and list your products\",\"content\":\"<ul>\\n<li>Register your business for free and create a product catalogue. Get free training on how to run your online business</li>\\n<li>Our Martfury Advisors will help you at every step and fully assist you in taking your business online</li>\\n</ul>\",\"image\":52},{\"_active\":true,\"title\":\"Receive orders and sell your product\",\"content\":\"<ul>\\n<li>Register your business for free and create a product catalogue. Get free training on how to run your online business</li>\\n<li>Our Martfury Advisors will help you at every step and fully assist you in taking your business online</li>\\n</ul>\",\"image\":53},{\"_active\":true,\"title\":\"Package and ship with ease\",\"content\":\"<ul>\\n<li>Register your business for free and create a product catalogue. Get free training on how to run your online business</li>\\n<li>Our Martfury Advisors will help you at every step and fully assist you in taking your business online</li>\\n</ul>\",\"image\":54},{\"_active\":true,\"title\":\"Get payments and grow your business\",\"content\":\"<ul>\\n<li>Register your business for free and create a product catalogue. Get free training on how to run your online business</li>\\n<li>Our Martfury Advisors will help you at every step and fully assist you in taking your business online</li>\\n</ul>\",\"image\":55}]},\"component\":\"RegularBlock\",\"open\":true,\"is_container\":false},{\"type\":\"BestFeesToStart\",\"name\":\"Best Fees To Start\",\"model\":{\"title\":\"BEST FEES TO START\",\"sub_title\":\"Affordable, transparent, and secure\",\"sub_content\":\"It doesn’t cost a thing to list up to 50 items a month, and you only pay after your stuff sells. <br> It’s just a small percent of the money you earn\",\"listPercent\":[{\"_active\":false,\"title\":\"Listing Fee\",\"number\":\"$0\"},{\"_active\":false,\"title\":\"Final Value Fee\",\"number\":\"5%\"}],\"title_list\":\"Here\'s what you get for your fee:\",\"title_list_content\":\"<ul>\\n<li>A worldwide community of more than 160 million shoppers.</li>\\n<li>Shipping labels you can print at home, with big discounts on postage.</li>\\n<li>Seller protection and customer support to help you sell your stuff.</li>\\n</ul>\",\"payment_content\":\"We process payments with PayPal, an external payments platform that allows you to process transactions with a variety of payment methods. Funds from PayPal sales on Martfury will be deposited into your PayPal account.\",\"payment_image\":56,\"title_footer\":\"Listing fees are billed for 0.20 USD, so if your bank’s currency is not USD, the amount in <br> your currency may vary based on changes in the exchange rate.\"},\"component\":\"RegularBlock\",\"open\":true,\"is_container\":false},{\"type\":\"SellerStories\",\"name\":\"Seller Stories\",\"model\":{\"title\":\"SELLER STORIES\",\"sub_title\":\"See Seller share about their successful on Martfury\",\"sliders\":[{\"_active\":false,\"avatar\":57,\"name\":\"Kanye West\",\"job\":\"CEO at Google INC\",\"content\":\"Sed elit quam, iaculis sed semper sit amet udin vitae nibh. at magna akal semperFusce commodo molestie luctus.Lorem ipsum Dolor tusima olatiup.\",\"link\":\"#\",\"link_title\":\"PLAY VIDEO\"},{\"_active\":false,\"avatar\":58,\"name\":\"Anabella Kleva\",\"job\":\"Managerment at Envato\",\"content\":\"Sed elit quam, iaculis sed semper sit amet udin vitae nibh. at magna akal semperFusce commodo molestie luctus.Lorem ipsum Dolor tusima olatiup.\",\"link\":\"#\",\"link_title\":\"PLAY VIDEO\"},{\"_active\":false,\"avatar\":59,\"name\":\"Kanye West\",\"job\":\"CEO at Google INC\",\"content\":\"Sed elit quam, iaculis sed semper sit amet udin vitae nibh. at magna akal semperFusce commodo molestie luctus.Lorem ipsum Dolor tusima olatiup.\",\"link\":\"#\",\"link_title\":\"PLAY VIDEO\"}]},\"component\":\"RegularBlock\",\"open\":true,\"is_container\":false},{\"type\":\"FrequentlyAskedQuestions\",\"name\":\"Frequently Asked Questions\",\"model\":{\"title\":\"FREQUENTLY ASKED QUESTIONS\",\"sub_title\":\"Here are some common questions about selling on Martfury\",\"item\":\"\",\"itemListLeft\":[{\"_active\":false,\"title\":\"How do fees work on Martfury?\",\"content\":\"<p style=\\\"box-sizing: border-box; margin-bottom: 1.7em; margin-top: 0px; color: #666666; font-family: \'Work Sans\', Arial, sans-serif; font-size: 16px; background-color: #ffffff;\\\">Joining and starting a shop on Martfury is free. There are three basic selling fees: a listing fee, a transaction fee, and a payment processing fee.</p>\\n<p style=\\\"box-sizing: border-box; margin-bottom: 1.7em; margin-top: 0px; color: #666666; font-family: \'Work Sans\', Arial, sans-serif; font-size: 16px; background-color: #ffffff;\\\">It costs USD 0.20 to publish a listing to the marketplace. A listing lasts for four months or until the item is sold. Once an item sells, there is a 3.5% transaction fee on the sale price (not including shipping costs). If you accept payments with PayPal, there is also a payment processing fee based on their fee structure.</p>\\n<p style=\\\"box-sizing: border-box; margin-bottom: 0px; margin-top: 0px; color: #666666; font-family: \'Work Sans\', Arial, sans-serif; font-size: 16px; background-color: #ffffff;\\\">Listing fees are billed for $0.20 USD, so if your bank&rsquo;s currency is not USD, the amount may differ based on changes in the exchange rate.</p>\"},{\"_active\":false,\"title\":\"What do I need to do to create a shop?\",\"content\":\"<p><span style=\\\"color: #666666; font-family: \'Work Sans\', Arial, sans-serif; font-size: 16px; background-color: #ffffff;\\\">t&rsquo;s easy to set up a shop on Martfury. Create an Martfury account (if you don&rsquo;t already have one), set your shop location and currency, choose a shop name, create a listing, set a payment method (how you want to be paid), and finally set a billing method (how you want to pay your Martfuryfees).</span></p>\"}],\"itemListRight\":[{\"_active\":false,\"title\":\"How do I get paid?\",\"content\":\"<p><span style=\\\"color: #666666; font-family: \'Work Sans\', Arial, sans-serif; font-size: 16px; background-color: #ffffff;\\\">If you accept payments with PayPal, funds from PayPal sales on Martfury will be deposited into your PayPal account. We encourage sellers to use a PayPal Business account and not a Personal account, as personal accounts are subject to monthly receiving limits and cannot accept payments from buyers that are funded by a credit card.</span></p>\"},{\"_active\":false,\"title\":\"Do I need a credit or debit card to create a shop?\",\"content\":\"<p><span style=\\\"color: #666666; font-family: \'Work Sans\', Arial, sans-serif; font-size: 16px; background-color: #ffffff;\\\">No, a credit or debit card is not required to create a shop. To be verified as a seller you have the choice to use either a credit card or to register via PayPal. You will not incur any charges until you open your shop and publish your listings.</span></p>\"},{\"_active\":false,\"title\":\"What can I sell on Martfury?\",\"content\":\"<p><span style=\\\"color: #666666; font-family: \'Work Sans\', Arial, sans-serif; font-size: 16px; background-color: #ffffff;\\\">Martfury provides a marketplace for crafters, artists and collectors to sell their handmade creations, vintage goods (at least 20 years old), and both handmade and non-handmade crafting supplies.</span></p>\"}],\"contact_title\":\"Still have more questions? Feel free to contact us.\",\"contact_link\":\"#\",\"contact_link_button\":\"Contact Us\"},\"component\":\"RegularBlock\",\"open\":true,\"is_container\":false},{\"type\":\"callaction\",\"name\":\"Call A Action\",\"model\":{\"title\":\"It\'s time to start making money.\",\"title_button\":\"Start Selling\",\"link_button\":\"#\",\"bg\":48},\"component\":\"RegularBlock\",\"open\":true,\"is_container\":false}]', NULL, 1, NULL, NULL, NULL, '2021-04-12 06:15:20', NULL);
INSERT INTO `core_templates` VALUES (2, 'Home Page', '[{\"type\":\"BannerHome1\",\"name\":\"Banner Home 1\",\"model\":{\"sliders\":[{\"_active\":false,\"image\":60},{\"_active\":false,\"image\":61},{\"_active\":false,\"image\":62}],\"saleOff\":[{\"_active\":true,\"title\":\"Unio<br> Leather<br> Bags\",\"number\":\"20%\",\"image\":63,\"link\":\"#\",\"sub_title\":\"100% leather <br> handmade\",\"color\":\"#f1f1f3\"},{\"_active\":true,\"title\":\"iPhone 6+<br> 32Gb\",\"number\":\"40%\",\"image\":64,\"link\":\"#\",\"sub_title\":\"Experience with<br> best smartphone<br> on the world\",\"color\":\"#f7f9f9\"}],\"banner_big_title\":\"\",\"banner_big_desc\":\"\"},\"component\":\"RegularBlock\",\"open\":true,\"is_container\":false},{\"type\":\"ListProduct\",\"name\":\"Product: List Items\",\"model\":{\"title\":\"Deals of the day\",\"desc\":\"\",\"number\":10,\"style\":\"\",\"location_id\":\"\",\"order\":\"title\",\"order_by\":\"asc\",\"is_featured\":false,\"link\":\"xxx\",\"link_product\":\"#\",\"style_list\":\"1\",\"category_id\":[\"1\"]},\"component\":\"RegularBlock\",\"open\":true,\"is_container\":false},{\"type\":\"Promotion\",\"name\":\"Promotion\",\"model\":{\"colItem\":\"4\",\"item\":[{\"_active\":true,\"title\":\"<strong>Acher Fluence</strong> Minimal<br> Speaker\",\"sub_title\":\"\",\"price\":\"298.60\",\"sale_price\":\"157.99\",\"discount\":null,\"image\":65},{\"_active\":true,\"title\":\"WOODEN<br> MINIMALISTIC<br> CHAIRS\",\"sub_title\":\"\",\"price\":null,\"sale_price\":null,\"discount\":40,\"image\":66,\"link\":\"\"},{\"_active\":true,\"title\":\"<strong>iQOS 2.4</strong><br> Holder &amp;<br> Charger\",\"sub_title\":\"\",\"price\":\"105.50\",\"sale_price\":\"\",\"discount\":null,\"image\":67}],\"styleItem\":\"\"},\"component\":\"RegularBlock\",\"open\":true,\"is_container\":false},{\"type\":\"ListProductInCategories\",\"name\":\"Product: List Items In Categories\",\"model\":{\"title\":\"Consumer Electronics\",\"category_id\":[\"29\"],\"number\":10,\"order\":\"\",\"order_by\":\"asc\",\"style_list\":\"\",\"link_all\":\"\",\"sliders\":[],\"custom_link\":[]},\"component\":\"RegularBlock\",\"open\":true,\"is_container\":false},{\"type\":\"ListProductInCategories\",\"name\":\"Product: List Items In Categories\",\"model\":{\"title\":\"Apparels & Clothings\",\"category_id\":[],\"number\":10,\"order\":\"\",\"order_by\":\"\",\"style_list\":\"\",\"link_all\":\"\",\"sliders\":[],\"custom_link\":[]},\"component\":\"RegularBlock\",\"open\":true,\"is_container\":false},{\"type\":\"ListProductInCategories\",\"name\":\"Product: List Items In Categories\",\"model\":{\"title\":\"Home, Garden & Kitchen\",\"category_id\":\"\",\"number\":10,\"order\":\"\",\"order_by\":\"\",\"style_list\":\"\",\"link_all\":\"\",\"sliders\":[],\"custom_link\":[]},\"component\":\"RegularBlock\",\"open\":true,\"is_container\":false}]', NULL, 1, 1, NULL, NULL, '2021-04-12 06:15:20', '2021-04-20 05:49:56');
INSERT INTO `core_templates` VALUES (3, 'Home Page 2', '[{\"type\":\"BannerHome2\",\"name\":\"Banner Home 2\",\"model\":{\"sliders\":[{\"_active\":false,\"title\":\"slider 1\",\"link\":\"#\",\"image\":70},{\"_active\":false,\"title\":\"slider 2\",\"link\":\"#\",\"image\":71},{\"_active\":false,\"title\":\"slider 3\",\"link\":\"#\",\"image\":72}],\"banner_list\":[{\"_active\":false,\"title\":\"<strong>LZTET</strong> Baby <br>Rocking Wood Chair\",\"link\":\"#\",\"price_content\":\"Price Just\",\"price\":159.99,\"sale_price\":\"\",\"discount_number\":\"20\",\"discount_text\":\"Discount <span style=\\\"color: #ff3300; font-weight: 600;\\\">30%</span>\",\"image\":73,\"desc_position\":\"top\"},{\"_active\":false,\"title\":\"MILK BLACKMORES<br>TODDLER 300G\",\"link\":\"#\",\"price_content\":null,\"price\":null,\"sale_price\":null,\"discount_text\":\"Discount <span style=\\\"color: #ff3300; font-weight: 600;\\\">30%</span>\",\"desc_position\":\"bottom\",\"image\":74},{\"_active\":false,\"title\":\"BABY<br>STROLLER<br>3IN1\",\"link\":\"#\",\"price_content\":null,\"price\":899,\"sale_price\":\"599\",\"discount_text\":null,\"desc_position\":\"bottom\",\"image\":75},{\"_active\":true,\"title\":\"COMBO 5X<br>NEWBORN DISAPER<br>BAMBO\",\"link\":\"#\",\"price_content\":null,\"price\":null,\"sale_price\":null,\"discount_text\":\"Discount <span style=\\\"color: #ff3300; font-weight: 600;\\\">20%</span>\",\"desc_position\":\"bottom\",\"image\":76},{\"_active\":false,\"title\":\"SNAP SWIM<br>DIAPER\",\"link\":\"#\",\"price_content\":null,\"price\":69,\"sale_price\":\"49.00\",\"discount_text\":null,\"desc_position\":\"bottom\",\"image\":77},{\"_active\":false,\"title\":\"VOUCHER BODY CARE<br>AT BRATUS SPA\",\"link\":\"#\",\"price_content\":null,\"price\":null,\"sale_price\":null,\"discount_text\":\"<span style=\\\"color: #ff3300; font-weight: 600;\\\">25%</span> Off\",\"desc_position\":null,\"image\":78}]},\"component\":\"RegularBlock\",\"open\":true,\"is_container\":false},{\"type\":\"Promotion\",\"name\":\"Promotion\",\"model\":{\"colItem\":\"4\",\"styleItem\":\"1\",\"item\":[{\"_active\":false,\"title\":\"Freeship<br>for orders<br>in local\",\"price\":\"\",\"sale_price\":\"\",\"discount\":\"\",\"link\":\"#\",\"desc\":\"<p class=\\\"hotline\\\" style=\\\"margin: 0; font-size: 20px; color: #669900\\\"><span style=\\\"display: flex; align-items: center;\\\"><i class=\\\"icon-telephone2\\\"></i> <strong style=\\\"font-weight: 600; padding-left: 10px;\\\">1800-6088</strong></span></p>\",\"image\":89},{\"_active\":false,\"title\":\"Diono<br>Convertible<br>Car Seat\",\"price\":null,\"sale_price\":null,\"discount\":\"20\",\"discount_position\":\"top\",\"link\":\"#\",\"desc\":\"3RX All in one<br>from Diono Radian\",\"image\":90},{\"_active\":true,\"title\":\"Versatile<br>Diaper<br>Bag Tote\",\"price\":null,\"sale_price\":null,\"discount\":\"30\",\"discount_position\":\"bottom\",\"link\":\"#\",\"desc\":\"with Multiple<br>Separate Pocket\",\"image\":91}]},\"component\":\"RegularBlock\",\"open\":true,\"is_container\":false},{\"type\":\"BestSellerBrands\",\"name\":\"Best Seller Brands\",\"model\":{\"title\":\"Best Seller Brands\",\"item\":[{\"_active\":false,\"title\":\"Herba\",\"link\":\"#\",\"image\":79},{\"_active\":false,\"title\":\"pencl sketches\",\"link\":\"#\",\"image\":80},{\"_active\":false,\"title\":\"golden feather\",\"link\":\"#\",\"image\":81},{\"_active\":true,\"title\":\"kool jeans\",\"link\":\"#\",\"image\":82},{\"_active\":true,\"title\":\"coffee stories\",\"link\":\"#\",\"image\":83},{\"_active\":true,\"title\":\"book eaters\",\"link\":\"#\",\"image\":84},{\"_active\":true,\"title\":\"suncity\",\"link\":\"#\",\"image\":85},{\"_active\":true,\"title\":\"join julie\",\"link\":\"#\",\"image\":86},{\"_active\":true,\"title\":\"Bison\",\"link\":\"#\",\"image\":87},{\"_active\":true,\"title\":\"Film Critie\",\"link\":\"#\",\"image\":88}]},\"component\":\"RegularBlock\",\"open\":true,\"is_container\":false},{\"type\":\"SearchTrending\",\"name\":\"Search Trending\",\"model\":{\"title\":\"Search Trending\",\"update_at\":\"Updated at 9:00AM\",\"tab_trending\":[{\"_active\":true,\"title\":\"Hot Trending\",\"icon\":\"icon-star\",\"category_id\":[\"1\"],\"order\":\"title\",\"order_by\":\"desc\"},{\"_active\":true,\"title\":\"Baby Hygiene\",\"icon\":\"icon-baby3\",\"category_id\":[\"1\"],\"order\":\"title\",\"order_by\":\"asc\"},{\"_active\":true,\"title\":\"Baby Sleeping\",\"icon\":\"icon-lampshade\"},{\"_active\":true,\"title\":\"Baby Out\",\"icon\":\"icon-umbrella\"},{\"_active\":true,\"title\":\"Apparels\",\"icon\":\"icon-shirt\"},{\"_active\":true,\"title\":\"Eat & Drink\",\"icon\":\"icon-baby-bottle\"},{\"_active\":true,\"title\":\"Toys\",\"icon\":\"icon-baseball\"},{\"_active\":true,\"title\":\"Home Wares\",\"icon\":\"icon-book2\"},{\"_active\":true,\"title\":\"Cars\",\"icon\":\"icon-car-siren\"}]},\"component\":\"RegularBlock\",\"open\":true,\"is_container\":false},{\"type\":\"ListProductInCategories\",\"name\":\"Product: List Items In Categories\",\"model\":{\"style_list\":\"1\",\"title\":\"Eat & Drink for Baby\",\"category_id\":[\"1\",\"16\"],\"number\":6,\"order\":\"title\",\"order_by\":\"desc\",\"sliders\":[{\"_active\":false,\"title\":\"Slider-1\",\"link\":\"#\",\"image\":92},{\"_active\":false,\"title\":\"Slider-2\",\"link\":\"#\",\"image\":93},{\"_active\":false,\"title\":\"Slider-3\",\"link\":\"#\",\"image\":94}],\"custom_link\":[{\"_active\":false,\"title\":\"Best Seller\",\"link\":\"#\"},{\"_active\":false,\"title\":\"New Arrivals\",\"link\":\"#\"},{\"_active\":false,\"title\":\"On Sales\",\"link\":\"#\"},{\"_active\":false,\"title\":\"Milks\",\"link\":\"#\"},{\"_active\":false,\"title\":\"Milk Bottles\",\"link\":\"#\"},{\"_active\":false,\"title\":\"Weaning Foods\",\"link\":\"#\"},{\"_active\":false,\"title\":\"Nipples\",\"link\":\"#\"},{\"_active\":false,\"title\":\"Others\",\"link\":\"#\"},{\"_active\":false,\"title\":\"Sales & Deal\",\"link\":\"#\"}],\"link_all\":\"http://becommere.dv/product?brand=1\"},\"component\":\"RegularBlock\",\"open\":true,\"is_container\":false},{\"type\":\"ListProductInCategories\",\"name\":\"Product: List Items In Categories\",\"model\":{\"style_list\":\"1\",\"title\":\"Apparels for Baby\",\"category_id\":\"\",\"number\":6,\"order\":\"title\",\"order_by\":\"desc\",\"sliders\":[{\"_active\":false,\"title\":\"slider-1\",\"link\":\"#\",\"image\":95},{\"_active\":false,\"title\":\"slider-2\",\"link\":\"#\",\"image\":96},{\"_active\":false,\"title\":\"slider-3\",\"link\":\"#\",\"image\":97}],\"custom_link\":[{\"_active\":true,\"title\":\"Best Seller\",\"link\":\"#\"},{\"_active\":true,\"title\":\"New Arrivals\",\"link\":\"#\"},{\"_active\":true,\"title\":\"For Newborns\",\"link\":\"#\"},{\"_active\":true,\"title\":\"Boy Fashion\",\"link\":\"#\"},{\"_active\":true,\"title\":\"Girl Fashion\",\"link\":\"#\"},{\"_active\":true,\"title\":\"Accessories\",\"link\":\"#\"},{\"_active\":true,\"title\":\"Sales & Deals\",\"link\":\"#\"}],\"link_all\":\"\"},\"component\":\"RegularBlock\",\"open\":true,\"is_container\":false},{\"type\":\"ListProductInCategories\",\"name\":\"Product: List Items In Categories\",\"model\":{\"style_list\":\"1\",\"title\":\"Baby Out\",\"category_id\":\"\",\"number\":6,\"order\":\"title\",\"order_by\":\"desc\",\"link_all\":\"/product?brand=2\",\"sliders\":[{\"_active\":true,\"title\":\"slider-1\",\"link\":\"#\",\"image\":98},{\"_active\":true,\"title\":\"slider-2\",\"link\":\"#\",\"image\":99},{\"_active\":true,\"title\":\"slider-3\",\"link\":\"#\",\"image\":100}],\"custom_link\":[{\"_active\":true,\"title\":\"Best Seller\",\"link\":\"#\"},{\"_active\":true,\"title\":\"New Arrivals\",\"link\":\"#\"},{\"_active\":true,\"title\":\"Strollers\",\"link\":\"#\"},{\"_active\":true,\"title\":\"Kids Scooters\",\"link\":\"#\"},{\"_active\":true,\"title\":\"Travel Gears\",\"link\":\"#\"},{\"_active\":true,\"title\":\"Car Seat\",\"link\":\"#\"},{\"_active\":true,\"title\":\"Hipseat Carrier\",\"link\":\"#\"},{\"_active\":true,\"title\":\"Car Electronic\",\"link\":\"#\"},{\"_active\":true,\"title\":\"Sales & Deal\",\"link\":\"#\"}]},\"component\":\"RegularBlock\",\"open\":true,\"is_container\":false},{\"type\":\"ListProductInCategories\",\"name\":\"Product: List Items In Categories\",\"model\":{\"style_list\":\"1\",\"title\":\"Pregnancy & Maternity\",\"category_id\":\"\",\"number\":\"\",\"order\":\"title\",\"order_by\":\"asc\",\"link_all\":\"\",\"sliders\":[{\"_active\":true,\"title\":\"slider-1\",\"link\":\"#\",\"image\":101},{\"_active\":true,\"title\":\"slider-2\",\"link\":\"#\",\"image\":102},{\"_active\":true,\"title\":\"slider-3\",\"link\":\"#\",\"image\":103}],\"custom_link\":[{\"_active\":true,\"title\":\"Best Seller\",\"link\":\"#\"},{\"_active\":true,\"title\":\"New Arrivals\",\"link\":\"#\"},{\"_active\":true,\"title\":\"Milk pump\",\"link\":\"#\"},{\"_active\":true,\"title\":\"Shampoo & Shower Gel\",\"link\":\"#\"},{\"_active\":true,\"title\":\"For Pregnant\",\"link\":\"#\"},{\"_active\":true,\"title\":\"For Mom After Borned\",\"link\":\"#\"},{\"_active\":true,\"title\":\"Maternity Pillows\",\"link\":\"#\"},{\"_active\":true,\"title\":\"Others\",\"link\":\"#\"},{\"_active\":true,\"title\":\"Sales & Deal\",\"link\":\"#\"}]},\"component\":\"RegularBlock\",\"open\":true},{\"type\":\"RecentlyViewedProducts\",\"name\":\"Product: Recently Viewed Products\",\"model\":{\"style_list\":\"\",\"title\":\"Recently Viewed Products\",\"category_id\":\"\",\"number\":\"\",\"order\":\"\",\"order_by\":\"\",\"sliders\":[]},\"component\":\"RegularBlock\",\"open\":true,\"is_container\":false}]', NULL, 1, NULL, NULL, NULL, '2021-04-12 06:15:20', NULL);

-- ----------------------------
-- Table structure for core_translations
-- ----------------------------
DROP TABLE IF EXISTS `core_translations`;
CREATE TABLE `core_translations`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `locale` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `string` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `parent_id` bigint(20) NULL DEFAULT NULL,
  `create_user` int(11) NULL DEFAULT NULL,
  `update_user` int(11) NULL DEFAULT NULL,
  `last_build_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for media_files
-- ----------------------------
DROP TABLE IF EXISTS `media_files`;
CREATE TABLE `media_files`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `file_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `file_path` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `file_size` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `file_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `file_extension` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `create_user` int(11) NULL DEFAULT NULL,
  `update_user` int(11) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  `app_id` int(11) NULL DEFAULT NULL,
  `app_user_id` int(11) NULL DEFAULT NULL,
  `file_width` int(11) NULL DEFAULT NULL,
  `file_height` int(11) NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 109 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of media_files
-- ----------------------------
INSERT INTO `media_files` VALUES (1, 'avatar', 'demo/general/avatar.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `media_files` VALUES (2, 'avatar-2', 'demo/general/avatar-2.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `media_files` VALUES (3, 'avatar-3', 'demo/general/avatar-3.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `media_files` VALUES (4, 'ico_adventurous', 'demo/general/ico_adventurous.png', NULL, 'image/png', 'png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `media_files` VALUES (5, 'ico_localguide', 'demo/general/ico_localguide.png', NULL, 'image/png', 'png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `media_files` VALUES (6, 'ico_maps', 'demo/general/ico_maps.png', NULL, 'image/png', 'png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `media_files` VALUES (7, 'ico_paymethod', 'demo/general/ico_paymethod.png', NULL, 'image/png', 'png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `media_files` VALUES (8, 'logo', 'demo/general/logo.png', NULL, 'image/png', 'png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `media_files` VALUES (9, 'bg_contact', 'demo/general/bg-contact.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `media_files` VALUES (10, 'favicon', 'demo/general/favicon.png', NULL, 'image/png', 'png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `media_files` VALUES (11, 'news-1', 'demo/news/news-1.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `media_files` VALUES (12, 'news-2', 'demo/news/news-2.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `media_files` VALUES (13, 'news-3', 'demo/news/news-3.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `media_files` VALUES (14, 'news-4', 'demo/news/news-4.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `media_files` VALUES (15, 'news-5', 'demo/news/news-5.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `media_files` VALUES (16, 'news-6', 'demo/news/news-6.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `media_files` VALUES (17, 'news-7', 'demo/news/news-7.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `media_files` VALUES (18, 'news-8', 'demo/news/news-8.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `media_files` VALUES (19, 'news-banner', 'demo/news/news-banner.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `media_files` VALUES (20, 'homepage-2-background', 'demo/templates/homepage-2-background.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `media_files` VALUES (21, 'menu-1', 'demo/templates/menu-1.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `media_files` VALUES (22, 'menu-2', 'demo/templates/menu-2.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `media_files` VALUES (23, 'product-banner-1', 'demo/templates/product-banner-1.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `media_files` VALUES (24, 'product-banner-2', 'demo/templates/product-banner-2.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `media_files` VALUES (25, 'ads-image', 'demo/templates/ads-image.png', NULL, 'image/png', 'png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `media_files` VALUES (26, 'gallery-image-1', 'demo/templates/gallery-image-1.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `media_files` VALUES (27, 'gallery-image-2', 'demo/templates/gallery-image-2.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `media_files` VALUES (28, 'gallery-image-3', 'demo/templates/gallery-image-3.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `media_files` VALUES (29, 'gallery-image-4', 'demo/templates/gallery-image-4.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `media_files` VALUES (30, 'product-image-1', 'demo/templates/product-image-1.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `media_files` VALUES (31, 'product-image-2', 'demo/templates/product-image-2.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `media_files` VALUES (32, 'product-image-3', 'demo/templates/product-image-3.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `media_files` VALUES (33, 'product-image-4', 'demo/templates/product-image-4.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `media_files` VALUES (34, 'product-image-5', 'demo/templates/product-image-5.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `media_files` VALUES (35, 'product-image-6', 'demo/templates/product-image-6.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `media_files` VALUES (36, 'product-image-7', 'demo/templates/product-image-7.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `media_files` VALUES (37, 'product-image-8', 'demo/templates/product-image-8.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `media_files` VALUES (38, 'product-image-9', 'demo/templates/product-image-9.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `media_files` VALUES (39, 'product-image-10', 'demo/templates/product-image-10.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `media_files` VALUES (40, 'cat-image-1', 'demo/templates/cat-image-1.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `media_files` VALUES (41, 'cat-image-2', 'demo/templates/cat-image-2.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `media_files` VALUES (42, 'cat-image-3', 'demo/templates/cat-image-3.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `media_files` VALUES (43, 'cat-image-4', 'demo/templates/cat-image-4.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `media_files` VALUES (44, 'cat-image-5', 'demo/templates/cat-image-5.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `media_files` VALUES (45, 'cat-image-6', 'demo/templates/cat-image-6.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `media_files` VALUES (46, 'cat-image-7', 'demo/templates/cat-image-7.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `media_files` VALUES (47, 'cat-image-8', 'demo/templates/cat-image-8.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `media_files` VALUES (48, 'templates-1', 'demo/templates/templates-1.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `media_files` VALUES (49, 'templates-2', 'demo/templates/templates-2.png', NULL, 'image/png', 'png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `media_files` VALUES (50, 'templates-3', 'demo/templates/templates-3.png', NULL, 'image/png', 'png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `media_files` VALUES (51, 'templates-4', 'demo/templates/templates-4.png', NULL, 'image/png', 'png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `media_files` VALUES (52, 'templates-5', 'demo/templates/templates-5.png', NULL, 'image/png', 'png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `media_files` VALUES (53, 'templates-6', 'demo/templates/templates-6.png', NULL, 'image/png', 'png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `media_files` VALUES (54, 'templates-7', 'demo/templates/templates-7.png', NULL, 'image/png', 'png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `media_files` VALUES (55, 'templates-8', 'demo/templates/templates-8.png', NULL, 'image/png', 'png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `media_files` VALUES (56, 'templates-9', 'demo/templates/templates-9.png', NULL, 'image/png', 'png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `media_files` VALUES (57, 'templates-10', 'demo/templates/templates-10.png', NULL, 'image/png', 'png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `media_files` VALUES (58, 'templates-11', 'demo/templates/templates-11.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `media_files` VALUES (59, 'templates-12', 'demo/templates/templates-12.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `media_files` VALUES (60, 'home-1', 'demo/templates/home-1.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `media_files` VALUES (61, 'home-2', 'demo/templates/home-2.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `media_files` VALUES (62, 'home-3', 'demo/templates/home-3.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `media_files` VALUES (63, 'home-4', 'demo/templates/home-4.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `media_files` VALUES (64, 'home-5', 'demo/templates/home-5.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `media_files` VALUES (65, 'home-6', 'demo/templates/home-6.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `media_files` VALUES (66, 'home-7', 'demo/templates/home-7.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `media_files` VALUES (67, 'home-8', 'demo/templates/home-8.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `media_files` VALUES (68, 'home-9', 'demo/templates/home-9.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `media_files` VALUES (69, 'home-10', 'demo/templates/home-10.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `media_files` VALUES (70, 'homepage2-banner-1', 'demo/templates/homepage2-banner-1.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `media_files` VALUES (71, 'homepage2-banner-2', 'demo/templates/homepage2-banner-2.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `media_files` VALUES (72, 'homepage2-banner-3', 'demo/templates/homepage2-banner-3.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `media_files` VALUES (73, 'homepage2-banner-4', 'demo/templates/homepage2-banner-4.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `media_files` VALUES (74, 'homepage2-banner-5', 'demo/templates/homepage2-banner-5.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `media_files` VALUES (75, 'homepage2-banner-6', 'demo/templates/homepage2-banner-6.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `media_files` VALUES (76, 'homepage2-banner-7', 'demo/templates/homepage2-banner-7.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `media_files` VALUES (77, 'homepage2-banner-8', 'demo/templates/homepage2-banner-8.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `media_files` VALUES (78, 'homepage2-banner-9', 'demo/templates/homepage2-banner-9.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `media_files` VALUES (79, 'homepage2-brand-1', 'demo/templates/homepage2-brand-1.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `media_files` VALUES (80, 'homepage2-brand-2', 'demo/templates/homepage2-brand-2.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `media_files` VALUES (81, 'homepage2-brand-3', 'demo/templates/homepage2-brand-3.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `media_files` VALUES (82, 'homepage2-brand-4', 'demo/templates/homepage2-brand-4.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `media_files` VALUES (83, 'homepage2-brand-5', 'demo/templates/homepage2-brand-5.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `media_files` VALUES (84, 'homepage2-brand-6', 'demo/templates/homepage2-brand-6.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `media_files` VALUES (85, 'homepage2-brand-7', 'demo/templates/homepage2-brand-7.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `media_files` VALUES (86, 'homepage2-brand-8', 'demo/templates/homepage2-brand-8.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `media_files` VALUES (87, 'homepage2-brand-9', 'demo/templates/homepage2-brand-9.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `media_files` VALUES (88, 'homepage2-brand-10', 'demo/templates/homepage2-brand-10.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `media_files` VALUES (89, 'homepage2-promotion-1', 'demo/templates/homepage2-promotion-1.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `media_files` VALUES (90, 'homepage2-promotion-2', 'demo/templates/homepage2-promotion-2.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `media_files` VALUES (91, 'homepage2-promotion-3', 'demo/templates/homepage2-promotion-3.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `media_files` VALUES (92, 'homepage2-slider-1', 'demo/templates/homepage2-slider-1.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `media_files` VALUES (93, 'homepage2-slider-2', 'demo/templates/homepage2-slider-2.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `media_files` VALUES (94, 'homepage2-slider-3', 'demo/templates/homepage2-slider-3.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `media_files` VALUES (95, 'homepage2-slider-4', 'demo/templates/homepage2-slider-4.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `media_files` VALUES (96, 'homepage2-slider-5', 'demo/templates/homepage2-slider-5.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `media_files` VALUES (97, 'homepage2-slider-6', 'demo/templates/homepage2-slider-6.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `media_files` VALUES (98, 'homepage2-slider-7', 'demo/templates/homepage2-slider-7.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `media_files` VALUES (99, 'homepage2-slider-8', 'demo/templates/homepage2-slider-8.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `media_files` VALUES (100, 'homepage2-slider-9', 'demo/templates/homepage2-slider-9.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `media_files` VALUES (101, 'homepage2-slider-10', 'demo/templates/homepage2-slider-10.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `media_files` VALUES (102, 'homepage2-slider-11', 'demo/templates/homepage2-slider-11.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `media_files` VALUES (103, 'homepage2-slider-12', 'demo/templates/homepage2-slider-12.jpg', NULL, 'image/jpeg', 'jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `media_files` VALUES (104, 'logo-1', '0000/1/2021/04/19/logo-1.png', '11475', 'image/png', 'png', 1, NULL, NULL, NULL, NULL, 320, 100, '2021-04-19 03:07:07', '2021-04-19 03:07:07');
INSERT INTO `media_files` VALUES (105, 'tbv-logo-05', '0000/1/2021/04/19/tbv-logo-05.png', '30401', 'image/png', 'png', 1, NULL, NULL, NULL, NULL, 1965, 1112, '2021-04-19 03:09:50', '2021-04-19 03:09:50');
INSERT INTO `media_files` VALUES (106, 'tbv-logo-01', '0000/1/2021/04/19/tbv-logo-01.jpg', '109790', 'image/jpeg', 'jpg', 1, NULL, NULL, NULL, NULL, 1965, 1111, '2021-04-19 03:10:14', '2021-04-19 03:10:14');
INSERT INTO `media_files` VALUES (107, 'lo-quay', '0000/1/2021/04/20/lo-quay.png', '47660', 'image/png', 'png', 1, NULL, NULL, NULL, NULL, 248, 253, '2021-04-20 05:47:34', '2021-04-20 05:47:34');
INSERT INTO `media_files` VALUES (108, 'bep-a-doi-tu', '0000/1/2021/04/20/bep-a-doi-tu.png', '39701', 'image/png', 'png', 1, NULL, NULL, NULL, NULL, 240, 256, '2021-04-20 05:48:56', '2021-04-20 05:48:56');

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 22 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO `migrations` VALUES (1, '2014_10_12_000000_create_users_table', 1);
INSERT INTO `migrations` VALUES (2, '2014_10_12_100000_create_password_resets_table', 1);
INSERT INTO `migrations` VALUES (3, '2019_03_13_174533_create_permission_tables', 1);
INSERT INTO `migrations` VALUES (4, '2019_03_17_114820_create_table_core_pages', 1);
INSERT INTO `migrations` VALUES (5, '2019_03_17_140539_create_media_files_table', 1);
INSERT INTO `migrations` VALUES (6, '2019_03_27_081940_create_core_setting_table', 1);
INSERT INTO `migrations` VALUES (7, '2019_04_01_150630_create_menu_table', 1);
INSERT INTO `migrations` VALUES (8, '2019_04_02_150630_create_core_news_table', 1);
INSERT INTO `migrations` VALUES (9, '2019_04_05_143248_create_core_templates_table', 1);
INSERT INTO `migrations` VALUES (10, '2019_05_07_085430_create_core_languages_table', 1);
INSERT INTO `migrations` VALUES (11, '2019_05_17_074008_create_bravo_review', 1);
INSERT INTO `migrations` VALUES (12, '2019_05_21_084235_create_bravo_contact_table', 1);
INSERT INTO `migrations` VALUES (13, '2019_05_28_152845_create_core_subscribers_table', 1);
INSERT INTO `migrations` VALUES (14, '2019_06_17_142338_bravo_seo', 1);
INSERT INTO `migrations` VALUES (15, '2019_08_09_163909_create_inbox_table', 1);
INSERT INTO `migrations` VALUES (16, '2019_08_20_162106_create_table_user_upgrade_requests', 1);
INSERT INTO `migrations` VALUES (17, '2019_08_22_071351_create_product_table', 1);
INSERT INTO `migrations` VALUES (18, '2019_08_22_071613_create_order_table', 1);
INSERT INTO `migrations` VALUES (19, '2020_05_19_075032_create_shoppingcart_table', 1);
INSERT INTO `migrations` VALUES (20, '2020_06_03_093741_create_bravo_coupon', 1);
INSERT INTO `migrations` VALUES (21, '2021_04_25_154949_update_table_production', 2);

-- ----------------------------
-- Table structure for password_resets
-- ----------------------------
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets`  (
  `email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  INDEX `password_resets_email_index`(`email`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for product_brand
-- ----------------------------
DROP TABLE IF EXISTS `product_brand`;
CREATE TABLE `product_brand`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `slug` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `status` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `image_id` int(11) NULL DEFAULT NULL,
  `icon` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `create_user` int(11) NULL DEFAULT NULL,
  `update_user` int(11) NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 12 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of product_brand
-- ----------------------------
INSERT INTO `product_brand` VALUES (1, 'Galaxy', NULL, 'galaxy', 'publish', NULL, NULL, 1, NULL, NULL, NULL);
INSERT INTO `product_brand` VALUES (2, 'Casio', NULL, 'casio', 'publish', NULL, NULL, 1, NULL, NULL, NULL);
INSERT INTO `product_brand` VALUES (3, 'Electrolux', NULL, 'electrolux', 'publish', NULL, NULL, 1, NULL, NULL, NULL);
INSERT INTO `product_brand` VALUES (4, 'Amcrest', NULL, 'amcrest', 'publish', NULL, NULL, 1, NULL, NULL, NULL);
INSERT INTO `product_brand` VALUES (5, 'Adidas', NULL, 'adidas', 'publish', NULL, NULL, 1, NULL, NULL, NULL);
INSERT INTO `product_brand` VALUES (6, 'Sony', NULL, 'sony', 'publish', NULL, NULL, 1, NULL, NULL, NULL);
INSERT INTO `product_brand` VALUES (7, 'XBox', NULL, 'xbox', 'publish', NULL, NULL, 1, NULL, NULL, NULL);
INSERT INTO `product_brand` VALUES (8, 'Samsung', NULL, 'samsung', 'publish', NULL, NULL, 1, NULL, NULL, NULL);
INSERT INTO `product_brand` VALUES (9, 'Syma', NULL, 'syma', 'publish', NULL, NULL, 1, NULL, NULL, NULL);
INSERT INTO `product_brand` VALUES (10, 'Apple', NULL, 'apple', 'publish', NULL, NULL, 1, NULL, NULL, NULL);
INSERT INTO `product_brand` VALUES (11, 'Asus', NULL, 'asus', 'publish', NULL, NULL, 1, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for product_brand_translations
-- ----------------------------
DROP TABLE IF EXISTS `product_brand_translations`;
CREATE TABLE `product_brand_translations`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `origin_id` bigint(20) NULL DEFAULT NULL,
  `locale` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `create_user` int(11) NULL DEFAULT NULL,
  `update_user` int(11) NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `product_brand_translations_origin_id_locale_unique`(`origin_id`, `locale`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for product_carts
-- ----------------------------
DROP TABLE IF EXISTS `product_carts`;
CREATE TABLE `product_carts`  (
  `identifier` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `instance` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`identifier`, `instance`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for product_category
-- ----------------------------
DROP TABLE IF EXISTS `product_category`;
CREATE TABLE `product_category`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `slug` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `status` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `image_id` int(11) NULL DEFAULT NULL,
  `icon` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `_lft` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `_rgt` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `parent_id` int(10) UNSIGNED NULL DEFAULT NULL,
  `create_user` int(11) NULL DEFAULT NULL,
  `update_user` int(11) NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `product_category__lft__rgt_parent_id_index`(`_lft`, `_rgt`, `parent_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 49 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of product_category
-- ----------------------------
INSERT INTO `product_category` VALUES (1, 'Clothing & Apparel', '', 'clothing-apparel', 'publish', 40, NULL, 1, 16, NULL, NULL, NULL, '2021-04-12 06:15:19', '2021-04-12 06:15:19');
INSERT INTO `product_category` VALUES (2, 'Womens', '', 'womens', 'publish', NULL, NULL, 2, 3, 1, NULL, NULL, '2021-04-12 06:15:19', '2021-04-12 06:15:19');
INSERT INTO `product_category` VALUES (3, 'Mens', '', 'mens', 'publish', NULL, NULL, 4, 5, 1, NULL, NULL, '2021-04-12 06:15:19', '2021-04-12 06:15:19');
INSERT INTO `product_category` VALUES (4, 'Shoes', '', 'shoes', 'publish', NULL, NULL, 6, 7, 1, NULL, NULL, '2021-04-12 06:15:19', '2021-04-12 06:15:19');
INSERT INTO `product_category` VALUES (5, 'Bags', '', 'bags', 'publish', NULL, NULL, 8, 9, 1, NULL, NULL, '2021-04-12 06:15:19', '2021-04-12 06:15:19');
INSERT INTO `product_category` VALUES (6, 'Sunglasses', '', 'sunglasses', 'publish', NULL, NULL, 10, 11, 1, NULL, NULL, '2021-04-12 06:15:19', '2021-04-12 06:15:19');
INSERT INTO `product_category` VALUES (7, 'Accessories', '', 'accessories', 'publish', NULL, NULL, 12, 13, 1, NULL, NULL, '2021-04-12 06:15:19', '2021-04-12 06:15:19');
INSERT INTO `product_category` VALUES (8, 'Kid’s Fashion', '', 'kids-fashion', 'publish', NULL, NULL, 14, 15, 1, NULL, NULL, '2021-04-12 06:15:19', '2021-04-12 06:15:19');
INSERT INTO `product_category` VALUES (9, 'Garden & Kitchen', '', 'garden-kitchen', 'publish', 41, NULL, 17, 30, NULL, NULL, NULL, '2021-04-12 06:15:19', '2021-04-12 06:15:19');
INSERT INTO `product_category` VALUES (10, 'Decoration', '', 'decoration', 'publish', NULL, NULL, 18, 19, 9, NULL, NULL, '2021-04-12 06:15:19', '2021-04-12 06:15:19');
INSERT INTO `product_category` VALUES (11, 'Furniture', '', 'furniture', 'publish', NULL, NULL, 20, 21, 9, NULL, NULL, '2021-04-12 06:15:19', '2021-04-12 06:15:19');
INSERT INTO `product_category` VALUES (12, 'Garden Tools', '', 'garden-tools', 'publish', NULL, NULL, 22, 23, 9, NULL, NULL, '2021-04-12 06:15:19', '2021-04-12 06:15:19');
INSERT INTO `product_category` VALUES (13, 'Home Improvement', '', 'home-improvement', 'publish', NULL, NULL, 24, 25, 9, NULL, NULL, '2021-04-12 06:15:19', '2021-04-12 06:15:19');
INSERT INTO `product_category` VALUES (14, 'Powers And Hand Tools', '', 'powers-and-hand-tools', 'publish', NULL, NULL, 26, 27, 9, NULL, NULL, '2021-04-12 06:15:19', '2021-04-12 06:15:19');
INSERT INTO `product_category` VALUES (15, 'Utensil & Gadget', '', 'utensil-gadget', 'publish', NULL, NULL, 28, 29, 9, NULL, NULL, '2021-04-12 06:15:19', '2021-04-12 06:15:19');
INSERT INTO `product_category` VALUES (16, 'Consumer Electrics', '', 'consumer-electrics', 'publish', 42, NULL, 31, 46, NULL, NULL, NULL, '2021-04-12 06:15:19', '2021-04-12 06:15:19');
INSERT INTO `product_category` VALUES (17, 'Air Conditioners', '', 'air-conditioners', 'publish', NULL, NULL, 32, 33, 16, NULL, NULL, '2021-04-12 06:15:19', '2021-04-12 06:15:19');
INSERT INTO `product_category` VALUES (18, 'Audios & Theaters', '', 'audios-theaters', 'publish', NULL, NULL, 34, 35, 16, NULL, NULL, '2021-04-12 06:15:19', '2021-04-12 06:15:19');
INSERT INTO `product_category` VALUES (19, 'Car Electronics', '', 'car-electronics', 'publish', NULL, NULL, 36, 37, 16, NULL, NULL, '2021-04-12 06:15:19', '2021-04-12 06:15:19');
INSERT INTO `product_category` VALUES (20, 'Office Electronics', '', 'office-electronics', 'publish', NULL, NULL, 38, 39, 16, NULL, NULL, '2021-04-12 06:15:19', '2021-04-12 06:15:19');
INSERT INTO `product_category` VALUES (21, 'TV Televisions', '', 'tv-televisions', 'publish', NULL, NULL, 40, 41, 16, NULL, NULL, '2021-04-12 06:15:19', '2021-04-12 06:15:19');
INSERT INTO `product_category` VALUES (22, 'Washing Machines', '', 'washing-machines', 'publish', NULL, NULL, 42, 43, 16, NULL, NULL, '2021-04-12 06:15:19', '2021-04-12 06:15:19');
INSERT INTO `product_category` VALUES (23, 'Refrigerators', '', 'refrigerators', 'publish', NULL, NULL, 44, 45, 16, NULL, NULL, '2021-04-12 06:15:19', '2021-04-12 06:15:19');
INSERT INTO `product_category` VALUES (24, 'Health & Beauty', '', 'health-beauty', 'publish', 43, NULL, 47, 56, NULL, NULL, NULL, '2021-04-12 06:15:19', '2021-04-12 06:15:19');
INSERT INTO `product_category` VALUES (25, 'Equipments', '', 'equipments', 'publish', NULL, NULL, 48, 49, 24, NULL, NULL, '2021-04-12 06:15:19', '2021-04-12 06:15:19');
INSERT INTO `product_category` VALUES (26, 'Hair Care', '', 'hair-care', 'publish', NULL, NULL, 50, 51, 24, NULL, NULL, '2021-04-12 06:15:19', '2021-04-12 06:15:19');
INSERT INTO `product_category` VALUES (27, 'Perfumer', '', 'perfumer', 'publish', NULL, NULL, 52, 53, 24, NULL, NULL, '2021-04-12 06:15:19', '2021-04-12 06:15:19');
INSERT INTO `product_category` VALUES (28, 'Skin Care', '', 'skin-care', 'publish', NULL, NULL, 54, 55, 24, NULL, NULL, '2021-04-12 06:15:19', '2021-04-12 06:15:19');
INSERT INTO `product_category` VALUES (29, 'Computers & Technologies', '', 'computers-technologies', 'publish', 44, NULL, 57, 64, NULL, NULL, NULL, '2021-04-12 06:15:19', '2021-04-12 06:15:19');
INSERT INTO `product_category` VALUES (30, 'Desktop PC', '', 'desktop-pc', 'publish', NULL, NULL, 58, 59, 29, NULL, NULL, '2021-04-12 06:15:19', '2021-04-12 06:15:19');
INSERT INTO `product_category` VALUES (31, 'Laptop', '', 'laptop', 'publish', NULL, NULL, 60, 61, 29, NULL, NULL, '2021-04-12 06:15:19', '2021-04-12 06:15:19');
INSERT INTO `product_category` VALUES (32, 'Smartphones', '', 'smartphones', 'publish', NULL, NULL, 62, 63, 29, NULL, NULL, '2021-04-12 06:15:19', '2021-04-12 06:15:19');
INSERT INTO `product_category` VALUES (33, 'Jewelry & Watches', '', 'jewelry-watches', 'publish', 45, NULL, 65, 72, NULL, NULL, NULL, '2021-04-12 06:15:19', '2021-04-12 06:15:19');
INSERT INTO `product_category` VALUES (34, 'Gemstone Jewelry', '', 'gemstone-jewelry', 'publish', NULL, NULL, 66, 67, 33, NULL, NULL, '2021-04-12 06:15:19', '2021-04-12 06:15:19');
INSERT INTO `product_category` VALUES (35, 'Men\'s Watches', '', 'mens-watches', 'publish', NULL, NULL, 68, 69, 33, NULL, NULL, '2021-04-12 06:15:19', '2021-04-12 06:15:19');
INSERT INTO `product_category` VALUES (36, 'Women\'s Watches', '', 'womens-watches', 'publish', NULL, NULL, 70, 71, 33, NULL, NULL, '2021-04-12 06:15:19', '2021-04-12 06:15:19');
INSERT INTO `product_category` VALUES (37, 'Phones & Accessories', '', 'phones-accessories', 'publish', 46, NULL, 73, 82, NULL, NULL, NULL, '2021-04-12 06:15:19', '2021-04-12 06:15:19');
INSERT INTO `product_category` VALUES (38, 'Iphone 8', '', 'iphone-8', 'publish', NULL, NULL, 74, 75, 37, NULL, NULL, '2021-04-12 06:15:19', '2021-04-12 06:15:19');
INSERT INTO `product_category` VALUES (39, 'Iphone X', '', 'iphone-x', 'publish', NULL, NULL, 76, 77, 37, NULL, NULL, '2021-04-12 06:15:19', '2021-04-12 06:15:19');
INSERT INTO `product_category` VALUES (40, 'Samsung Note 8', '', 'samsung-note-8', 'publish', NULL, NULL, 78, 79, 37, NULL, NULL, '2021-04-12 06:15:19', '2021-04-12 06:15:19');
INSERT INTO `product_category` VALUES (41, 'Samsung S8', '', 'samsung-s8', 'publish', NULL, NULL, 80, 81, 37, NULL, NULL, '2021-04-12 06:15:20', '2021-04-12 06:15:20');
INSERT INTO `product_category` VALUES (42, 'Sport & Outdoor', '', 'sport-outdoor', 'publish', 47, NULL, 83, 90, NULL, NULL, NULL, '2021-04-12 06:15:20', '2021-04-12 06:15:20');
INSERT INTO `product_category` VALUES (43, 'Freezer Burn', '', 'freezer-burn', 'publish', NULL, NULL, 84, 85, 42, NULL, NULL, '2021-04-12 06:15:20', '2021-04-12 06:15:20');
INSERT INTO `product_category` VALUES (44, 'Fridge Cooler', '', 'fridge-cooler', 'publish', NULL, NULL, 86, 87, 42, NULL, NULL, '2021-04-12 06:15:20', '2021-04-12 06:15:20');
INSERT INTO `product_category` VALUES (45, 'Wine Cabinets', '', 'wine-cabinets', 'publish', NULL, NULL, 88, 89, 42, NULL, NULL, '2021-04-12 06:15:20', '2021-04-12 06:15:20');
INSERT INTO `product_category` VALUES (46, 'Babies & Moms', '', 'babies-moms', 'publish', NULL, NULL, 91, 92, NULL, NULL, NULL, '2021-04-12 06:15:20', '2021-04-12 06:15:20');
INSERT INTO `product_category` VALUES (47, 'Books & Office', '', 'books-office', 'publish', NULL, NULL, 93, 94, NULL, NULL, NULL, '2021-04-12 06:15:20', '2021-04-12 06:15:20');
INSERT INTO `product_category` VALUES (48, 'Cars & Motocycles', '', 'cars-motocycles', 'publish', NULL, NULL, 95, 96, NULL, NULL, NULL, '2021-04-12 06:15:20', '2021-04-12 06:15:20');

-- ----------------------------
-- Table structure for product_category_relations
-- ----------------------------
DROP TABLE IF EXISTS `product_category_relations`;
CREATE TABLE `product_category_relations`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `cat_id` int(11) NULL DEFAULT NULL,
  `target_id` int(11) NULL DEFAULT NULL,
  `create_user` bigint(20) NULL DEFAULT NULL,
  `update_user` bigint(20) NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of product_category_relations
-- ----------------------------
INSERT INTO `product_category_relations` VALUES (1, 1, 1, NULL, NULL, '2021-04-12 06:15:20', '2021-04-12 06:15:20');
INSERT INTO `product_category_relations` VALUES (2, 1, 2, NULL, NULL, '2021-04-12 06:15:20', '2021-04-12 06:15:20');
INSERT INTO `product_category_relations` VALUES (3, 1, 3, NULL, NULL, '2021-04-12 06:15:20', '2021-04-12 06:15:20');
INSERT INTO `product_category_relations` VALUES (4, 1, 4, NULL, NULL, '2021-04-12 06:15:20', '2021-04-12 06:15:20');
INSERT INTO `product_category_relations` VALUES (5, 1, 5, NULL, NULL, '2021-04-12 06:15:20', '2021-04-12 06:15:20');
INSERT INTO `product_category_relations` VALUES (6, 1, 6, NULL, NULL, '2021-04-12 06:15:20', '2021-04-12 06:15:20');
INSERT INTO `product_category_relations` VALUES (7, 1, 7, NULL, NULL, '2021-04-12 06:15:20', '2021-04-12 06:15:20');
INSERT INTO `product_category_relations` VALUES (8, 16, 8, NULL, NULL, '2021-04-12 06:15:20', '2021-04-12 06:15:20');
INSERT INTO `product_category_relations` VALUES (9, 29, 10, 1, NULL, '2021-04-20 05:48:07', '2021-04-20 05:48:07');
INSERT INTO `product_category_relations` VALUES (10, 29, 11, 1, NULL, '2021-04-20 05:49:07', '2021-04-20 05:49:07');

-- ----------------------------
-- Table structure for product_category_translations
-- ----------------------------
DROP TABLE IF EXISTS `product_category_translations`;
CREATE TABLE `product_category_translations`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `origin_id` bigint(20) NULL DEFAULT NULL,
  `locale` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `create_user` int(11) NULL DEFAULT NULL,
  `update_user` int(11) NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `product_category_translations_origin_id_locale_unique`(`origin_id`, `locale`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for product_order_item_meta
-- ----------------------------
DROP TABLE IF EXISTS `product_order_item_meta`;
CREATE TABLE `product_order_item_meta`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `order_id` bigint(20) NULL DEFAULT NULL,
  `order_item_id` bigint(20) NULL DEFAULT NULL,
  `meta_key` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `meta_value` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `create_user` int(11) NULL DEFAULT NULL,
  `update_user` int(11) NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for product_order_items
-- ----------------------------
DROP TABLE IF EXISTS `product_order_items`;
CREATE TABLE `product_order_items`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NULL DEFAULT NULL,
  `vendor_id` int(11) NULL DEFAULT NULL,
  `customer_id` int(11) NULL DEFAULT NULL,
  `product_id` int(11) NULL DEFAULT NULL,
  `product_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `type` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `qty` int(11) NULL DEFAULT NULL,
  `price` decimal(8, 2) NULL DEFAULT NULL,
  `subtotal` decimal(8, 2) NULL DEFAULT NULL,
  `commission` decimal(10, 2) NULL DEFAULT NULL,
  `commission_type` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `create_user` int(11) NULL DEFAULT NULL,
  `update_user` int(11) NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of product_order_items
-- ----------------------------
INSERT INTO `product_order_items` VALUES (1, 1, 1, NULL, 3, 'Herschel Leather Duffle Bag In Brown Color', 'product', 1, 125.30, 125.30, 12.53, '{\"amount\":\"10\",\"type\":\"percent', NULL, NULL, '2021-04-29 08:56:20', '2021-04-29 08:56:20');
INSERT INTO `product_order_items` VALUES (2, 2, 1, NULL, 2, 'Paul’s Smith Sneaker InWhite Color', 'product', 1, 75.44, 75.44, 7.54, '{\"amount\":\"10\",\"type\":\"percent', 1, NULL, '2021-05-05 05:01:01', '2021-05-05 05:01:01');
INSERT INTO `product_order_items` VALUES (3, 3, 1, NULL, 11, 'Android Tivi Sony 4K 43 inch KD-43X7500E', 'product', 1, 999999.99, 999999.99, 180000.00, '{\"amount\":\"10\",\"type\":\"percent', 1, NULL, '2021-05-05 05:21:47', '2021-05-05 05:21:47');
INSERT INTO `product_order_items` VALUES (4, 7, 1, NULL, 11, 'Android Tivi Sony 4K 43 inch KD-43X7500E', 'product', 1, 999999.99, 999999.99, 180000.00, '{\"amount\":\"10\",\"type\":\"percent', 1, NULL, '2021-05-05 05:41:52', '2021-05-05 05:41:52');
INSERT INTO `product_order_items` VALUES (5, 9, 1, NULL, 11, 'Android Tivi Sony 4K 43 inch KD-43X7500E', 'product', 1, 999999.99, 999999.99, 180000.00, '{\"amount\":\"10\",\"type\":\"percent', 1, NULL, '2021-05-05 05:43:28', '2021-05-05 05:43:28');

-- ----------------------------
-- Table structure for product_order_meta
-- ----------------------------
DROP TABLE IF EXISTS `product_order_meta`;
CREATE TABLE `product_order_meta`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `order_id` bigint(20) NULL DEFAULT NULL,
  `meta_key` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `meta_value` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `create_user` int(11) NULL DEFAULT NULL,
  `update_user` int(11) NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of product_order_meta
-- ----------------------------
INSERT INTO `product_order_meta` VALUES (1, 1, 'locale', 'en', NULL, NULL, NULL, NULL);
INSERT INTO `product_order_meta` VALUES (2, 2, 'locale', 'en', NULL, NULL, NULL, NULL);
INSERT INTO `product_order_meta` VALUES (3, 3, 'locale', 'en', NULL, NULL, NULL, NULL);
INSERT INTO `product_order_meta` VALUES (4, 7, 'locale', 'en', NULL, NULL, NULL, NULL);
INSERT INTO `product_order_meta` VALUES (5, 9, 'locale', 'en', NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for product_order_payments
-- ----------------------------
DROP TABLE IF EXISTS `product_order_payments`;
CREATE TABLE `product_order_payments`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NULL DEFAULT NULL,
  `payment_gateway` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `amount` decimal(10, 2) NULL DEFAULT NULL,
  `currency` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `converted_amount` decimal(10, 2) NULL DEFAULT NULL,
  `converted_currency` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `exchange_rate` decimal(10, 2) NULL DEFAULT NULL,
  `status` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `logs` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `create_user` int(11) NULL DEFAULT NULL,
  `update_user` int(11) NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for product_orders
-- ----------------------------
DROP TABLE IF EXISTS `product_orders`;
CREATE TABLE `product_orders`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `code` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `payment_id` int(11) NULL DEFAULT NULL,
  `gateway` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `customer_id` bigint(20) NULL DEFAULT NULL,
  `total` decimal(10, 2) NULL DEFAULT NULL,
  `final_total` decimal(10, 2) NULL DEFAULT NULL,
  `coupons` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `currency` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `status` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `total_before_fees` decimal(10, 2) NULL DEFAULT NULL,
  `total_before_tax` decimal(10, 2) NULL DEFAULT NULL,
  `tax_amount` decimal(10, 2) NULL DEFAULT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `first_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `last_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `phone` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `address2` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `city` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `state` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `postcode` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `country` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `company` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `shipping_first_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `shipping_last_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `shipping_address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `shipping_address2` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `shipping_city` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `shipping_state` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `shipping_postcode` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `shipping_country` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `shipping_company` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `customer_notes` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `payment_gateway` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `create_user` int(11) NULL DEFAULT NULL,
  `update_user` int(11) NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `product_orders_code_unique`(`code`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of product_orders
-- ----------------------------
INSERT INTO `product_orders` VALUES (1, '79afc0df5b1b2e849a35a33b8f2e93f3', NULL, 'offline_payment', NULL, 125.30, 125.30, '[]', NULL, 'processing', NULL, NULL, NULL, 'thanhdv193@gmail.com', 'Thanh', 'Do', '0347519202', 'hà nội', NULL, 'hà Nội', NULL, '100000', 'VN', 'thanh do', 'Thanh', 'Do', 'hà nội', NULL, 'hà Nội', NULL, '100000', 'VN', 'thanh do', NULL, NULL, NULL, NULL, '2021-04-29 08:56:20', '2021-04-29 08:56:20', NULL);
INSERT INTO `product_orders` VALUES (2, '52522cfe0cd222d5462cfaf7be6d3556', NULL, 'offline_payment', 1, 75.44, 75.44, '[]', NULL, 'processing', NULL, NULL, NULL, 'thanhdv193@gmail.com', 'Thanh', 'Thanh', '0347519202', 'hà nội', NULL, NULL, NULL, NULL, 'VI', 'thanh do', 'Thanh', 'Thanh', 'hà nội', NULL, NULL, NULL, NULL, 'VI', 'thanh do', NULL, NULL, 1, 1, '2021-05-05 05:01:01', '2021-05-05 05:01:01', NULL);
INSERT INTO `product_orders` VALUES (3, 'c0e7d269fd71bea1df3a115bb3b11a9f', NULL, 'offline_payment', 1, 1800000.00, 1800000.00, '[]', NULL, 'processing', NULL, NULL, NULL, 'thanhdv193@gmail.com', 'Thanh', 'Thanh', '0347519202', 'hà nội', NULL, NULL, NULL, NULL, 'VI', 'thanh do', 'Thanh', 'Thanh', 'hà nội', NULL, NULL, NULL, NULL, 'VI', 'thanh do', NULL, NULL, 1, 1, '2021-05-05 05:21:47', '2021-05-05 05:21:47', NULL);
INSERT INTO `product_orders` VALUES (4, '155bce8b7cc8a226d4f4365d8a85a3ce', NULL, 'offline_payment', 1, 1800000.00, 1800000.00, '[]', NULL, 'draft', NULL, NULL, NULL, 'admin@dev.com', 'ThanhAdmin', 'ThanhAdmin', '0347519202', 'hà nội', NULL, NULL, NULL, NULL, 'VI', NULL, 'ThanhAdmin', 'ThanhAdmin', 'hà nội', NULL, NULL, NULL, NULL, 'VI', NULL, NULL, NULL, 1, NULL, '2021-05-05 05:30:36', '2021-05-05 05:30:36', NULL);
INSERT INTO `product_orders` VALUES (5, '0b0bfcb4141976534861b0649afc5d7b', NULL, 'offline_payment', 1, 1800000.00, 1800000.00, '[]', NULL, 'draft', NULL, NULL, NULL, 'admin@dev.com', 'ThanhAdmin', 'ThanhAdmin', '0347519202', 'hà nội', NULL, NULL, NULL, NULL, 'VI', NULL, 'ThanhAdmin', 'ThanhAdmin', 'hà nội', NULL, NULL, NULL, NULL, 'VI', NULL, NULL, NULL, 1, NULL, '2021-05-05 05:31:07', '2021-05-05 05:31:07', NULL);
INSERT INTO `product_orders` VALUES (6, '93ab581437fad6d8c0fec15fbbdabbe5', NULL, 'offline_payment', 1, 1800000.00, 1800000.00, '[]', NULL, 'draft', NULL, NULL, NULL, 'admin@dev.com', 'ThanhAdmin', 'ThanhAdmin', '0347519202', 'hà nội', NULL, NULL, NULL, NULL, 'VI', NULL, 'ThanhAdmin', 'ThanhAdmin', 'hà nội', NULL, NULL, NULL, NULL, 'VI', NULL, NULL, NULL, 1, NULL, '2021-05-05 05:31:23', '2021-05-05 05:31:23', NULL);
INSERT INTO `product_orders` VALUES (7, 'd35b6e29bbb58ac64a5661a8e2327cce', NULL, 'offline_payment', 1, 1800000.00, 1800000.00, '[]', NULL, 'processing', NULL, NULL, NULL, 'admin@dev.com', 'ThanhAdmin', 'ThanhAdmin', '0347519202', 'hà nội', NULL, NULL, NULL, NULL, 'VI', NULL, 'ThanhAdmin', 'ThanhAdmin', 'hà nội', NULL, NULL, NULL, NULL, 'VI', NULL, NULL, NULL, 1, 1, '2021-05-05 05:41:52', '2021-05-05 05:41:52', NULL);
INSERT INTO `product_orders` VALUES (8, 'b566e2b1baa70a7a90077254ca36e580', NULL, 'offline_payment', 1, 1800000.00, 1800000.00, '[]', NULL, 'draft', NULL, NULL, NULL, 'admin@dev.com', 'ThanhAdminAdmin', 'ThanhAdminAdmin', '0347519202', 'hà nội', NULL, NULL, NULL, NULL, 'VI', NULL, 'ThanhAdminAdmin', 'ThanhAdminAdmin', 'hà nội', NULL, NULL, NULL, NULL, 'VI', NULL, NULL, NULL, 1, NULL, '2021-05-05 05:42:33', '2021-05-05 05:42:33', NULL);
INSERT INTO `product_orders` VALUES (9, '5ab39d70711846b3954047bd613991a3', NULL, 'offline_payment', 1, 1800000.00, 1800000.00, '[]', NULL, 'processing', NULL, NULL, NULL, 'admin@dev.com', 'ThanhAdminAdmin', 'ThanhAdminAdmin', '0347519202', 'hà nội', NULL, NULL, NULL, NULL, 'VI', NULL, 'ThanhAdminAdmin', 'ThanhAdminAdmin', 'hà nội', NULL, NULL, NULL, NULL, 'VI', NULL, NULL, NULL, 1, 1, '2021-05-05 05:43:28', '2021-05-05 05:43:28', NULL);

-- ----------------------------
-- Table structure for product_tag
-- ----------------------------
DROP TABLE IF EXISTS `product_tag`;
CREATE TABLE `product_tag`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `tag_id` int(11) NULL DEFAULT NULL,
  `target_id` int(11) NULL DEFAULT NULL,
  `create_user` bigint(20) NULL DEFAULT NULL,
  `update_user` bigint(20) NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for product_term
-- ----------------------------
DROP TABLE IF EXISTS `product_term`;
CREATE TABLE `product_term`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `term_id` int(11) NULL DEFAULT NULL,
  `target_id` int(11) NULL DEFAULT NULL,
  `create_user` bigint(20) NULL DEFAULT NULL,
  `update_user` bigint(20) NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 20 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of product_term
-- ----------------------------
INSERT INTO `product_term` VALUES (1, 1, 1, NULL, NULL, '2021-04-12 06:15:20', '2021-04-12 06:15:20');
INSERT INTO `product_term` VALUES (2, 3, 1, NULL, NULL, '2021-04-12 06:15:20', '2021-04-12 06:15:20');
INSERT INTO `product_term` VALUES (3, 2, 2, NULL, NULL, '2021-04-12 06:15:20', '2021-04-12 06:15:20');
INSERT INTO `product_term` VALUES (4, 3, 2, NULL, NULL, '2021-04-12 06:15:20', '2021-04-12 06:15:20');
INSERT INTO `product_term` VALUES (5, 1, 3, NULL, NULL, '2021-04-12 06:15:20', '2021-04-12 06:15:20');
INSERT INTO `product_term` VALUES (6, 5, 3, NULL, NULL, '2021-04-12 06:15:20', '2021-04-12 06:15:20');
INSERT INTO `product_term` VALUES (7, 2, 4, NULL, NULL, '2021-04-12 06:15:20', '2021-04-12 06:15:20');
INSERT INTO `product_term` VALUES (8, 6, 4, NULL, NULL, '2021-04-12 06:15:20', '2021-04-12 06:15:20');
INSERT INTO `product_term` VALUES (9, 2, 5, NULL, NULL, '2021-04-12 06:15:20', '2021-04-12 06:15:20');
INSERT INTO `product_term` VALUES (10, 6, 5, NULL, NULL, '2021-04-12 06:15:20', '2021-04-12 06:15:20');
INSERT INTO `product_term` VALUES (11, 1, 7, NULL, NULL, '2021-04-12 06:15:20', '2021-04-12 06:15:20');
INSERT INTO `product_term` VALUES (12, 5, 7, NULL, NULL, '2021-04-12 06:15:20', '2021-04-12 06:15:20');
INSERT INTO `product_term` VALUES (13, 2, 8, NULL, NULL, '2021-04-12 06:15:20', '2021-04-12 06:15:20');
INSERT INTO `product_term` VALUES (14, 3, 8, NULL, NULL, '2021-04-12 06:15:20', '2021-04-12 06:15:20');
INSERT INTO `product_term` VALUES (15, 1, 6, NULL, NULL, '2021-04-12 06:15:20', '2021-04-12 06:15:20');
INSERT INTO `product_term` VALUES (16, 2, 6, NULL, NULL, '2021-04-12 06:15:20', '2021-04-12 06:15:20');
INSERT INTO `product_term` VALUES (17, 4, 6, NULL, NULL, '2021-04-12 06:15:20', '2021-04-12 06:15:20');
INSERT INTO `product_term` VALUES (18, 5, 6, NULL, NULL, '2021-04-12 06:15:20', '2021-04-12 06:15:20');
INSERT INTO `product_term` VALUES (19, 6, 6, NULL, NULL, '2021-04-12 06:15:20', '2021-04-12 06:15:20');

-- ----------------------------
-- Table structure for product_translations
-- ----------------------------
DROP TABLE IF EXISTS `product_translations`;
CREATE TABLE `product_translations`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `slug` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `short_desc` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `create_user` bigint(20) NULL DEFAULT NULL,
  `update_user` bigint(20) NULL DEFAULT NULL,
  `origin_id` bigint(20) NULL DEFAULT NULL,
  `locale` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `product_translations_slug_index`(`slug`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of product_translations
-- ----------------------------
INSERT INTO `product_translations` VALUES (1, 'Lò vi sóng Sharp R-G272VN-S 20 lít', '', '<ul style=\"margin-top:0px;padding:0px;margin-bottom:1.3em;color:#0a0a0a;font-family:Roboto, sans-serif;font-size:15px;background-color:#ffffff;\">\r\n<li style=\"margin-bottom:.6em;margin-left:1.3em;\">Bảng điều khiển điện tử tích hợp sẵn các chức năng nấu, thực đơn tự động.</li>\r\n<li style=\"margin-bottom:.6em;margin-left:1.3em;\">Chức năng khóa bảng điều khiển tự động vô hiệu hóa toàn bộ bàn phím.</li>\r\n<li style=\"margin-bottom:.6em;margin-left:1.3em;\">Lò vi sóng Sharp R-G272VN-S thương hiệu nổi tiếng Nhật Bản.</li>\r\n</ul>', NULL, 1, NULL, 10, 'en', NULL, '2021-04-20 05:48:07', '2021-04-20 05:48:07');
INSERT INTO `product_translations` VALUES (2, 'Android Tivi Sony 4K 43 inch KD-43X7500E', '', '<p><span style=\"color:#0a0a0a;font-family:Roboto, sans-serif;font-size:15px;background-color:#ffffff;\">Thiết kế mỏng ấn tượng Smart Tivi Sony 4K 43 inch KD-43X7500E sở hữu kiểu dáng tinh tế, thời trang cùng với kích thước màn hình rộng 43 inch, kết hợp khung viền chắc chắn, sắc sảo và tông màu đen quý phái. Trải nghiệm hình ảnh rõ ràng, chân thật Không chỉ…</span></p>', NULL, 1, 1, 11, 'en', NULL, '2021-04-20 05:49:07', '2021-05-05 05:21:34');
INSERT INTO `product_translations` VALUES (3, 'Korea Long Sofa Fabric In Blue Navy Color', '', '<p><strong>Embodying the Raw, Wayward Spirit of Rock \'N\' Roll</strong></p>\r\n<p>Embodying the raw, wayward spirit of rock ‘n’ roll, the Kilburn portable active stereo speaker takes the unmistakable look and sound of Marshall, unplugs the chords, and takes the show on the road.</p>\r\n<p> </p>\r\n<p>Weighing in under 7 pounds, the Kilburn is a lightweight piece of vintage styled engineering. Setting the bar as one of the loudest speakers in its class, the Kilburn is a compact, stout-hearted hero with a well-balanced audio which boasts a clear midrange and extended highs for a sound that is both articulate and pronounced. The analogue knobs allow you to fine tune the controls to your personal preferences while the guitar-influenced leather strap enables easy and stylish travel.</p>\r\n<p><img src=\"../../../../uploads/demo/templates/post-image.jpg\" alt=\"Embodying the Raw, Wayward Spirit of Rock \'N\' Roll\" width=\"654\" height=\"205\" /></p>\r\n<p>What do you get</p>\r\n<p>Sound of Marshall, unplugs the chords, and takes the show on the road.</p>\r\n<p>Weighing in under 7 pounds, the Kilburn is a lightweight piece of vintage styled engineering. Setting the bar as one of the loudest speakers in its class, the Kilburn is a compact, stout-hearted hero with a well-balanced audio which boasts a clear midrange and extended highs for a sound that is both articulate and pronounced. The analogue knobs allow you to fine tune the controls to your personal preferences while the guitar-influenced leather strap enables easy and stylish travel.</p>\r\n<p> </p>\r\n<p>The FM radio is perhaps gone for good, the assumption apparently being that the jury has ruled in favor of streaming over the internet. The IR blaster is another feature due for retirement – the S6 had it, then the Note5 didn’t, and now with the S7 the trend is clear.</p>\r\n<p> </p>\r\n<p>Perfectly Done</p>\r\n<p>Meanwhile, the IP68 water resistance has improved from the S5, allowing submersion of up to five feet for 30 minutes, plus there’s no annoying flap covering the charging port</p>\r\n<p> </p>\r\n<ul>\r\n<li>No FM radio (except for T-Mobile units in the US, so far)</li>\r\n<li>No IR blaster</li>\r\n<li>No stereo speakers</li>\r\n</ul>\r\n<p>If you’ve taken the phone for a plunge in the bath, you’ll need to dry the charging port before plugging in. Samsung hasn’t reinvented the wheel with the design of the Galaxy S7, but it didn’t need to. The Gala S6 was an excellently styled device, and the S7 has managed to improve on that.</p>\r\n<div>\r\n<div class=\"gtx-trans-icon\"> </div>\r\n</div>', '<ul>\r\n<li>Unrestrained and portable active stereo speaker</li>\r\n<li>Free from the confines of wires and chords</li>\r\n<li>20 hours of portable capabilities</li>\r\n<li>Double-ended Coil Cord with 3.5mm Stereo Plugs Included</li>\r\n<li>3/4&Prime; Dome Tweeters: 2X and 4&Prime; Woofer: 1X</li>\r\n</ul>', 1, 1, 7, 'en', NULL, '2021-05-04 05:18:46', '2021-05-04 05:19:35');

-- ----------------------------
-- Table structure for product_variation_term
-- ----------------------------
DROP TABLE IF EXISTS `product_variation_term`;
CREATE TABLE `product_variation_term`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NULL DEFAULT NULL,
  `variation_id` int(11) NULL DEFAULT NULL,
  `term_id` int(11) NULL DEFAULT NULL,
  `create_user` bigint(20) NULL DEFAULT NULL,
  `update_user` bigint(20) NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 13 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of product_variation_term
-- ----------------------------
INSERT INTO `product_variation_term` VALUES (1, 6, 6, 1, 1, NULL, NULL, NULL);
INSERT INTO `product_variation_term` VALUES (2, 6, 6, 4, 1, NULL, NULL, NULL);
INSERT INTO `product_variation_term` VALUES (3, 6, 5, 1, 1, NULL, NULL, NULL);
INSERT INTO `product_variation_term` VALUES (4, 6, 5, 5, 1, NULL, NULL, NULL);
INSERT INTO `product_variation_term` VALUES (5, 6, 4, 1, 1, NULL, NULL, NULL);
INSERT INTO `product_variation_term` VALUES (6, 6, 4, 6, 1, NULL, NULL, NULL);
INSERT INTO `product_variation_term` VALUES (7, 6, 3, 2, 1, NULL, NULL, NULL);
INSERT INTO `product_variation_term` VALUES (8, 6, 3, 4, 1, NULL, NULL, NULL);
INSERT INTO `product_variation_term` VALUES (9, 6, 2, 2, 1, NULL, NULL, NULL);
INSERT INTO `product_variation_term` VALUES (10, 6, 2, 5, 1, NULL, NULL, NULL);
INSERT INTO `product_variation_term` VALUES (11, 6, 1, 2, 1, NULL, NULL, NULL);
INSERT INTO `product_variation_term` VALUES (12, 6, 1, 6, 1, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for product_variation_translations
-- ----------------------------
DROP TABLE IF EXISTS `product_variation_translations`;
CREATE TABLE `product_variation_translations`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `variation_id` int(11) NULL DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `create_user` bigint(20) NULL DEFAULT NULL,
  `update_user` bigint(20) NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for product_variations
-- ----------------------------
DROP TABLE IF EXISTS `product_variations`;
CREATE TABLE `product_variations`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NULL DEFAULT NULL,
  `shipping_class` int(11) NULL DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `position` tinyint(4) NULL DEFAULT NULL,
  `sku` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `image_id` int(11) NULL DEFAULT NULL,
  `price` decimal(10, 2) NULL DEFAULT NULL,
  `sold` tinyint(4) NULL DEFAULT NULL,
  `quantity` tinyint(4) NULL DEFAULT NULL,
  `is_manage_stock` tinyint(4) NULL DEFAULT NULL,
  `stock_status` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `weight` decimal(5, 2) NULL DEFAULT NULL,
  `length` decimal(5, 2) NULL DEFAULT NULL,
  `width` decimal(5, 2) NULL DEFAULT NULL,
  `height` decimal(5, 2) NULL DEFAULT NULL,
  `active` tinyint(4) NULL DEFAULT NULL,
  `create_user` bigint(20) NULL DEFAULT NULL,
  `update_user` bigint(20) NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of product_variations
-- ----------------------------
INSERT INTO `product_variations` VALUES (1, 6, NULL, NULL, NULL, NULL, NULL, 22.00, NULL, NULL, NULL, 'in', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL);
INSERT INTO `product_variations` VALUES (2, 6, NULL, NULL, NULL, NULL, NULL, 22.00, NULL, NULL, NULL, 'in', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL);
INSERT INTO `product_variations` VALUES (3, 6, NULL, NULL, NULL, NULL, NULL, 22.00, NULL, NULL, NULL, 'in', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL);
INSERT INTO `product_variations` VALUES (4, 6, NULL, NULL, NULL, NULL, NULL, 20.00, NULL, NULL, NULL, 'in', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL);
INSERT INTO `product_variations` VALUES (5, 6, NULL, NULL, NULL, NULL, NULL, 20.00, NULL, NULL, NULL, 'in', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL);
INSERT INTO `product_variations` VALUES (6, 6, NULL, NULL, NULL, NULL, NULL, 20.00, NULL, NULL, NULL, 'in', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for products
-- ----------------------------
DROP TABLE IF EXISTS `products`;
CREATE TABLE `products`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `sku` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `slug` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `image_id` int(11) NULL DEFAULT NULL,
  `banner_image_id` int(11) NULL DEFAULT NULL,
  `short_desc` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `category_id` int(11) NULL DEFAULT NULL,
  `brand_id` int(11) NULL DEFAULT NULL,
  `is_featured` tinyint(4) NULL DEFAULT NULL,
  `shipping_class` int(11) NULL DEFAULT NULL,
  `gallery` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `video` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `price` decimal(12, 2) NULL DEFAULT NULL,
  `sale_price` decimal(12, 2) NULL DEFAULT NULL,
  `status` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `attributes_for_variation` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `weight` decimal(5, 2) NULL DEFAULT NULL,
  `length` decimal(5, 2) NULL DEFAULT NULL,
  `width` decimal(5, 2) NULL DEFAULT NULL,
  `height` decimal(5, 2) NULL DEFAULT NULL,
  `sold` tinyint(4) NULL DEFAULT NULL,
  `quantity` tinyint(4) NULL DEFAULT NULL,
  `is_manage_stock` tinyint(4) NULL DEFAULT NULL,
  `stock_status` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `product_type` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `review_score` decimal(2, 1) NULL DEFAULT NULL,
  `create_user` bigint(20) NULL DEFAULT NULL,
  `update_user` bigint(20) NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `specification` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `products_slug_index`(`slug`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 12 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of products
-- ----------------------------
INSERT INTO `products` VALUES (1, NULL, 'Sleeve Linen Blend Caro Pane Shirt', 'sleeve-linen-blend-caro-pane-shirt', '<p><strong>Embodying the Raw, Wayward Spirit of Rock \'N\' Roll</strong></p><p>Embodying the raw, wayward spirit of rock ‘n’ roll, the Kilburn portable active stereo speaker takes the unmistakable look and sound of Marshall, unplugs the chords, and takes the show on the road.</p><p> </p><p>Weighing in under 7 pounds, the Kilburn is a lightweight piece of vintage styled engineering. Setting the bar as one of the loudest speakers in its class, the Kilburn is a compact, stout-hearted hero with a well-balanced audio which boasts a clear midrange and extended highs for a sound that is both articulate and pronounced. The analogue knobs allow you to fine tune the controls to your personal preferences while the guitar-influenced leather strap enables easy and stylish travel.</p><p><img src=\"/uploads/demo/templates/post-image.jpg\" alt=\"Embodying the Raw, Wayward Spirit of Rock \'N\' Roll\" width=\"654\" height=\"205\" /></p><p>What do you get</p><p>Sound of Marshall, unplugs the chords, and takes the show on the road.</p><p>Weighing in under 7 pounds, the Kilburn is a lightweight piece of vintage styled engineering. Setting the bar as one of the loudest speakers in its class, the Kilburn is a compact, stout-hearted hero with a well-balanced audio which boasts a clear midrange and extended highs for a sound that is both articulate and pronounced. The analogue knobs allow you to fine tune the controls to your personal preferences while the guitar-influenced leather strap enables easy and stylish travel.</p><p> </p><p>The FM radio is perhaps gone for good, the assumption apparently being that the jury has ruled in favor of streaming over the internet. The IR blaster is another feature due for retirement – the S6 had it, then the Note5 didn’t, and now with the S7 the trend is clear.</p><p> </p><p>Perfectly Done</p><p>Meanwhile, the IP68 water resistance has improved from the S5, allowing submersion of up to five feet for 30 minutes, plus there’s no annoying flap covering the charging port</p><p> </p><ul><li>No FM radio (except for T-Mobile units in the US, so far)</li><li>No IR blaster</li><li>No stereo speakers</li></ul><p>If you’ve taken the phone for a plunge in the bath, you’ll need to dry the charging port before plugging in. Samsung hasn’t reinvented the wheel with the design of the Galaxy S7, but it didn’t need to. The Gala S6 was an excellently styled device, and the S7 has managed to improve on that.</p><div><div class=\"gtx-trans-icon\"> </div></div>', 30, NULL, '<ul><li>Unrestrained and portable active stereo speaker</li><li>Free from the confines of wires and chords</li><li>20 hours of portable capabilities</li><li>Double-ended Coil Cord with 3.5mm Stereo Plugs Included</li><li>3/4″ Dome Tweeters: 2X and 4″ Woofer: 1X</li></ul>', NULL, 5, NULL, NULL, '26,27,28,29', NULL, 39.99, 29.39, 'publish', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'in', 'simple', NULL, 1, NULL, NULL, NULL, NULL);
INSERT INTO `products` VALUES (2, NULL, 'Paul’s Smith Sneaker InWhite Color', 'pauls-smith-sneaker-inwhite-color', '<p><strong>Embodying the Raw, Wayward Spirit of Rock \'N\' Roll</strong></p><p>Embodying the raw, wayward spirit of rock ‘n’ roll, the Kilburn portable active stereo speaker takes the unmistakable look and sound of Marshall, unplugs the chords, and takes the show on the road.</p><p> </p><p>Weighing in under 7 pounds, the Kilburn is a lightweight piece of vintage styled engineering. Setting the bar as one of the loudest speakers in its class, the Kilburn is a compact, stout-hearted hero with a well-balanced audio which boasts a clear midrange and extended highs for a sound that is both articulate and pronounced. The analogue knobs allow you to fine tune the controls to your personal preferences while the guitar-influenced leather strap enables easy and stylish travel.</p><p><img src=\"/uploads/demo/templates/post-image.jpg\" alt=\"Embodying the Raw, Wayward Spirit of Rock \'N\' Roll\" width=\"654\" height=\"205\" /></p><p>What do you get</p><p>Sound of Marshall, unplugs the chords, and takes the show on the road.</p><p>Weighing in under 7 pounds, the Kilburn is a lightweight piece of vintage styled engineering. Setting the bar as one of the loudest speakers in its class, the Kilburn is a compact, stout-hearted hero with a well-balanced audio which boasts a clear midrange and extended highs for a sound that is both articulate and pronounced. The analogue knobs allow you to fine tune the controls to your personal preferences while the guitar-influenced leather strap enables easy and stylish travel.</p><p> </p><p>The FM radio is perhaps gone for good, the assumption apparently being that the jury has ruled in favor of streaming over the internet. The IR blaster is another feature due for retirement – the S6 had it, then the Note5 didn’t, and now with the S7 the trend is clear.</p><p> </p><p>Perfectly Done</p><p>Meanwhile, the IP68 water resistance has improved from the S5, allowing submersion of up to five feet for 30 minutes, plus there’s no annoying flap covering the charging port</p><p> </p><ul><li>No FM radio (except for T-Mobile units in the US, so far)</li><li>No IR blaster</li><li>No stereo speakers</li></ul><p>If you’ve taken the phone for a plunge in the bath, you’ll need to dry the charging port before plugging in. Samsung hasn’t reinvented the wheel with the design of the Galaxy S7, but it didn’t need to. The Gala S6 was an excellently styled device, and the S7 has managed to improve on that.</p><div><div class=\"gtx-trans-icon\"> </div></div>', 31, NULL, '<ul><li>Unrestrained and portable active stereo speaker</li><li>Free from the confines of wires and chords</li><li>20 hours of portable capabilities</li><li>Double-ended Coil Cord with 3.5mm Stereo Plugs Included</li><li>3/4″ Dome Tweeters: 2X and 4″ Woofer: 1X</li></ul>', NULL, 5, NULL, NULL, '26,27,28,29', NULL, 75.44, NULL, 'publish', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, 'in', 'simple', NULL, 1, NULL, NULL, '2021-05-05 05:01:01', NULL);
INSERT INTO `products` VALUES (3, NULL, 'Herschel Leather Duffle Bag In Brown Color', 'herschel-leather-duffle-bag-in-brown-color', '<p><strong>Embodying the Raw, Wayward Spirit of Rock \'N\' Roll</strong></p><p>Embodying the raw, wayward spirit of rock ‘n’ roll, the Kilburn portable active stereo speaker takes the unmistakable look and sound of Marshall, unplugs the chords, and takes the show on the road.</p><p> </p><p>Weighing in under 7 pounds, the Kilburn is a lightweight piece of vintage styled engineering. Setting the bar as one of the loudest speakers in its class, the Kilburn is a compact, stout-hearted hero with a well-balanced audio which boasts a clear midrange and extended highs for a sound that is both articulate and pronounced. The analogue knobs allow you to fine tune the controls to your personal preferences while the guitar-influenced leather strap enables easy and stylish travel.</p><p><img src=\"/uploads/demo/templates/post-image.jpg\" alt=\"Embodying the Raw, Wayward Spirit of Rock \'N\' Roll\" width=\"654\" height=\"205\" /></p><p>What do you get</p><p>Sound of Marshall, unplugs the chords, and takes the show on the road.</p><p>Weighing in under 7 pounds, the Kilburn is a lightweight piece of vintage styled engineering. Setting the bar as one of the loudest speakers in its class, the Kilburn is a compact, stout-hearted hero with a well-balanced audio which boasts a clear midrange and extended highs for a sound that is both articulate and pronounced. The analogue knobs allow you to fine tune the controls to your personal preferences while the guitar-influenced leather strap enables easy and stylish travel.</p><p> </p><p>The FM radio is perhaps gone for good, the assumption apparently being that the jury has ruled in favor of streaming over the internet. The IR blaster is another feature due for retirement – the S6 had it, then the Note5 didn’t, and now with the S7 the trend is clear.</p><p> </p><p>Perfectly Done</p><p>Meanwhile, the IP68 water resistance has improved from the S5, allowing submersion of up to five feet for 30 minutes, plus there’s no annoying flap covering the charging port</p><p> </p><ul><li>No FM radio (except for T-Mobile units in the US, so far)</li><li>No IR blaster</li><li>No stereo speakers</li></ul><p>If you’ve taken the phone for a plunge in the bath, you’ll need to dry the charging port before plugging in. Samsung hasn’t reinvented the wheel with the design of the Galaxy S7, but it didn’t need to. The Gala S6 was an excellently styled device, and the S7 has managed to improve on that.</p><div><div class=\"gtx-trans-icon\"> </div></div>', 32, NULL, '<ul><li>Unrestrained and portable active stereo speaker</li><li>Free from the confines of wires and chords</li><li>20 hours of portable capabilities</li><li>Double-ended Coil Cord with 3.5mm Stereo Plugs Included</li><li>3/4″ Dome Tweeters: 2X and 4″ Woofer: 1X</li></ul>', NULL, 1, NULL, NULL, '26,27,28,29', NULL, 125.30, NULL, 'publish', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, 'in', 'simple', NULL, 1, NULL, NULL, '2021-04-29 08:56:20', NULL);
INSERT INTO `products` VALUES (4, NULL, 'Unero Military Classical Backpack', 'unero-military-classical-backpack', '<p><strong>Embodying the Raw, Wayward Spirit of Rock \'N\' Roll</strong></p><p>Embodying the raw, wayward spirit of rock ‘n’ roll, the Kilburn portable active stereo speaker takes the unmistakable look and sound of Marshall, unplugs the chords, and takes the show on the road.</p><p> </p><p>Weighing in under 7 pounds, the Kilburn is a lightweight piece of vintage styled engineering. Setting the bar as one of the loudest speakers in its class, the Kilburn is a compact, stout-hearted hero with a well-balanced audio which boasts a clear midrange and extended highs for a sound that is both articulate and pronounced. The analogue knobs allow you to fine tune the controls to your personal preferences while the guitar-influenced leather strap enables easy and stylish travel.</p><p><img src=\"/uploads/demo/templates/post-image.jpg\" alt=\"Embodying the Raw, Wayward Spirit of Rock \'N\' Roll\" width=\"654\" height=\"205\" /></p><p>What do you get</p><p>Sound of Marshall, unplugs the chords, and takes the show on the road.</p><p>Weighing in under 7 pounds, the Kilburn is a lightweight piece of vintage styled engineering. Setting the bar as one of the loudest speakers in its class, the Kilburn is a compact, stout-hearted hero with a well-balanced audio which boasts a clear midrange and extended highs for a sound that is both articulate and pronounced. The analogue knobs allow you to fine tune the controls to your personal preferences while the guitar-influenced leather strap enables easy and stylish travel.</p><p> </p><p>The FM radio is perhaps gone for good, the assumption apparently being that the jury has ruled in favor of streaming over the internet. The IR blaster is another feature due for retirement – the S6 had it, then the Note5 didn’t, and now with the S7 the trend is clear.</p><p> </p><p>Perfectly Done</p><p>Meanwhile, the IP68 water resistance has improved from the S5, allowing submersion of up to five feet for 30 minutes, plus there’s no annoying flap covering the charging port</p><p> </p><ul><li>No FM radio (except for T-Mobile units in the US, so far)</li><li>No IR blaster</li><li>No stereo speakers</li></ul><p>If you’ve taken the phone for a plunge in the bath, you’ll need to dry the charging port before plugging in. Samsung hasn’t reinvented the wheel with the design of the Galaxy S7, but it didn’t need to. The Gala S6 was an excellently styled device, and the S7 has managed to improve on that.</p><div><div class=\"gtx-trans-icon\"> </div></div>', 33, NULL, '<ul><li>Unrestrained and portable active stereo speaker</li><li>Free from the confines of wires and chords</li><li>20 hours of portable capabilities</li><li>Double-ended Coil Cord with 3.5mm Stereo Plugs Included</li><li>3/4″ Dome Tweeters: 2X and 4″ Woofer: 1X</li></ul>', NULL, 1, NULL, NULL, '26,27,28,29', NULL, 42.39, NULL, 'publish', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'in', 'simple', NULL, 1, NULL, NULL, NULL, NULL);
INSERT INTO `products` VALUES (5, NULL, 'Rayban Rounded Sunglass Brown Color', 'rayban-rounded-sunglass-brown-color', '<p><strong>Embodying the Raw, Wayward Spirit of Rock \'N\' Roll</strong></p><p>Embodying the raw, wayward spirit of rock ‘n’ roll, the Kilburn portable active stereo speaker takes the unmistakable look and sound of Marshall, unplugs the chords, and takes the show on the road.</p><p> </p><p>Weighing in under 7 pounds, the Kilburn is a lightweight piece of vintage styled engineering. Setting the bar as one of the loudest speakers in its class, the Kilburn is a compact, stout-hearted hero with a well-balanced audio which boasts a clear midrange and extended highs for a sound that is both articulate and pronounced. The analogue knobs allow you to fine tune the controls to your personal preferences while the guitar-influenced leather strap enables easy and stylish travel.</p><p><img src=\"/uploads/demo/templates/post-image.jpg\" alt=\"Embodying the Raw, Wayward Spirit of Rock \'N\' Roll\" width=\"654\" height=\"205\" /></p><p>What do you get</p><p>Sound of Marshall, unplugs the chords, and takes the show on the road.</p><p>Weighing in under 7 pounds, the Kilburn is a lightweight piece of vintage styled engineering. Setting the bar as one of the loudest speakers in its class, the Kilburn is a compact, stout-hearted hero with a well-balanced audio which boasts a clear midrange and extended highs for a sound that is both articulate and pronounced. The analogue knobs allow you to fine tune the controls to your personal preferences while the guitar-influenced leather strap enables easy and stylish travel.</p><p> </p><p>The FM radio is perhaps gone for good, the assumption apparently being that the jury has ruled in favor of streaming over the internet. The IR blaster is another feature due for retirement – the S6 had it, then the Note5 didn’t, and now with the S7 the trend is clear.</p><p> </p><p>Perfectly Done</p><p>Meanwhile, the IP68 water resistance has improved from the S5, allowing submersion of up to five feet for 30 minutes, plus there’s no annoying flap covering the charging port</p><p> </p><ul><li>No FM radio (except for T-Mobile units in the US, so far)</li><li>No IR blaster</li><li>No stereo speakers</li></ul><p>If you’ve taken the phone for a plunge in the bath, you’ll need to dry the charging port before plugging in. Samsung hasn’t reinvented the wheel with the design of the Galaxy S7, but it didn’t need to. The Gala S6 was an excellently styled device, and the S7 has managed to improve on that.</p><div><div class=\"gtx-trans-icon\"> </div></div>', 34, NULL, '<ul><li>Unrestrained and portable active stereo speaker</li><li>Free from the confines of wires and chords</li><li>20 hours of portable capabilities</li><li>Double-ended Coil Cord with 3.5mm Stereo Plugs Included</li><li>3/4″ Dome Tweeters: 2X and 4″ Woofer: 1X</li></ul>', NULL, 2, NULL, NULL, '26,27,28,29', NULL, 125.89, NULL, 'publish', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'in', 'simple', NULL, 1, NULL, NULL, NULL, NULL);
INSERT INTO `products` VALUES (6, NULL, 'Men’s Sports Runnning Swim Board Shorts', 'mens-sports-runnning-swim-board-shorts', '<p><strong>Embodying the Raw, Wayward Spirit of Rock \'N\' Roll</strong></p><p>Embodying the raw, wayward spirit of rock ‘n’ roll, the Kilburn portable active stereo speaker takes the unmistakable look and sound of Marshall, unplugs the chords, and takes the show on the road.</p><p> </p><p>Weighing in under 7 pounds, the Kilburn is a lightweight piece of vintage styled engineering. Setting the bar as one of the loudest speakers in its class, the Kilburn is a compact, stout-hearted hero with a well-balanced audio which boasts a clear midrange and extended highs for a sound that is both articulate and pronounced. The analogue knobs allow you to fine tune the controls to your personal preferences while the guitar-influenced leather strap enables easy and stylish travel.</p><p><img src=\"/uploads/demo/templates/post-image.jpg\" alt=\"Embodying the Raw, Wayward Spirit of Rock \'N\' Roll\" width=\"654\" height=\"205\" /></p><p>What do you get</p><p>Sound of Marshall, unplugs the chords, and takes the show on the road.</p><p>Weighing in under 7 pounds, the Kilburn is a lightweight piece of vintage styled engineering. Setting the bar as one of the loudest speakers in its class, the Kilburn is a compact, stout-hearted hero with a well-balanced audio which boasts a clear midrange and extended highs for a sound that is both articulate and pronounced. The analogue knobs allow you to fine tune the controls to your personal preferences while the guitar-influenced leather strap enables easy and stylish travel.</p><p> </p><p>The FM radio is perhaps gone for good, the assumption apparently being that the jury has ruled in favor of streaming over the internet. The IR blaster is another feature due for retirement – the S6 had it, then the Note5 didn’t, and now with the S7 the trend is clear.</p><p> </p><p>Perfectly Done</p><p>Meanwhile, the IP68 water resistance has improved from the S5, allowing submersion of up to five feet for 30 minutes, plus there’s no annoying flap covering the charging port</p><p> </p><ul><li>No FM radio (except for T-Mobile units in the US, so far)</li><li>No IR blaster</li><li>No stereo speakers</li></ul><p>If you’ve taken the phone for a plunge in the bath, you’ll need to dry the charging port before plugging in. Samsung hasn’t reinvented the wheel with the design of the Galaxy S7, but it didn’t need to. The Gala S6 was an excellently styled device, and the S7 has managed to improve on that.</p><div><div class=\"gtx-trans-icon\"> </div></div>', 35, NULL, '<ul><li>Unrestrained and portable active stereo speaker</li><li>Free from the confines of wires and chords</li><li>20 hours of portable capabilities</li><li>Double-ended Coil Cord with 3.5mm Stereo Plugs Included</li><li>3/4″ Dome Tweeters: 2X and 4″ Woofer: 1X</li></ul>', NULL, 5, NULL, NULL, '26,27,28,29', NULL, 13.43, NULL, 'publish', '[\"1\",\"2\"]', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'in', 'variable', NULL, 1, NULL, NULL, NULL, NULL);
INSERT INTO `products` VALUES (7, NULL, 'Korea Long Sofa Fabric In Blue Navy Color', 'korea-long-sofa-fabric-in-blue-navy-color', '<p><strong>Embodying the Raw, Wayward Spirit of Rock \'N\' Roll</strong></p>\r\n<p>Embodying the raw, wayward spirit of rock ‘n’ roll, the Kilburn portable active stereo speaker takes the unmistakable look and sound of Marshall, unplugs the chords, and takes the show on the road.</p>\r\n<p> </p>\r\n<p>Weighing in under 7 pounds, the Kilburn is a lightweight piece of vintage styled engineering. Setting the bar as one of the loudest speakers in its class, the Kilburn is a compact, stout-hearted hero with a well-balanced audio which boasts a clear midrange and extended highs for a sound that is both articulate and pronounced. The analogue knobs allow you to fine tune the controls to your personal preferences while the guitar-influenced leather strap enables easy and stylish travel.</p>\r\n<p><img src=\"../../../../uploads/demo/templates/post-image.jpg\" alt=\"Embodying the Raw, Wayward Spirit of Rock \'N\' Roll\" width=\"654\" height=\"205\" /></p>\r\n<p>What do you get</p>\r\n<p>Sound of Marshall, unplugs the chords, and takes the show on the road.</p>\r\n<p>Weighing in under 7 pounds, the Kilburn is a lightweight piece of vintage styled engineering. Setting the bar as one of the loudest speakers in its class, the Kilburn is a compact, stout-hearted hero with a well-balanced audio which boasts a clear midrange and extended highs for a sound that is both articulate and pronounced. The analogue knobs allow you to fine tune the controls to your personal preferences while the guitar-influenced leather strap enables easy and stylish travel.</p>\r\n<p> </p>\r\n<p>The FM radio is perhaps gone for good, the assumption apparently being that the jury has ruled in favor of streaming over the internet. The IR blaster is another feature due for retirement – the S6 had it, then the Note5 didn’t, and now with the S7 the trend is clear.</p>\r\n<p> </p>\r\n<p>Perfectly Done</p>\r\n<p>Meanwhile, the IP68 water resistance has improved from the S5, allowing submersion of up to five feet for 30 minutes, plus there’s no annoying flap covering the charging port</p>\r\n<p> </p>\r\n<ul>\r\n<li>No FM radio (except for T-Mobile units in the US, so far)</li>\r\n<li>No IR blaster</li>\r\n<li>No stereo speakers</li>\r\n</ul>\r\n<p>If you’ve taken the phone for a plunge in the bath, you’ll need to dry the charging port before plugging in. Samsung hasn’t reinvented the wheel with the design of the Galaxy S7, but it didn’t need to. The Gala S6 was an excellently styled device, and the S7 has managed to improve on that.</p>\r\n<div>\r\n<div class=\"gtx-trans-icon\"> </div>\r\n</div>', 36, NULL, '<ul>\r\n<li>Unrestrained and portable active stereo speaker</li>\r\n<li>Free from the confines of wires and chords</li>\r\n<li>20 hours of portable capabilities</li>\r\n<li>Double-ended Coil Cord with 3.5mm Stereo Plugs Included</li>\r\n<li>3/4″ Dome Tweeters: 2X and 4″ Woofer: 1X</li>\r\n</ul>', NULL, 5, NULL, NULL, '26,27,28,29', NULL, NULL, NULL, 'publish', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'out', 'simple', NULL, 1, NULL, NULL, '2021-05-04 05:23:58', NULL);
INSERT INTO `products` VALUES (8, NULL, 'LG White Front Load Steam Washer', 'lg-white-front-load-steam-washer', '<p><strong>Embodying the Raw, Wayward Spirit of Rock \'N\' Roll</strong></p><p>Embodying the raw, wayward spirit of rock ‘n’ roll, the Kilburn portable active stereo speaker takes the unmistakable look and sound of Marshall, unplugs the chords, and takes the show on the road.</p><p> </p><p>Weighing in under 7 pounds, the Kilburn is a lightweight piece of vintage styled engineering. Setting the bar as one of the loudest speakers in its class, the Kilburn is a compact, stout-hearted hero with a well-balanced audio which boasts a clear midrange and extended highs for a sound that is both articulate and pronounced. The analogue knobs allow you to fine tune the controls to your personal preferences while the guitar-influenced leather strap enables easy and stylish travel.</p><p><img src=\"/uploads/demo/templates/post-image.jpg\" alt=\"Embodying the Raw, Wayward Spirit of Rock \'N\' Roll\" width=\"654\" height=\"205\" /></p><p>What do you get</p><p>Sound of Marshall, unplugs the chords, and takes the show on the road.</p><p>Weighing in under 7 pounds, the Kilburn is a lightweight piece of vintage styled engineering. Setting the bar as one of the loudest speakers in its class, the Kilburn is a compact, stout-hearted hero with a well-balanced audio which boasts a clear midrange and extended highs for a sound that is both articulate and pronounced. The analogue knobs allow you to fine tune the controls to your personal preferences while the guitar-influenced leather strap enables easy and stylish travel.</p><p> </p><p>The FM radio is perhaps gone for good, the assumption apparently being that the jury has ruled in favor of streaming over the internet. The IR blaster is another feature due for retirement – the S6 had it, then the Note5 didn’t, and now with the S7 the trend is clear.</p><p> </p><p>Perfectly Done</p><p>Meanwhile, the IP68 water resistance has improved from the S5, allowing submersion of up to five feet for 30 minutes, plus there’s no annoying flap covering the charging port</p><p> </p><ul><li>No FM radio (except for T-Mobile units in the US, so far)</li><li>No IR blaster</li><li>No stereo speakers</li></ul><p>If you’ve taken the phone for a plunge in the bath, you’ll need to dry the charging port before plugging in. Samsung hasn’t reinvented the wheel with the design of the Galaxy S7, but it didn’t need to. The Gala S6 was an excellently styled device, and the S7 has managed to improve on that.</p><div><div class=\"gtx-trans-icon\"> </div></div>', 37, NULL, '<ul><li>Unrestrained and portable active stereo speaker</li><li>Free from the confines of wires and chords</li><li>20 hours of portable capabilities</li><li>Double-ended Coil Cord with 3.5mm Stereo Plugs Included</li><li>3/4″ Dome Tweeters: 2X and 4″ Woofer: 1X</li></ul>', NULL, 3, NULL, NULL, '26,27,28,29', NULL, 1422.00, 1025.00, 'publish', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'in', 'simple', NULL, 2, NULL, NULL, NULL, NULL);
INSERT INTO `products` VALUES (10, NULL, 'Lò vi sóng Sharp R-G272VN-S 20 lít', '1', '<ul style=\"margin-top:0px;padding:0px;margin-bottom:1.3em;color:#0a0a0a;font-family:Roboto, sans-serif;font-size:15px;background-color:#ffffff;\">\r\n<li style=\"margin-bottom:.6em;margin-left:1.3em;\">Bảng điều khiển điện tử tích hợp sẵn các chức năng nấu, thực đơn tự động.</li>\r\n<li style=\"margin-bottom:.6em;margin-left:1.3em;\">Chức năng khóa bảng điều khiển tự động vô hiệu hóa toàn bộ bàn phím.</li>\r\n<li style=\"margin-bottom:.6em;margin-left:1.3em;\">Lò vi sóng Sharp R-G272VN-S thương hiệu nổi tiếng Nhật Bản.</li>\r\n</ul>', 107, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 120000.00, 100000.00, 'publish', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'in', 'simple', NULL, 1, NULL, '2021-04-20 05:46:43', '2021-04-20 05:48:07', NULL);
INSERT INTO `products` VALUES (11, NULL, 'Android Tivi Sony 4K 43 inch KD-43X7500E', 'android-tivi-sony-4k-43-inch-kd-43x7500e', '<p><span style=\"color:#0a0a0a;font-family:Roboto, sans-serif;font-size:15px;background-color:#ffffff;\">Thiết kế mỏng ấn tượng Smart Tivi Sony 4K 43 inch KD-43X7500E sở hữu kiểu dáng tinh tế, thời trang cùng với kích thước màn hình rộng 43 inch, kết hợp khung viền chắc chắn, sắc sảo và tông màu đen quý phái. Trải nghiệm hình ảnh rõ ràng, chân thật Không chỉ…</span></p>', 108, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2000000.00, 1800000.00, 'publish', NULL, NULL, NULL, NULL, NULL, 7, NULL, NULL, 'in', 'simple', NULL, 1, NULL, '2021-04-20 05:48:30', '2021-05-05 05:43:28', NULL);

-- ----------------------------
-- Table structure for user_meta
-- ----------------------------
DROP TABLE IF EXISTS `user_meta`;
CREATE TABLE `user_meta`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NULL DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `val` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `create_user` int(11) NULL DEFAULT NULL,
  `update_user` int(11) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for user_upgrade_request
-- ----------------------------
DROP TABLE IF EXISTS `user_upgrade_request`;
CREATE TABLE `user_upgrade_request`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NULL DEFAULT NULL,
  `role_request` int(11) NULL DEFAULT NULL,
  `approved_time` datetime(0) NULL DEFAULT NULL,
  `status` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `approved_by` int(11) NULL DEFAULT NULL,
  `create_user` int(11) NULL DEFAULT NULL,
  `update_user` int(11) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for user_wishlist
-- ----------------------------
DROP TABLE IF EXISTS `user_wishlist`;
CREATE TABLE `user_wishlist`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `object_id` int(11) NULL DEFAULT NULL,
  `object_model` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `user_id` int(11) NULL DEFAULT NULL,
  `create_user` int(11) NULL DEFAULT NULL,
  `update_user` int(11) NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `first_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `last_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp(0) NULL DEFAULT NULL,
  `password` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `address2` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `phone` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `birthday` date NULL DEFAULT NULL,
  `city` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `state` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `country` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `postcode` int(11) NULL DEFAULT NULL,
  `last_login_at` datetime(0) NULL DEFAULT NULL,
  `avatar_id` bigint(20) NULL DEFAULT NULL,
  `bio` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `status` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `create_user` int(11) NULL DEFAULT NULL,
  `update_user` int(11) NULL DEFAULT NULL,
  `vendor_commission_amount` int(11) NULL DEFAULT NULL,
  `vendor_commission_type` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `locale` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `company` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `shipping_first_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `shipping_last_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `shipping_address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `shipping_address2` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `shipping_city` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `shipping_country` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `shipping_postcode` int(11) NULL DEFAULT NULL,
  `shipping_company` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  `remember_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `users_email_unique`(`email`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (1, 'Thanh admin', 'Thanh admin', 'Thanh admin', 'admin@dev.com', NULL, '$2y$10$Beu5HPAZ4smPlJ8kotwFr.WbdZRBOosxvjS13wrABU1kHwIxlkPOW', 'hà nội', NULL, '0347519202', NULL, NULL, NULL, NULL, NULL, NULL, 87, 'We\'re designers who have fallen in love with creating spaces for others to reflect, reset, and create. We split our time between two deserts (the Mojave, and the Sonoran). We love the way the heat sinks into our bones, the vibrant sunsets, and the wildlife we get to call our neighbors.', 'publish', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '3MMFSQLIzv27F7nvofwFjDdJRCp8CKgbxWnjBz6wbWbip12WFVAkRes9quPS', '2021-04-12 06:15:19', '2021-05-15 07:23:01');
INSERT INTO `users` VALUES (2, 'Đỗ Thanh', 'Đỗ Thanh', 'Đỗ Thanh', 'thanhdv193@gmail.com', NULL, '', 'hà nội', NULL, '0347519202', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, '<p>xxxx</p>', 'publish', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-04-14 08:01:50', '2021-05-15 05:42:29');

SET FOREIGN_KEY_CHECKS = 1;
