<!doctype html>
<html lang="en" class="no-js">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Martfury - Marketplace Theme</title>

    <link href="https://fonts.googleapis.com/css?family=Work+Sans:400,500,600,700" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('landing')}}/css/bootstrap.min.css"/>
    <link type="text/css" rel="stylesheet" href="{{asset('landing')}}/css/style.css"/>
    <link type="text/css" rel="stylesheet" href="{{asset('landing')}}/css/responsive.css"/>
    <script src="{{asset('landing')}}/js/jquery-3.3.1.min.js"></script>

</head>
<body class="landing-page">

<header id="site-header" class="site-header">
    <div class="un-container">
        <div class="row header-row">
            <div class="logo col-lg-2 col-md-6 col-sm-6 col-xs-6">
                <a href="#"><img src="{{asset('landing')}}/images/logo.png"></a>
            </div>
            <div class="site-menu col-md-10 hidden-sm hidden-xs hidden-md">
                <nav id="site-navigation" class="filters main-nav">
                    <ul class="nav navbar-nav">
                        <li><a href="#section-homepages">Demos</a></li>
                        <li><a href="#section-mobile-version">Mobile Version </a></li>
                        <li><a href="#section-shop">Shop Pages </a></li>
                        <li><a href="#section-product">Product Layouts </a></li>
                        <li><a href="#section-features">Features</a></li>

                    </ul>
                    <a target="_blank" href="#" class="purchase">Purchase script $59</a>
                </nav>
            </div>
            <div class="site-toggle col-md-6 col-sm-6 col-xs-6 hidden-lg">
                <button class="toggle-menu">
                    <div class="sidebar one"></div>
                    <div class="sidebar two"></div>
                    <div class="sidebar three"></div>
                </button>
            </div>
        </div>
    </div>
</header>
<div class="site-menu--mobile">
    <button class="toggle-menu">
        <div class="sidebar one"></div>
        <div class="sidebar two"></div>
        <div class="sidebar three"></div>
    </button>
    <nav class="menu-mobile">
        <ul class="navbar-nav">
            <li><a href="#section-homepages">Demos</a></li>
            <li><a href="#section-mobile-version">Mobile Version </a></li>
            <li><a href="#section-shop">Shop Pages </a></li>
            <li><a href="#section-vendor">Vendors Pages</a></li>
            <li><a href="#section-product">Product Layouts </a></li>
            <li><a href="#section-features">Features</a></li>
            <li><a target="_blank" href="http://docs.bookingcore.org/martfury/">Documentation</a></li>

        </ul>
        <a target="_blank" href="#" class="purchase">Purchase script $59</a>
    </nav>
</div>
<div class="site-banner">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-7 col-md-offset-6 col-sm-offset-5">
                <h2 class="intro-title">
                    Most <span class="primary-color">powerful</span>
                    Laravel Script for
                    your store online
                </h2>
                <p class="intro-desc">
                    <strong>MartFury - the ultimate Laravel Script e-commerce toolkit</strong> that helps
                    you sell anything.The best choice for your next personal or client's
                    e-shop or multi-vendor marketplace project.
                </p>

            </div>
        </div>
    </div>
</div>

<div class="site-content">
    <section id="section-homepages" class="section-homepages section scrollto">
        <div class="list-features">
            <div class="feature-item">
                <h3>High Performance
                    Speed</h3>
                <p>Everyone hates waiting. Codecanyon, we work hard for highest performance. Result prove it.</p>
            </div>
            <div class="feature-item">
                <h3>Mobile Optimized
                    Design</h3>
                <p>Unero focus on the factors responsive and optimized design based on user experience.</p>
            </div>
            <div class="feature-item">
                <h3>Full WooCommerce
                    Compatible</h3>
                <p>A free eCommerce plugin that allows you can sell anything.</p>
            </div>
            <div class="feature-item">
                <h3>Easily Built Multi-Vendor
                    Marketplace</h3>
                <p>Martfury comes with full intergatation of Dokan, WC Vendors and WC Marketplace.</p>
            </div>
            <div class="feature-item">
                <h3>Visual Composer
                    Included</h3>
                <p>Building anything with Drag & Drop page builder and a large element library.</p>
            </div>
            <div class="feature-item">
                <h3>Install Demo Content</h3>
                <p>Install your demo content settings with one click.</p>
            </div>
            <div class="feature-item">
                <h3>Color, Label and
                    Image Swatches</h3>
                <p>Generate color, label and image swatches to display the available product variable attributes like
                    colors, sizes, styles....</p>
            </div>
            <div class="feature-item">
                <h3>Fully
                    AJAX Shop</h3>
                <p>Filter products by products categories, products attributes: color, size, weight... and price without
                    needing to load the page again.</p>
            </div>
            <div class="feature-item">
                <h3>Buil-in
                    Live search</h3>
                <p>Search your products instantly and sell them quickly.</p>
            </div>
            <div class="feature-item">
                <h3>Advanced
                    Typography</h3>
                <p>Create custom google fonts in the admin area that are instantly available in the Customizer
                    preview.</p>
            </div>
            <div class="feature-item">
                <h3>Featured
                    Product Video</h3>
                <p>See a YouTube or Vimeo video instead of the featured image of the product detail page.</p>
            </div>
        </div>
        <div class="list-homepages">
            <div class="un-container">
                <div class="row">
                    <div class="section-header col-md-12 text-center">
                        <h2 class="section-title">Unique Demos</h2>
                        <p class="line-1">Martfury created many concepts designed based on
                            <strong>user experience</strong> from real sites.</p>
                        <p class="line-2">as <strong>Amazon, Flipkart, Jumia, Ebay,</strong> etc.
                            <strong>Help increase high converation rate</strong> to buy product with your customers so
                            quickly.
                        </p>
                    </div>


                    <div class="tabs-homepage col-md-12">
                        <div class="tab-content">
                            <div class="item-content">
                                <div class="row justify-content-center" style="
    display: flex;
    justify-content: space-between;">
                                    <div class="item-home col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                        <div class="homepage-item">
                                            <a class="item" href="{{ asset('/') }}" target="_blank">
                                                <span class="img-item">
                                                    <img src="{{asset('landing')}}/images/h11.jpg" alt="Home 11">
                                                </span>
                                                <h2>Marketplace Full Width</h2>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item-content">
                                <div class="row">
                                    <div class="item-home col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                        <div class="homepage-item">
                                            <a class="item" href="http://demo2.drfuri.com/martfury3/" target="_blank">
                                                <span class="img-item">
                                                    <img src="{{ asset('landing') }}/images/h11.jpg" alt="Home 11">
                                                </span>
                                                <h2>Marketplace Full Width</h2>
                                            </a>
                                        </div>

                                    </div>
                                    <div class="item-home col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                        <div class="homepage-item">
                                            <a class="item" href="http://demo2.drfuri.com/martfury9/" target="_blank">
                                                <span class="img-item">
                                                    <img src="{{asset('landing')}}/images/h10.jpg" alt="Home 10">
                                                </span>
                                                <h2>Auto Parts</h2>
                                            </a>
                                        </div>

                                    </div>

                                    <div class="item-home col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                        <div class="homepage-item">
                                            <a class="item" href="http://demo2.drfuri.com/martfury/" target="_blank">
                                                <span class="img-item">
                                                    <img src="{{asset('landing')}}/images/h-1.jpg" alt="Home 1">
                                                </span>
                                                <h2>Marketplace 1</h2>
                                            </a>
                                        </div>

                                    </div>
                                    <div class="item-home col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                        <div class="homepage-item">
                                            <a class="item" href="http://demo2.drfuri.com/martfury/?infinite=0"
                                               target="_blank">
                                                <span class="img-item">
                                                    <img src="{{asset('landing')}}/images/h-1.jpg" alt="Home 1">
                                                </span>
                                                <h2>Marketplace 1 Without AJAX</h2>
                                            </a>
                                        </div>

                                    </div>
                                    <div class="item-home col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                        <div class="homepage-item">
                                            <a class="item" href="http://demo2.drfuri.com/martfury2" target="_blank">
                                                        <span class="img-item">
                                                            <img src="{{asset('landing')}}/images/h-2.jpg" alt="Home 2">
                                                        </span>
                                                <h2>Marketplace 2</h2>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="item-home col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                        <div class="homepage-item">
                                            <a class="item" href="http://demo2.drfuri.com/martfury2?infinite=0"
                                               target="_blank">
                                                        <span class="img-item">
                                                            <img src="{{asset('landing')}}/images/h-2.jpg" alt="Home 2">
                                                        </span>
                                                <h2>Marketplace 2 Without AJAX</h2>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="item-home col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                        <div class="homepage-item">
                                            <a class="item"
                                               href="http://demo2.drfuri.com/martfury3/homepage/?home_style=3"
                                               target="_blank">
                                                        <span class="img-item">
                                                            <img src="{{asset('landing')}}/images/h-3.jpg" alt="Home 3">
                                                        </span>
                                                <h2>Marketplace 3</h2>
                                            </a>
                                        </div>

                                    </div>
                                    <div class="item-home col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                        <div class="homepage-item">
                                            <a class="item"
                                               href="http://demo2.drfuri.com/martfury3/homepage/?home_style=3&infinite=0"
                                               target="_blank">
                                                        <span class="img-item">
                                                            <img src="{{asset('landing')}}/images/h-3.jpg" alt="Home 3">
                                                        </span>
                                                <h2>Marketplace 3 Without AJAX</h2>
                                            </a>
                                        </div>

                                    </div>
                                    <div class="item-home col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                        <div class="homepage-item">
                                            <a class="item"
                                               href="http://demo2.drfuri.com/martfury2/homepage-4/?home_style=4"
                                               target="_blank">
                                                        <span class="img-item">
                                                            <img src="{{asset('landing')}}/images/h-4.jpg" alt="Home 4">
                                                        </span>
                                                <h2>Marketplace 4</h2>
                                            </a>
                                        </div>

                                    </div>
                                    <div class="item-home col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                        <div class="homepage-item">
                                            <a class="item" href="http://demo2.drfuri.com/martfury4" target="_blank">
                                                        <span class="img-item">
                                                            <img src="{{asset('landing')}}/images/h-5.jpg" alt="Home 5">
                                                        </span>
                                                <h2>Electronic</h2>
                                            </a>
                                        </div>

                                    </div>
                                    <div class="item-home col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                        <div class="homepage-item">
                                            <a class="item" href="http://demo2.drfuri.com/martfury4?infinite=0"
                                               target="_blank">
                                                        <span class="img-item">
                                                            <img src="{{asset('landing')}}/images/h-5.jpg" alt="Home 5">
                                                        </span>
                                                <h2>Electronic Without AJAX</h2>
                                            </a>
                                        </div>

                                    </div>
                                    <div class="item-home col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                        <div class="homepage-item">
                                            <a class="item" href="http://demo2.drfuri.com/martfury5" target="_blank">
                                                        <span class="img-item">
                                                            <img src="{{asset('landing')}}/images/h-6.jpg" alt="Home 6">
                                                        </span>
                                                <h2>Furniture</h2>
                                            </a>
                                        </div>

                                    </div>
                                    <div class="item-home col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                        <div class="homepage-item">
                                            <a class="item" href="http://demo2.drfuri.com/martfury7" target="_blank">
                                                        <span class="img-item">
                                                            <img src="{{asset('landing')}}/images/h8.jpg" alt="Home 7">
                                                        </span>
                                                <h2>Organic</h2>
                                            </a>
                                        </div>

                                    </div>
                                    <div class="item-home col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                        <div class="homepage-item">
                                            <a class="item" href="http://demo2.drfuri.com/martfury8" target="_blank">
                                                        <span class="img-item">
                                                            <img src="{{asset('landing')}}/images/h9.jpg" alt="Home 8">
                                                        </span>
                                                <h2>Technology</h2>
                                            </a>
                                        </div>

                                    </div>
                                    <div class="item-home col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                        <div class="homepage-item">
                                            <a class="item" href="http://demo2.drfuri.com/martfury6/fa/"
                                               target="_blank">
                                                        <span class="img-item">
                                                            <img src="{{asset('landing')}}/images/h-7.jpg" alt="Home 7">
                                                        </span>
                                                <h2>RTL</h2>
                                            </a>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>

    <section id="section-mobile-version" class="section-mobile-version section scrollto hidden">
        <div class="section-header col-md-12 text-center">
            <h2 class="section-title">Welcome to Martfury <strong>Mobile Version</strong>
                <small>HOT</small>
            </h2>
            <p class="line-1">This is Martfury Mobile Layout demo. Please scan the <strong>QR code </strong> on your
                right with your mobile device</p>
            <p>to have <strong>an authentic experience.</strong></p>
        </div>
        <div class="list-mobile-version">
            <div class="row">
                <div class="mobile-item col-lg-4 col-md-6 col-sm-12 col-xs-12">
                    <div class="box-img-item">
                        <div class="img-item">
                            <div class="box-img">
                                <img src="{{asset('landing')}}/images/home_v1.png" alt="Marketplace V1 Mobile">
                            </div>
                            <h2>Marketplace V1</h2>
                        </div>
                        <div class="qr-code">
                            <p>Scan the QR Code
                                bellow to view a live
                                demo on your
                                mobile device
                            </p>
                            <div class="qr-code-img">
                                <img src="{{asset('landing')}}/images/home_v1_qr.png" alt="Marketplace V1 Mobile">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="mobile-item col-lg-4 col-md-6 col-sm-12 col-xs-12">
                    <div class="box-img-item">
                        <div class="img-item">
                            <div class="box-img">
                                <img src="{{asset('landing')}}/images/home_v2.png" alt="Marketplace V2 Mobile">
                            </div>
                            <h2>Marketplace V2</h2>
                        </div>
                        <div class="qr-code">
                            <p>Scan the QR Code
                                bellow to view a live
                                demo on your
                                mobile device
                            </p>
                            <div class="qr-code-img">
                                <img src="{{asset('landing')}}/images/home_v2_qr.png" alt="Marketplace V2 Mobile">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="mobile-item col-lg-4 col-md-6 col-sm-12 col-xs-12">
                    <div class="box-img-item">
                        <div class="img-item">
                            <div class="box-img">
                                <img src="{{asset('landing')}}/images/home_v3.png" alt="Marketplace V3 Mobile">
                            </div>
                            <h2>Marketplace V3</h2>
                        </div>
                        <div class="qr-code">
                            <p>Scan the QR Code
                                bellow to view a live
                                demo on your
                                mobile device
                            </p>
                            <div class="qr-code-img">
                                <img src="{{asset('landing')}}/images/home_v3_qr.png" alt="Marketplace V3 Mobile">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="mobile-buttons">
            <a target="_blank" href="https://drfuri.com/intro/martfury-mobile" class="purchase">View All Mobile</a>
        </div>
    </section>

    <section id="section-features" class="section section-features scrollto hidden">
        <div class="un-container">
            <div class="section-header text-center">
                <h2 class="section-title">MartFury Features</h2>
                <p>Packed with essential sale-boosting features that make your eCommerce shop awesome</p>
            </div>

            <div class="features-wrapper row" style="
    display: flex;
    justify-content: space-between;">
                <div class="features-item col-md-4 col-sm-6 col-xs-12">
                    <img src="{{asset('landing')}}/images/fea-7.jpg">
                </div>
            </div>
        </div>
    </section>

    <section id="section-shop" class="section section-category scrollto">
        <div class="un-container">
            <div class="row" >
                <div class="section-header col-md-12 text-center">
                    <h2 class="section-title">Shop Pages</h2>
                    <p>With 05 layouts style as
                        <strong>Default, Categories, Sidebar, Product of Category & Carousel</strong> etc.</p>
                    <p>You can easy to customize for your shop page.</p>
                </div>
                <div style="
    display: flex;
    justify-content: space-between;">
                <div class="item col-md-4 col-sm-6 col-xs-12">
                    <a href="{{route('product.index')}}" target="_blank">
								<span class="img-item">
									<img src="{{asset('landing')}}/images/s211.jpg" alt="Shop Full Width">
								</span>
                        <span class="item-title">Shop Full Width</span>
                    </a>
                </div>
                </div>
            </div>
        </div>
    </section>

    <section id="section-product" class="section section-product scrollto">
        <div class="un-container">
            <div class="row">
                <div class="section-header col-md-12 text-center">
                    <h2 class="section-title">Product Layouts</h2>
                    <p>MartFury is designed to make your life easier. Every product layout has been optimized to fit
                        perfectly with your needs.</p>
                </div>
            </div>
            <div class="row" style="
    display: flex;
    justify-content: space-between;">
                <div class="item col-md-4 col-sm-6 col-xs-12">
                    <a href="http://demo2.drfuri.com/martfury3/shop/marshall-kilburn-portable-wireless-bluetooth-speaker-black/"
                       target="_blank">
								<span class="img-item">
									<img src="{{asset('landing')}}/images/p211.jpg" alt="Product Full Width">
									</span>
                        <span class="item-title">Product Full Width</span>
                    </a>
                </div>
            </div>
        </div>
    </section>

</div>

<footer class="site-footer text-center">
    <div class="container">
        <div class="row">
            <div class="col-md-12 footer-extra">
                <h2 class="footer-title">Start Creating That Amazing Website
                    That You Have Dreamed For!</h2>
                <p>Beautifully designed, powerful, and easy to customize – MartFury the best Laravel Script for online
                    marketplace.</p>
            </div>
        </div>
    </div>
</footer>

<script type="text/javascript" src="{{asset('landing')}}/js/imagesloaded.min.js"></script>
<script type="text/javascript" src="{{asset('landing')}}/js/isotope.pkgd.min.js"></script>
<script src="{{asset('landing/js')}}/jquery.stickyNavbar.min.js"></script>
<script src="{{asset('landing')}}/js/slick.min.js"></script>
<script src="{{asset('landing')}}/js/scripts.js"></script>

</body>
</html>
